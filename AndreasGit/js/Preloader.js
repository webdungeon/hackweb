Game.Preloader = function(game){
    this.preloadBar = null;
};

Game.Preloader.prototype = {
    preload:function(){
        
        this.preloadBar = this.add.sprite(this.world.centerX,this.world.centerY,'loading3');
 
        this.preloadBar.anchor.setTo(0.5,0.5);
        this.time.advancedTiming = true ;
        this.load.setPreloadSprite(this.preloadBar);
        
        //LOAD ALL ASSETS
        this.load.tilemap('map', 'assets//tilemaps/level1.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('map2', 'assets//tilemaps/level2.json', null,Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('map3','assets//tilemaps/level3.json',null,Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('CookieMainCamera','assets//tilemaps/CookieMainCamera.json',null,Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('SecurityMainCamera','assets//tilemaps/SecurityMainCamera.json',null,Phaser.Tilemap.TILED_JSON);//
        this.load.tilemap('upDownMap','assets//tilemaps/upDownMap.json',null,Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('leftRightMap','assets//tilemaps/leftRightMap.json',null,Phaser.Tilemap.TILED_JSON);
        
        //level1
        this.load.image('tiles1','assets/level1Images/level1img3.png');
        this.load.image('nature_tiles','assets/level1Images/nature_tiles.png');
        this.load.image('nature_tiles2','assets/level1Images/nature_tiles2.png');
        this.load.image('nature_tiles3','assets/level1Images/nature_tiles3.png');
        this.load.image('nature_tiles4','assets/level1Images/nature_tiles4.png');
        
       
        //level 2
        this.load.image('deserthouses','assets/level2Images/deserthouses.png');
        this.load.image('miniboat','assets/level2Images/miniboat.png');
        this.load.image('water','assets/level2Images/water.png');
        this.load.image('train','assets/level2Images/train.png');
        this.load.image('trees','assets/level2Images/trees.png');
        
        
        //level 3
        
        this.load.image('ground','assets/level3Images/ground.jpg');
        this.load.image('treeslv3','assets/level3Images/trees.png');
        this.load.image('house','assets/level3Images/house.png');
        this.load.image('dragon1','assets/level3Images/dragon1.png');
        
        //level3 version 2
        this.load.image('ground6','assets/level3Images/ground6.jpg');
        this.load.image('ground4','assets/level3Images/ground4.jpg');
        
        //level Cookie Main Map
        this.load.image('city_ground','assets/levelCookieImages/city_ground.png');
        this.load.image('ground_cookie','assets/levelCookieImages/ground_cookie.jpg');
        this.load.image('cookietrees3','assets/levelCookieImages/cookietrees3.png');
        this.load.image('cookiehouse1','assets/levelCookieImages/cookiehouse1.png');
        this.load.image('cookiehouse2','assets/levelCookieImages/cookiehouse2.png');
        this.load.image('city_buildings','assets/levelCookieImages/city_buildings.png');
        this.load.image('city_tiles_2','assets/levelCookieImages/city_tiles_2.png');
        this.load.image('walls','assets/levelCookieImages/walls.png');
        //this.load.image('cookiewalls','assets/levelCookieImages/cookiewalls.png');
        this.load.spritesheet('topMapNpc','assets/levelCookieImages/topMapNpc.png');
        this.load.spritesheet('mainNpc','assets/levelCookieImages/mainNpc.png');
        this.load.spritesheet('stoneMainNpc','assets/levelCookieImages/stoneMainNpc.png');
        this.load.image('cookieMob','assets/levelCookieImages/cookieMob.png');
		this.load.spritesheet('cookieAnimation','assets/levelCookieImages/cookieAnimation.png',64,64,16);
        this.load.spritesheet('doorsanimation1','assets/levelCookieImages/dooranimation1.png',67,30,4);
        this.load.spritesheet('cookiemonster2','assets/levelCookieImages/cookiemonster2.png',64,64);
        this.load.spritesheet('finalCookieMob','assets/levelCookieImages/finalCookieMob.png',75,70,4);
        this.load.spritesheet('fountain1','assets/levelCookieImages/fountain1.png',160,128);
        this.load.spritesheet('doors1','assets/levelCookieImages/doors1.png',192,64);
        this.load.spritesheet('portal1','assets/levelCookieImages/portal1.png',116.5,115.5);
        this.load.image('Cookie-types','assets/levelCookieImages/Cookie-types.png');
        
        //Level Security
        
        
        
        //Security Main Camera
        this.load.image('terrain','assets/levelSecurityImages/mainCamera/terrain.jpg');
        //this.load.image('terrain2','assets/levelSecurityImages/mainCamera/terrain2.jpg');
        this.load.image('terrainLava','assets/levelSecurityImages/mainCamera/terrainLava.jpg');
        
        this.load.image('terrain2','assets/levelSecurityImages/mainCamera/terrain2.png');
        this.load.image('terrain3','assets/levelSecurityImages/mainCamera/terrain3.jpg');
        this.load.image('lava7','assets/levelSecurityImages/mainCamera/lava7.png');
        
        this.load.image('volcano1','assets/levelSecurityImages/mainCamera/volcano1.png');
        this.load.image('volcano2','assets/levelSecurityImages/mainCamera/volcano2.png');
        this.load.spritesheet('fireAnim1','assets/levelSecurityImages/mainCamera/fireAnim1.png',96,96);
        this.load.spritesheet('securityPortal1','assets/levelSecurityImages/mainCamera/securityPortal1.png',192,192);
        this.load.spritesheet('portal_spark','assets/levelSecurityImages/mainCamera/portal_spark.png',182,206);
        this.load.bitmapFont('desyrel', 'assets/levelSecurityImages/mainCamera/desyrel.png', 'assets/levelSecurityImages/mainCamera/desyrel.xml');
        
        //UP DOWN MAP
        this.load.image('ground9','assets/levelSecurityImages/upDownCamera/ground9.jpg');
        this.load.image('wallSprite','assets/levelSecurityImages/upDownCamera/wallSprite.jpg');
        this.load.spritesheet('npc1','assets/levelSecurityImages/upDownCamera/npc1.png');
        this.load.spritesheet('ghostMonster','assets/levelSecurityImages/upDownCamera/ghostMonster.png',46.6,48);
        this.load.image('exclamationMark','assets/levelSecurityImages/upDownCamera/exclamationMark.png');
        this.load.spritesheet('portal1x','assets/levelSecurityImages/upDownCamera/portal1x.png',96,64);
        this.load.spritesheet('mummyMonster','assets/levelSecurityImages/upDownCamera/mummyMonster.png',37,45);
        this.load.spritesheet('soldatPlayer','assets/levelSecurityImages/upDownCamera/soldatPlayer.png',64,64);
        this.load.spritesheet('skeletonMonster','assets/levelSecurityImages/upDownCamera/skeletonMonster.png',64,64);
        this.load.spritesheet('chest1','assets/levelSecurityImages/upDownCamera/chest1.png',78,48);
        this.load.image('arrow','assets/levelSecurityImages/upDownCamera/arrow.png');
        this.load.spritesheet('key','assets/levelSecurityImages/upDownCamera/key.png');
        this.load.spritesheet('coins','assets/levelSecurityImages/upDownCamera/coins.png',32,32);
        this.load.spritesheet('information','assets/levelSecurityImages/upDownCamera/information.png',32,32);
        this.load.spritesheet('bigwall','assets/levelSecurityImages/upDownCamera/bigwall.png');
        
        //LEFT RIGHT MAP
        this.load.image('wallMapHacked','assets/levelSecurityImages/leftRightCamera/wallMapHacked.png');
        this.load.image('inventory','assets/levelSecurityImages/leftRightCamera/inventory.png');
        this.load.image('button','assets/levelSecurityImages/leftRightCamera/button.png');
        this.load.image('close','assets/levelSecurityImages/leftRightCamera/close.png');
        this.load.image('killMonsters','assets/levelSecurityImages/leftRightCamera/killMonsters.png');
        this.load.image('chestSpell','assets/levelSecurityImages/leftRightCamera/chestSpell.png');
        this.load.image('teleportSpell','assets/levelSecurityImages/leftRightCamera/teleportSpell.png');
        this.load.image('rsz_banut','assets/levelSecurityImages/leftRightCamera/rsz_banut.png');
        this.load.spritesheet('npcbot1','assets/levelSecurityImages/leftRightCamera/npcbot1.png');
        this.load.spritesheet('firework','assets/levelSecurityImages/leftRightCamera/firework.png',200,200);
        
        
        
        //PLAYER
        //this.load.spritesheet('player','assets/level1Images/player.png',24,26);  //girl player
        //this.load.spritesheet('player','assets/level2Images/player6.png',107,96);   //horse player
        //this.load.spritesheet('player','assets/level2Images/dragonplayer.png',192,192);  //dragon player
        this.load.spritesheet('player','assets/level2Images/player8.png',32,64);  // normal player
        this.load.spritesheet('cookieplayer','assets/levelCookieImages/cookieplayer.png',75,117);
        
        
        //MONSTERS
        
        
        this.load.image('dragon1','assets/level3Images/dragon1.png');
        
        
        
        //SHOOT SPRINTS
        
        this.load.image('bullet','assets/level3Images/bullet4.png');    
        
        
        
        
        
        // ANIMATIONS
        this.load.spritesheet('fire','assets/level2Images/fire.png',64,59,3);
        
        this.load.spritesheet('doarcazan','assets/level2Images/doarcazan.png',31.75,41,3);
        
        this.load.spritesheet('fire2','assets/level2Images/fire2.png',64,64,3);        
        
        this.load.spritesheet('doors2','assets/level1Images/doors4.png',96,64);
        
        this.load.spritesheet('portal2','assets/level2Images/portal2.jpg',112,149);
        
        
        //SOUNDS
        //this.game.load.audio("soundKey", "assets/sounds/oliver.mp3");
        
    },
    
    create:function(){
         //sleep(4000);
    
},
    
    update:function(){
        //this.state.start('Level2');
        this.state.start('Level1');//
        //this.state.start('LevelSecurityLeftRightMap');//
        //this.state.start('LevelSecurityMainCamera');
       
    }
    
    
    
    
    
};

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 9999999999999999; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
