Game.Level2 = function(game){
   
};

var map;
var layer;
var music;
var fire;
var respawn;

var player;
var controls = {};
var playerSpeed = 500;
var jumpTimer = 0;
var pauseKey;
 

Game.Level2.prototype = {
    create: function(){
        this.stage.backgroundColor = '#3A5963';
        pausedgame = false;
        
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('map2');             
       
        this.map.addTilesetImage('nature_tiles4');
        this.map.addTilesetImage('deserthouses');
        this.map.addTilesetImage('miniboat');
        this.map.addTilesetImage('water');
        this.map.addTilesetImage('train');
        this.map.addTilesetImage('trees');
                            
        this.botLayer = this.map.createLayer('botLayer');     
        this.secondBotLayer = this.map.createLayer('secondBotLayer');
        this.topLayer = this.map.createLayer('topLayer');
        this.topTopLayer = this.map.createLayer('topTopLayer');
        this.topTopTopLayer = this.map.createLayer('topTopTopLayer');

        this.botLayer.resizeWorld();
        
        
        // OBJECT LAYER
        
              //HERE IS THE START POSITION
        this.map.createFromObjects('objectLayer',1999 , '' ,0,true,false,respawn);
        
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        
        // FIRE SPRITE
        fire = this.add.sprite(225,575,'fire');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fire);
        fire.animations.add('fire_anim');
        fire.animations.play('fire_anim', 12, true);
        fire.body.collideWorldBounds = true;
        fire.body.immovable = true;
        
        // doar un cazan SPRITE
        doarcazan = this.add.sprite(1159,278,'doarcazan');
        //doarcazan.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doarcazan);
        var doarcazan_anim = doarcazan.animations.add('doarcazan_anim');
        doarcazan.animations.play('doarcazan_anim', 7, true);
        //doarcazan.body.collideWorldBounds = true;
        doarcazan.body.immovable = true;
        
        // FIRE 2 SPRITE
        fire2 = this.add.sprite(1285,275,'fire2');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fire2);
        var fire2_anim = fire2.animations.add('fire2_anim');
        fire2.animations.play('fire2_anim', 4, true);
        //fire2.body.collideWorldBounds = true;
        fire2.body.immovable = true;
    
        //DOOR TO Level 1
        doors2 = this.add.sprite(70,70,'doors2');
        doors2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doors2);
        doors2.animations.add('doors2_anim',[0,4,7,10,7,4,0]);
        doors2.animations.play('doors2_anim', 10, true);
        doors2.body.collideWorldBounds = true;
        doors2.body.immovable = true;
        
        //PORTAL TO BIG FOREST MAP
        portal2 = this.add.sprite(1448,107,'portal2');
        portal2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal2);
        var portal2_anim = portal2.animations.add('portal2_anim',[0,1,2,3,4,5,6,7,8,9,10]);
        portal2.animations.play('portal2_anim', 10, true);
        portal2.body.collideWorldBounds = true;
        portal2.body.immovable = true;
        
        
        
        
        //PLAYER
    
        
        this.map.setCollisionBetween(1, 100000, true, 'topLayer'); 
        this.map.setCollisionBetween(1, 100000, true, 'topTopLayer');//!!!
        //this.map.setCollisionBetween(1, 100000, true, 'topTopTopLayer');
        
        player = this.add.sprite(0,0,'player');
        player.anchor.setTo(0.5,0.5);
        
        
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        //this.physics.arcade.enable(player);
        this.game.physics.enable(player, Phaser.Physics.ARCADE);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
            
            
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(togglePause, this);
        
 
        
    },
    
    
    update:function(){
         
        
        this.physics.arcade.collide(player,this.botLayer);      
        this.physics.arcade.collide(player,this.topLayer);
        this.physics.arcade.collide(player,this.topTopLayer);
        //this.physics.arcade.collide(player,this.topTopTopLayer);
       // this.physics.arcade.collide(player,fire);
        this.physics.arcade.collide(player,doors2,this.goToLevel1,null,this);
        //this.physics.arcade.collide(player,portal2,this.goToLevel3,null,this);
        
        
        
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        
        if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;       
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
               
        
        
    },
    
    
    //SPAWN FUNCTION
    spawn : function(){
        respawn.forEach(function(spawnPoint){
            player.reset(spawnPoint.x,spawnPoint.y);
        },this);
    },
    
    
    //WHEN HIT FIRE JUMP TO LEVEL1 MAP
    goToLevel1 :function(){
        this.state.start('Level1');
    },
    
    goToLevel3 :function(){
        this.state.start('Level3');
    },
    
    
   
     
    
};


function togglePause() {

    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

}



