Game.Level1 = function(game){
   
};

var map;
var layer;
var music;
var respawn;

var player;
var controls = {};
var playerSpeed = 150;
var jumpTimer = 0;

var pauseKey;
 var element = document.getElementById("HPBar");

var portalToSecurity;
var portalToAjax;
var portalToCookie;
var portalToRest;

Game.Level1.prototype = {
    create: function(){
        //alert(element.value);
        this.stage.backgroundColor = '#3A5963';
        
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('map');       
        
        this.map.addTilesetImage('tiles1');
        this.map.addTilesetImage('nature_tiles');
        this.map.addTilesetImage('nature_tiles2');
        this.map.addTilesetImage('nature_tiles3');
        this.map.addTilesetImage('nature_tiles4');
        this.map.addTilesetImage('ghost');
        
       
        
        this.bottomLayer = this.map.createLayer('bottomLayer');
        this.bottomLayer = this.map.createLayer('secondBottomCollideLayer');
        this.secondBottomLayer = this.map.createLayer('secondBottomLayer');
        this.topLayer = this.map.createLayer('topLayer');
        this.moreThenTopLayer = this.map.createLayer('moreThenTopLayer');
        this.moreMoreThenTopLayer = this.map.createLayer('moreMoreThenTopLayer');
        this.objectLayer = this.map.createLayer('objectLayer');
        
        //this.map.setCollisionByExclusion([0]);
        this.bottomLayer.resizeWorld();
        
        // OBJECT LAYER
        
              //HERE IS THE START POSITION
        this.map.createFromObjects('objectLayer',820 , '' ,0,true,false,respawn);
        
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        //FIRE
        /*fire = this.add.sprite(400,300,'fire');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fire);
        var fire_anim = fire.animations.add('fire_anim');
        fire.animations.play('fire_anim', 12, true);
        fire.body.collideWorldBounds = true;
        fire.body.immovable = true;*/
        
        
        //DOOR TO NEXT LEVEL
        doors2 = this.add.sprite(65,96,'doors2');
        doors2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doors2);
        var doors2_anim = doors2.animations.add('doors2_anim',[0,4,7,10]);
        doors2.animations.play('doors2_anim', 1, true);
        doors2.body.collideWorldBounds = true;
        doors2.body.immovable = true;
        
        
        //PORTALS
        portalToAjax = this.add.sprite(500   ,150,'portal1x');
        portalToAjax.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portalToAjax);
        portalToAjax.animations.add('portal1x_anim',[0,1,2,1,0]);
        portalToAjax.animations.play('portal1x_anim', 12, true);
        portalToAjax.body.collideWorldBounds = true;
        portalToAjax.body.immovable = true;
        
        text1 = this.game.add.bitmapText(500, 100, 'desyrel', 'Portal to Ajax', 30);
        text1.anchor.x = 0.5;
        text1.anchor.y = 0.5;
        
        
        
        portalToRest = this.add.sprite(1150   ,150,'portal1x');
        portalToRest.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portalToRest);
        portalToRest.animations.add('portal1x_anim',[0,1,2,1,0]);
        portalToRest.animations.play('portal1x_anim', 12, true);
        portalToRest.body.collideWorldBounds = true;
        portalToRest.body.immovable = true;
        
        text2 = this.game.add.bitmapText(1150, 100, 'desyrel', 'Portal to Rest and Application Server', 30);
        text2.anchor.x = 0.5;
        text2.anchor.y = 0.5;
        
        
        portalToCookie = this.add.sprite(250   ,470,'portal1x');
        portalToCookie.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portalToCookie);
        portalToCookie.animations.add('portal1x_anim',[0,1,2,1,0]);
        portalToCookie.animations.play('portal1x_anim', 12, true);
        portalToCookie.body.collideWorldBounds = true;
        portalToCookie.body.immovable = true;
        
        text3 = this.game.add.bitmapText(250, 420, 'desyrel', 'Portal to Cookies and Sessions', 30);
        text3.anchor.x = 0.5;
        text3.anchor.y = 0.5;
        
        portalToSecurity = this.add.sprite(1400 ,450,'portal1x');
        portalToSecurity.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portalToSecurity);
        portalToSecurity.animations.add('portal1x_anim',[0,1,2,1,0]);
        portalToSecurity.animations.play('portal1x_anim', 12, true);
        portalToSecurity.body.collideWorldBounds = true;
        portalToSecurity.body.immovable = true;
        
        text4 = this.game.add.bitmapText(1280, 400, 'desyrel', 'Portal to Sql Injection and XSS', 30);
        text4.anchor.x = 0.5;
        text4.anchor.y = 0.5;
        
        //PLAYER
        

        this.map.setCollisionBetween(1, 100000, true, 'topLayer');  //!!!
        
      
        
        //
        
        player = this.add.sprite(0,0,'player');
        player.anchor.setTo(0.5,0.5);
        
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        this.physics.arcade.enable(player);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
            
            
            
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(togglePause, this);
        
        
        
      
        
     
        
    },
    
    update:function(){
        
        this.physics.arcade.collide(player,this.moreMoreThenTopLayer);
        this.physics.arcade.collide(player,this.moreThenTopLayer);
        this.physics.arcade.collide(player,this.bottomLayer); 
        //this.physics.arcade.collide(player,this.secondBottomCollideLayer);
        //this.physics.arcade.collide(player,this.secondBottomLayer);
        
        this.physics.arcade.collide(player,this.topLayer);
        //this.physics.arcade.collide(player,fire);
        this.physics.arcade.collide(player,doors2,this.goToLevel2,null,this);
        this.physics.arcade.collide(player,portalToSecurity,this.goToLevelSecurityUpDownMap,null,this);
        this.physics.arcade.collide(player,portalToCookie,this.goToLevelCookieMainCamera,null,this);
        this.physics.arcade.collide(player,portalToAjax,this.goToLevelAgajax,null,this);
        this.physics.arcade.collide(player,portalToRest,this.goToLevelServer,null,this);
        
        
        
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        
        if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;
                        element.value-=0.5;
        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;  
            element.value+=0.5;
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
    },
    
    
    //RESET PLAYER TO GIVEN POSITION
    resetPlayer :function(){
        player.reset(100,560);
    },
    
    //SPAWN FUNCTION
    spawn : function(){
        element = document.getElementById("HPBar");
        alert(element.value)
        respawn.forEach(function(spawnPoint){
            player.reset(spawnPoint.x,spawnPoint.y);
        },this);
    },
    
    
    //WHEN HIT FIRE JUMP TO LEVEL2 MAP
    goToLevel2 :function(){
        this.state.start('Level2');
    },
    
    goToLevelSecurityUpDownMap : function(){
        this.state.start('LevelSecurityUpDownMap');
    },
    
    goToLevelCookieMainCamera : function(){
        this.state.start('LevelCookieMainCamera');
    },
    
    goToLevelAgajax : function(){
        this.state.start('Agajax');
    },
    
    goToLevelServer : function(){
        this.state.start('LevelServer');
    },
    
    
};

function togglePause() {

    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

}