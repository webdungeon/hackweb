Phaser.Tilemap.prototype.setCollisionBetween = function (start, stop, collides, layer, recalculate) {

	if (collides === undefined) { collides = true; }
	if (layer === undefined) { layer = this.currentLayer; }
	if (recalculate === undefined) { recalculate = true; }

	layer = this.getLayer(layer);

	for (var index = start; index <= stop; index++)
	{
		if (collides)
		{
			this.collideIndexes.push(index);
		}
		else
		{
			var i = this.collideIndexes.indexOf(index);

			if (i > -1)
			{
				this.collideIndexes.splice(i, 1);
			}
		}
	}

	for (var y = 0; y < this.layers[layer].height; y++)
	{
		for (var x = 0; x < this.layers[layer].width; x++)
		{
			var tile = this.layers[layer].data[y][x];

			if (tile && tile.index >= start && tile.index <= stop)
			{
				if (collides)
				{
					tile.setCollision(true, true, true, true);
				}
				else
				{
					tile.resetCollision();
				}

				tile.faceTop = collides;
				tile.faceBottom = collides;
				tile.faceLeft = collides;
				tile.faceRight = collides;
			}
		}
	}

	if (recalculate)
	{
		//  Now re-calculate interesting faces
		this.calculateFaces(layer);
	}

	return layer;

};

Game.LevelSecurityMainCamera = function(game){
   
};

var map;
var layer;
var music;
var fire;
var respawn;

var player;
var controls = {};
var playerSpeed = 500;
var jumpTimer = 0;
var pauseKey;

var text;
 

Game.LevelSecurityMainCamera.prototype = {
    create: function(){
        this.stage.backgroundColor = '#3A5963';
        pausedgame = false;
        
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('SecurityMainCamera');             
       
        //this.map.addTilesetImage('terrain3');
        //this.map.addTilesetImage('terrainLava');
        //this.map.addTilesetImage('volcano1');
        //this.map.addTilesetImage('volcano2');
        this.map.addTilesetImage('lava7');

                            
        this.groundLayer = this.map.createLayer('groundLayer');  
        this.layer2 = this.map.createLayer('layer2'); 

        this.groundLayer.resizeWorld();
        
        
        // OBJECT LAYER
        
              //HERE IS THE START POSITION
        
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        
        // FIRE SPRITE
        fireAnim1 = this.add.sprite(780,220,'fireAnim1');
        fireAnim1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fireAnim1);
        fireAnim1.animations.add('fireAnim1_anim');
        fireAnim1.animations.play('fireAnim1_anim', 12, true);
        fireAnim1.body.collideWorldBounds = true;
        fireAnim1.body.immovable = true;
        
        fireAnim1 = this.add.sprite(200,220,'fireAnim1');
        fireAnim1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fireAnim1);
        fireAnim1.animations.add('fireAnim1_anim');
        fireAnim1.animations.play('fireAnim1_anim', 12, true);
        fireAnim1.body.collideWorldBounds = true;
        fireAnim1.body.immovable = true;
        
        fireAnim1 = this.add.sprite(200,700,'fireAnim1');
        fireAnim1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fireAnim1);
        fireAnim1.animations.add('fireAnim1_anim');
        fireAnim1.animations.play('fireAnim1_anim', 12, true);
        fireAnim1.body.collideWorldBounds = true;
        fireAnim1.body.immovable = true;
        
        fireAnim1 = this.add.sprite(1000,450,'fireAnim1');
        fireAnim1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fireAnim1);
        fireAnim1.animations.add('fireAnim1_anim');
        fireAnim1.animations.play('fireAnim1_anim', 12, true);
        fireAnim1.body.collideWorldBounds = true;
        fireAnim1.body.immovable = true;
        
        fireAnim1 = this.add.sprite(700,850,'fireAnim1');
        fireAnim1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fireAnim1);
        fireAnim1.animations.add('fireAnim1_anim');
        fireAnim1.animations.play('fireAnim1_anim', 12, true);
        fireAnim1.body.collideWorldBounds = true;
        fireAnim1.body.immovable = true;
        
        fireAnim1 = this.add.sprite(350,450,'fireAnim1');
        fireAnim1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fireAnim1);
        fireAnim1.animations.add('fireAnim1_anim');
        fireAnim1.animations.play('fireAnim1_anim', 12, true);
        fireAnim1.body.collideWorldBounds = true;
        fireAnim1.body.immovable = true;
        
        //Portal1 sprite
        securityPortal1 = this.add.sprite(200,500,'securityPortal1');
        securityPortal1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(securityPortal1);
        securityPortal1.animations.add('securityPortal1_anim',[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2]);
        securityPortal1.animations.play('securityPortal1_anim', 12, true);
        securityPortal1.body.collideWorldBounds = true;
        securityPortal1.body.immovable = true;
        
        securityPortal2 = this.add.sprite(1500,500,'securityPortal1');
        securityPortal2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(securityPortal2);
        securityPortal2.animations.add('securityPortal2_anim',[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2]);
        securityPortal2.animations.play('securityPortal2_anim', 12, true);
        securityPortal2.body.collideWorldBounds = true;
        securityPortal2.body.immovable = true;
        
        text1 = this.game.add.bitmapText(200, 400, 'desyrel', 'Portal to Secured WebPage Dungeon', 22);
        text1.anchor.x = 0.5;
        text1.anchor.y = 0.5;
        
        text2 = this.game.add.bitmapText(1475, 400, 'desyrel', 'Portal to NOT Secured WebPage Dungeon', 22);
        text2.anchor.x = 0.5;
        text2.anchor.y = 0.5;
        
        
        
        
        //PLAYER
    
        
        //this.map.setCollisionBetween(1, 100000, true, 'topLayer'); 

        
        player = this.add.sprite(830,600,'player');
        player.anchor.setTo(0.5,0.5);
        
        
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        //this.physics.arcade.enable(player);
        this.game.physics.enable(player, Phaser.Physics.ARCADE);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
            
            
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(togglePause, this);
        
 
        
    },
    
    
    update:function(){
         
        
        this.physics.arcade.collide(player,this.groundLayer);      

     
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        
        if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;       
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
               
        
        
    },
    
    
    //SPAWN FUNCTION
    spawn : function(){
        respawn.forEach(function(spawnPoint){
            player.reset(spawnPoint.x,spawnPoint.y);
        },this);
    },
    
    
    //WHEN HIT FIRE JUMP TO LEVEL1 MAP
    goToLevel1 :function(){
        this.state.start('Level1');
    },
    
    
    
};


function togglePause() {

    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

}



