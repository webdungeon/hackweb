
exclamMark = function(index,game,x,y){
    this.mark =  game.add.sprite(x,y,'exclamationMark');
    this.mark.anchor.setTo(0.5,0.5);
    this.mark.name = index.toString();
    game.physics.enable(this.mark,Phaser.Physics.ARCADE);
    this.mark.body.immovable = true;
    this.mark.body.collideWorldBounds = true;
    this.mark.body.allowGravity = false;
    
    this.markTween = game.add.tween(this.mark).to({
        y: this.mark.y + 20
    },2000,'Linear',true,0,100,true);
};

EnemyGhost = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.ghost =  game.add.sprite(x,y,'ghostMonster');
    this.ghost.anchor.setTo(0.5,0.5);
    this.ghost.name = index.toString();
    //game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.cookie3);
        game.physics.enable(this.ghost,Phaser.Physics.ARCADE);
        this.ghost.animations.add('ghost_anim');
        this.ghost.animations.add('ghost_anim_walk_up',[3,4,5]);
        this.ghost.animations.add('ghost_anim_walk_down',[0,1,2]);
        this.ghost.animations.add('ghost_anim_atack',[33,35,35,37,15]);
        //this.ghost.animations.play('ghost_anim', 8, true);
        this.ghost.body.collideWorldBounds = true;
        this.ghost.body.immovable = true;
        this.ghost.body.allowGravity = false;
    
    //this.cookie3.body.immovable = true;
    //this.cookie3.body.collideWorldBounds = true;
    //this.cookie3.body.allowGravity = false;

//    this.ghostTween = game.add.tween(this.ghost).to({
//        y: this.ghost.y + 70,
//    },2000,'Linear',true,0,100,true); 
    
    //game.physics.arcade.moveToXY(this.cookie3, player.x,player.y, 200);
    
    
    //this.cookie3.reset(x,y);
};

EnemyMummy = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.mummy =  game.add.sprite(x,y,'mummyMonster');
    this.mummy.anchor.setTo(0.5,0.5);
    this.mummy.name = index.toString();
    //game.physics.enable(this.mummy,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.cookie3);
        game.physics.enable(this.mummy,Phaser.Physics.ARCADE);
        this.mummy.animations.add('mummy_anim');
        this.mummy.animations.add('mummy_anim_walk_left',[4,3,2,1,0,14,13,12,11,10,24,23,22,21,20,34,33,32]);
        this.mummy.animations.add('mummy_anim_walk_right',[5,6,7,8,9,15,16,17,18,19,25,26,27,28,29,35,36,37]);
        //this.mummy.animations.add('mummy_anim_atack',[33,35,35,37,15]);
        this.mummy.animations.play('mummy_anim_walk_left', 8, true);
        this.mummy.body.collideWorldBounds = true;
        this.mummy.body.immovable = true;
        this.mummy.body.allowGravity = false;
    
    //this.cookie3.body.immovable = true;
    //this.cookie3.body.collideWorldBounds = true;
    //this.cookie3.body.allowGravity = false;

//    this.mummyTween = game.add.tween(this.mummy).to({
//        x: this.mummy.x + 100,
//    },2000,'Linear',true,0,100,true); 
    
    //game.physics.arcade.moveToXY(this.mummy, player.x,player.y, 200);
    
    //this.mummy.reset(x,y);
};

enemysv2keleton = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.skeleton =  game.add.sprite(x,y,'skeletonMonster');
    this.skeleton.anchor.setTo(0.5,0.5);
    this.skeleton.name = index.toString();
    //game.physics.enable(this.skeleton,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.skeleton);
        game.physics.enable(this.skeleton,Phaser.Physics.ARCADE);
        this.skeleton.animations.add('skeleton_anim_idle',[182,183,184,185,186,187,186,185,184,183,182]);
        this.skeleton.animations.add('skeleton_anim_attack_up',[208,209,210,211,212,213,214,215,216,217,218,219,220]);
        this.skeleton.animations.add('skeleton_anim_attack_left',[221,222,223,224,225,226,227,228,229,230,231,232,233]);
        this.skeleton.animations.add('skeleton_anim_attack_down',[234,235,236,237,238,239,240,241,242,243,244,245,246]);
        this.skeleton.animations.add('skeleton_anim_attack_right',[247,248,249,250,251,252,253,254,255,256,257,258,259]);
        //this.mummy.animations.add('skeleton_anim_atack',[33,35,35,37,15]);
        this.skeleton.animations.play('skeleton_anim_idle', 8, true);
        this.skeleton.body.collideWorldBounds = true;
        this.skeleton.body.immovable = true;
        this.skeleton.body.allowGravity = false;

//    this.skeletonTween = game.add.tween(this.skeleton).to({
//        x: this.skeleton.x + 100,
//    },2000,'Linear',true,0,100,true); 
    
    //game.physics.arcade.moveToXY(this.skeleton, player.x,player.y, 200);
    
    //this.skeleton.reset(x,y);
};

newKey = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.key =  game.add.sprite(x,y,'key');
    this.key.anchor.setTo(0.5,0.5);
    this.key.name = index.toString();
        game.physics.enable(this.key,Phaser.Physics.ARCADE);
        //this.key.animations.add('key_anim',[0]);
        //this.key.animations.play('key_anim', 8, true);
        this.key.body.collideWorldBounds = true;
        this.key.body.immovable = true;
        this.key.body.allowGravity = false;
 
    this.keyTween = game.add.tween(this.key).to({
        y: this.key.y + 50,
    },2000,'Linear',true,0,100,true); 
    
};

PieceOfInformationGhost = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.piece =  game.add.sprite(x,y,'information');
    this.piece.anchor.setTo(0.5,0.5);
    this.piece.name = index.toString();
        game.physics.enable(this.piece,Phaser.Physics.ARCADE);
        this.piece.body.collideWorldBounds = true;
        this.piece.body.immovable = true;
        this.piece.body.allowGravity = false;
 
    this.pieceTween = game.add.tween(this.piece).to({
        y: this.piece.y + 20,
    },2000,'Linear',true,0,100,true); 
    
};

PieceOfInformationMummy = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.piece =  game.add.sprite(x,y,'information');
    this.piece.anchor.setTo(0.5,0.5);
    this.piece.name = index.toString();
        game.physics.enable(this.piece,Phaser.Physics.ARCADE);
        this.piece.body.collideWorldBounds = true;
        this.piece.body.immovable = true;
        this.piece.body.allowGravity = false;
 
    this.pieceTween = game.add.tween(this.piece).to({
        y: this.piece.y + 20,
    },2000,'Linear',true,0,100,true); 
    
};

PieceOfInformationSkeleton = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.piece =  game.add.sprite(x,y,'information');
    this.piece.anchor.setTo(0.5,0.5);
    this.piece.name = index.toString();
        game.physics.enable(this.piece,Phaser.Physics.ARCADE);
        this.piece.body.collideWorldBounds = true;
        this.piece.body.immovable = true;
        this.piece.body.allowGravity = false;
 
    this.pieceTween = game.add.tween(this.piece).to({
        y: this.piece.y + 20,
    },2000,'Linear',true,0,100,true); 
    
};



Phaser.Tilemap.prototype.setCollisionBetween = function (start, stop, collides, layer, recalculate) {

	if (collides === undefined) { collides = true; }
	if (layer === undefined) { layer = this.currentLayer; }
	if (recalculate === undefined) { recalculate = true; }

	layer = this.getLayer(layer);

	for (var index = start; index <= stop; index++)
	{
		if (collides)
		{
			this.collideIndexes.push(index);
		}
		else
		{
			var i = this.collideIndexes.indexOf(index);

			if (i > -1)
			{
				this.collideIndexes.splice(i, 1);
			}
		}
	}

	for (var y = 0; y < this.layers[layer].height; y++)
	{
		for (var x = 0; x < this.layers[layer].width; x++)
		{
			var tile = this.layers[layer].data[y][x];

			if (tile && tile.index >= start && tile.index <= stop)
			{
				if (collides)
				{
					tile.setCollision(true, true, true, true);
				}
				else
				{
					tile.resetCollision();
				}

				tile.faceTop = collides;
				tile.faceBottom = collides;
				tile.faceLeft = collides;
				tile.faceRight = collides;
			}
		}
	}

	if (recalculate)
	{
		//  Now re-calculate interesting faces
		this.calculateFaces(layer);
	}

	return layer;

};




Game.LevelSecurityLeftRightMap = function(game){
   
};

var map;
var layer;
var music;
var fire;
var respawn;

var player;
var controls = {};
var playerSpeed = 500;
var jumpTimer = 0;
var pauseKey;

var text;

var gold =0;

var npc1_v2;
var npc1_nr_v2 = 90;
var Npc1Dialog_v2 = {pos :0 , dialog: null};
var aliasDialog ={ pos: 0 , dialog: null};
var npc2_v2;
var npc2_nr_v2;

var enemysv2=[];//ghosts
var enemiesGhost_Life=[];
var cookies=[];//ghosts
var enemiesMummyv2=[];//mummy
var enemiesMummyv2_Life=[];
var nrenemies=15;//ghosts
var nrenemiesMummyv2 = 10;//mummy
var enemiesRemain=15;//ghosts

var enemysv2Copy=[];//ghosts
var cookiesCopy=[];//ghosts
var nrenemiesCopy=15;//ghosts
var enemiesCopyRemain=15;//ghosts

var enemysv2CopyMummy=[];//mummy
var cookiesCopyMummy=[];//mummy
var nrenemiesCopyMummy=10;//mummy
var enemiesCopyRemainMummy=10;//mummy

//Skeleton
var enemiesSkeletonv2=[];
var enemiesSkeletonv2Copy=[];
var nrenemiesSkeletonv2 = 5;
var nrenemiesSkeletonv2Copy = 5;
var enemiesSkeletonv2_Life=[];

var exclamationMark1;
var exclamationMark2;

var playerCopy;

var portal1x;
var portal_top1;
var portal2x;
var porta2_top1;

var element;
var isPlayer=false;

var chest=[];
var chestCopy=[];
var chest_life=[];

var bullets=[];
var bullet=[];
var fireRate = 1500;
var nextFire = 0;


var openChestTime = 1000;
var openChest = 0;
var chestOpened=[];

var waitingTime=0;
var checkDeadPlayer=false;
var deadAnimPlayed = false;

var keys=[];
var keysCopy=[];
var nrKeys=5;
var nrCopyKeys=5;

var nrKeysUntilNow = 0;
var nrPiecesInfoUntilNow = 0;

var keysText;
var numberOfInfoText;

var ghostHP=[];
var mummyHP=[];
var skeletonHP=[];
var chestHP=[];

var myHP = 0;
var pieceOfInfoMummy=[];
var pieceOfInfoGhost=[];
var pieceOfInfoSkeleton=[];

var pieceOfInfoMummyCopy=[];
var pieceOfInfoGhostCopy=[];
var pieceOfInfoSkeletonCopy=[];

var ghostKilled=[];
var mummyKilled=[];
var skeletonKilled=[];

//inventory
var popup;
var tween = null;
var button;

var killMonsters;
var nrKillMobItems;
var nrKillItemsLeft = 4;

var openChest;
var nrOpenChestItems;
var nrOpenItemsLeft = 2;

var nrTeleportSpellItems;
var nrTeleportsLeft = 2;
var teleportItem;

var addCoinsSpell;
var nrAddCoinsSpellItems;
var nrAddCoinsLeft = 5;


var pressed = false;
var waitingTime1 = 0;
var waitingTime2 = 0;
var waitingTime3 = 0;
var waitingTime4 = 0;

var canTakeInfoSkeleton=[];
 

Game.LevelSecurityLeftRightMap.prototype = {
    create: function(){
        var i;
        for(i=0;i<5;i++){
            canTakeInfoSkeleton[i] = false;
        }
        
        for(i=0;i<nrenemies + nrenemiesCopy;i++){
            enemiesGhost_Life[i]=50;
            ghostKilled[i] = false;
        }
        for(i=0;i<nrenemiesMummyv2 + nrenemiesCopyMummy;i++){
            enemiesMummyv2_Life[i]=50;
            mummyKilled[i] = false;
        }
        for(i=0;i<5;i++){
            chest_life[i] = 200;
        }
        for(i=0;i<5;i++){
            enemiesSkeletonv2_Life[i]=25;
            skeletonKilled[i] = false;
        }
        
        //chests initial closed
        for(i=0;i<5;i++){
            chestOpened[i]=false;
        }
        
        
        this.stage.backgroundColor = '#3A5963';
        pausedgame = false;
        
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        //respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('leftRightMap');             
       
        this.map.addTilesetImage('ground9');
        this.map.addTilesetImage('wallMapHacked');


                            
        this.groundLayer = this.map.createLayer('groundLayer');  
        this.wallLayer = this.map.createLayer('wallLayer'); 

        this.groundLayer.resizeWorld();
        
        
        // OBJECT LAYER
        
        
        //Afisare numar key 
        keysText = this.game.add.text(50,50,"You have 0 keys",{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
        //afisare numar de bucati de informatii
        numberOfInfoText = this.game.add.text(50,50,"You have 0 pieces of informations",{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
        //afisare HP GHOSTS
        for(i=0;i<nrenemies;i++){
            ghostHP[i] = this.game.add.text(50,50,enemiesGhost_Life[i],{
            font: "12px Arial",
            fill: "#ff0044",
            align: "left"
            });
        }
        
        //AFISARE HP MUMMY
        for(i=0;i<nrenemiesMummyv2;i++){
            mummyHP[i] = this.game.add.text(50,50,enemiesMummyv2_Life[i],{
            font: "12px Arial",
            fill: "#ff0044",
            align: "left"
            });
        }
        
        //Afisare hp skeleton
        for(i=0;i<5;i++){
            skeletonHP[i] = this.game.add.text(50,50,enemiesSkeletonv2_Life[i],{
            font: "12px Arial",
            fill: "#ff0044",
            align: "left"
            });
        }
        
        //afisare hp chests
        for(i=0;i<5;i++){
            chestHP[i] = this.game.add.text(50,50,chest_life[i],{
            font: "12px Arial",
            fill: "#ff0044",
            align: "left"
            });
        }
        
        //afisare my hp
            myHP= this.game.add.text(50,50,"100",{
            font: "12px Arial",
            fill: "#00ff44",
            align: "left"
            });
        
        
        
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        //NPCs
        
        //NPC 1
        npc1_v2 = this.add.sprite(150,6300,'npcbot1');
        npc1_v2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(npc1_v2);
        //fireAnim1.animations.add('fireAnim1_anim');
        //fireAnim1.animations.play('fireAnim1_anim', 12, true);
        npc1_v2.body.collideWorldBounds = true;
        npc1_v2.body.immovable = true;
        getDialog(npc1_nr_v2,Npc1Dialog_v2);
        
        //NPC 2
        npc2_v2 = this.add.sprite(890,6300,'npcbot1');
        npc2_v2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(npc2_v2);
        //fireAnim1.animations.add('fireAnim1_anim');
        //fireAnim1.animations.play('fireAnim1_anim', 12, true);
        npc2_v2.body.collideWorldBounds = true;
        npc2_v2.body.immovable = true;
        
//        //PORTAL TO NEXT CAMERA 2
        
        portal_top1 = this.add.sprite(400   ,200,'portal1x');
        portal_top1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal_top1);
        portal_top1.animations.add('portal1x_anim',[0,1,2,1,0]);
        portal_top1.animations.play('portal1x_anim', 12, true);
        portal_top1.body.collideWorldBounds = true;
        portal_top1.body.immovable = true;
        
        portal_top2 = this.add.sprite(1140,200,'portal1x');
        portal_top2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal_top2);
        portal_top2.animations.add('portal2x_anim',[0,1,2,1,0]);
        portal_top2.animations.play('portal2x_anim', 12, true);
        portal_top2.body.collideWorldBounds = true;
        portal_top2.immovable = true;
        
        //fireworks
        
         firework1 = this.add.sprite(175,350,'firework');
        firework1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(firework1);
        firework1.animations.add('portal1x_anim');
        firework1.animations.play('portal1x_anim', 25, true);
        firework1.body.collideWorldBounds = true;
        firework1.body.immovable = true;
        
        firework2 = this.add.sprite(915,350,'firework');
        firework2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(firework2);
        firework2.animations.add('portal2x_anim');
        firework2.animations.play('portal2x_anim', 25, true);
        firework2.body.collideWorldBounds = true;
        firework2.immovable = true;
        
        firework3 = this.add.sprite(600,350,'firework');
        firework3.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(firework3);
        firework3.animations.add('portal1x_anim');
        firework3.animations.play('portal1x_anim', 25, true);
        firework3.body.collideWorldBounds = true;
        firework3.body.immovable = true;
        
        firework4 = this.add.sprite(1345,350,'firework');
        firework4.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(firework4);
        firework4.animations.add('portal2x_anim');
        firework4.animations.play('portal2x_anim', 25, true);
        firework4.body.collideWorldBounds = true;
        firework4.immovable = true;
        
        
        //CHESTS
        
        var incr1 = 1500;
        var skeletons_until_now=0;
        var skeletons_copy_until_now=0;
        var k;
        for(i=0;i<5;i++){
            //CHESTS
            chest[i] = this.add.sprite(400,incr1,'chest1');
            chest[i].anchor.setTo(0.5,0.5);
            this.physics.arcade.enable(chest[i]);
            chest[i].animations.add('chest_open_anim',[0,1,2,3,2,1]);
            chest[i].animations.add('chest_opened_anim',[3]);
            //chest[i].animations.play('chest_open_anim', 7, true);
            chest[i].body.collideWorldBounds = true;
            chest[i].body.immovable = true;
            //COPY CHESTS
            chestCopy[i] = this.add.sprite(1140,incr1,'chest1');
            chestCopy[i].anchor.setTo(0.5,0.5);
            this.physics.arcade.enable(chestCopy[i]);
            chestCopy[i].animations.add('chest_copy_open_anim',[0,1,2,3,2,1]);
            chestCopy[i].animations.add('chest_copy_opened_anim',[3]);
            //chestCopy[i].animations.play('chest_copy_open_anim', 7, true);
            chestCopy[i].body.collideWorldBounds = true;
            chestCopy[i].body.immovable = true;
            
            incr1 = incr1 + 1000;
        }
        
        //SKELETONS
        for(i=0;i<5;i++){
//            enemy=new enemysv2keleton(i*3+0,this.game,chest[i].x-80,chest[i].y);
//            enemiesSkeletonv2.push(enemy);
//            enemy=new enemysv2keleton(i*3+1,this.game,chest[i].x+80,chest[i].y);
//            enemiesSkeletonv2.push(enemy);
            enemy=new enemysv2keleton(i,this.game,chest[i].x+60,chest[i].y);
            enemiesSkeletonv2.push(enemy);
        }
        //SKELETONS COPY
        for(i=0;i<5;i++){
//            enemy=new enemysv2keleton(nrenemiesSkeletonv2 + i*3+0,this.game,chest[i].x-80,chest[i].y+400);
//            enemiesSkeletonv2.push(enemy);
//            enemy=new enemysv2keleton(nrenemiesSkeletonv2 + i*3+1,this.game,chest[i].x+80,chest[i].y+400);
//            enemiesSkeletonv2.push(enemy);
            enemy=new enemysv2keleton(nrenemiesSkeletonv2 + i,this.game,chest[i].x+800,chest[i].y);
            enemiesSkeletonv2.push(enemy);
        }
        
        
  
        
        
        //PLAYER
    
        
        this.map.setCollisionBetween(1, 100000, true, 'wallLayer'); 

        
        player = this.add.sprite(300,6300,'soldatPlayer');
        player.anchor.setTo(0.5,0.5);
        
        
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        
        player.animations.add('walkdown',[240,241,242,243,244,245,246,247,248],13,true);
        player.animations.add('walkup',[192,193,194,195,196,197,198,199,200],13,true);
        player.animations.add('walkleft',[216,217,218,219,220,221,222,223,224],13,true);
        player.animations.add('walkright',[264,266,267,268,269,270,271,272],13,true);
        player.animations.add('attackUp',[529,532,535,538,541,544],20,true);
        player.animations.add('attackLeft',[601,604,607,610,613,616],20,true);
        player.animations.add('attackDown',[673,676,679,682,685],20,true);
        player.animations.add('attackRight',[745,748,751,754,757],20,true);
        
        player.animations.add('dead',[480,481,482,483,484,485],10,false,true);
        player.animations.add('idle',[673,676],4,true);
        
//        deadAnimation.killOnComplete = true;
//        deadAnimation.onComplete.add(function() {
//        console.log("dadada");
//        }, player);
       
       
        //player.events.onAnimationComplete.add(this.animCompleteCallback, this);
        
        
        
        
        //this.physics.arcade.enable(player);
        this.game.physics.enable(player, Phaser.Physics.ARCADE);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        //PlayerCopy
        
        playerCopy = this.add.sprite(1040,6300,'soldatPlayer');
        playerCopy.anchor.setTo(0.5,0.5);
        
        
        //PLAYER SPAWNS AT THE SET POSITION
        //this.spawn();
        
        
        playerCopy.animations.add('walkdown',[240,241,242,243,244,245,246,247,248],13,true);
        playerCopy.animations.add('walkup',[192,193,194,195,196,197,198,199,200],13,true);
        playerCopy.animations.add('walkleft',[216,217,218,219,220,221,222,223,224],13,true);
        playerCopy.animations.add('walkright',[264,266,267,268,269,270,271,272],13,true);
        playerCopy.animations.add('attackUp',[529,532,535,538,541,544],20,true);
        playerCopy.animations.add('attackLeft',[601,604,607,610,613,616],20,true);
        playerCopy.animations.add('attackDown',[673,676,679,682,685],20,true);
        playerCopy.animations.add('attackRight',[745,748,751,754,757],20,true);
        playerCopy.animations.add('dead',[480,481,482,483,484,485],13,false,true);
        playerCopy.animations.add('idle',[336,337,338,339,340,341,340,339,338,337],4,true);
        
//        deadAnimationCopy.killOnComplete = true;
//        deadAnimationCopy.onComplete.add(function() {
//        console.log("dadada");
//        }, playerCopy);
        
        
        //this.physics.arcade.enable(player);
        this.game.physics.enable(playerCopy, Phaser.Physics.ARCADE);
        this.camera.follow(playerCopy);
        playerCopy.body.collideWorldBounds = true;
        
        
        //CONTROLS
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
            shoot : this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR),
            openInventory : this.input.keyboard.addKey(Phaser.Keyboard.I),
            one : this.input.keyboard.addKey(Phaser.Keyboard.ONE),
            two :  this.input.keyboard.addKey(Phaser.Keyboard.TWO),
            three :  this.input.keyboard.addKey(Phaser.Keyboard.THREE),
            four : this.input.keyboard.addKey(Phaser.Keyboard.FOUR),
            
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(this.togglePause, this);
        
        
        //Monsters
        
        //ghosts
        for(i=0; i<nrenemies; i++)
            {
                randomx=Math.floor(Math.random() * 550) + 150;
                randomy=Math.floor(Math.random() * 4500) + 1300;
                enemy=new EnemyGhost(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                enemysv2.push(enemy);
                
            }
        var j = 0;    
            for(i=nrenemies; i<nrenemies + nrenemiesCopy; i++)
            {
                enemy=new EnemyGhost(i,this.game,enemysv2[j].originx+740,enemysv2[j].originy);
                enemysv2.push(enemy);
                j++;
                
            }
        
        //making mummies
        for(j=0;j<nrenemiesMummyv2;j++){
            randomx=Math.floor(Math.random() * 550) + 150;
                randomy=Math.floor(Math.random() * 4500) + 1300;
                enemy=new EnemyMummy(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                enemiesMummyv2.push(enemy);
        }
        
        j=0;
        for(i=nrenemiesMummyv2; i<nrenemiesMummyv2 + nrenemiesCopyMummy; i++)
            {
                enemy=new EnemyMummy(i,this.game,enemiesMummyv2[j].originx + 740,enemiesMummyv2[j].originy);
                enemiesMummyv2.push(enemy);
//                enemy.mummy.alpha = 0;
                j++;
                
            }
        
       
        
//        exclamationMark1 = new exclamMark(1,this.game,135,50);
//        exclamationMark2 = new exclamMark(2,this.game,135,460);
        
        for(i=0;i<nrenemiesSkeletonv2;i++){
           bullets[i] = this.game.add.group();
           bullets[i].enableBody = true;
           bullets[i].physicsBodyType = Phaser.Physics.ARCADE;
           bullets[i].createMultiple(30,'arrow',0,false);                //check nr of bullets
           //bullets[i].setAll('anchor.x',0.5);
           //bullets[i].setAll('anchor.y',0.5);
        
           bullets[i].callAll('anchor.setTo', 'anchor', 0.5, 1.0);
           bullets[i].callAll('events.onOutOfBounds.add','events.onOutOfBounds', resetBullet);
           bullets[i].setAll('checkWorldBounds', true);
        }
        
        //KEY CREATION INVIZ
        for(i=0;i<5;i++){
            keys[i] = new newKey(i,this.game,chest[i].x + 50,chest[i].y);
            keys[i].key.alpha = 0;
            
        }
        
        //Piece of INFORMATIONS CREATION INVIZ
        for(i=0;i<nrenemiesMummyv2;i++){
            pieceOfInfoMummy[i] = new PieceOfInformationMummy(i,this.game,enemiesMummyv2[i].originx,enemiesMummyv2[i].originy);
            pieceOfInfoMummy[i].piece.alpha = 0;
        }
        for(i=0;i<nrenemies;i++){
            pieceOfInfoGhost[i] = new PieceOfInformationGhost(i,this.game,enemysv2[i].originx,enemysv2[i + nrenemies].originy);
            pieceOfInfoGhost[i].piece.alpha = 0;
        }
        for(i=0;i<5;i++){
            pieceOfInfoSkeleton[i] = new PieceOfInformationSkeleton(i,this.game,enemiesSkeletonv2[i].originx,enemiesSkeletonv2[i].originy);
            pieceOfInfoSkeleton[i].piece.alpha = 0;
        }
        
        
        //INVENTORY
        
        button = this.game.add.button(this.game.camera.x, this.game.camera.y, 'button', this.openWindow, this, 2, 1, 0);
    button.input.useHandCursor = true;

    //  You can drag the pop-up window around
    popup = this.game.add.sprite(this.game.camera.x, this.game.camera.y, 'inventory');
    popup.alpha = 0.8;
    popup.anchor.set(0.5);
    popup.inputEnabled = true;
    popup.input.enableDrag();

    //  Position the close button to the top-right of the popup sprite (minus 8px for spacing)
    var pw = (popup.width / 2) - 40;
    var ph = (popup.height / 2) - 8;

    //  And click the close button to close it down again
    var closeButton = this.game.make.sprite(pw, -ph, 'close');
    closeButton.inputEnabled = true;
    closeButton.input.priorityID = 1;
    closeButton.input.useHandCursor = true;
    closeButton.events.onInputDown.add(this.closeWindow, this);
    
        
    //ADD KILL MONSTERS SPELL
    killMonsters = this.game.make.sprite(pw-490 ,-ph+20 ,'killMonsters');
    killMonsters.inputEnabled = true;
    killMonsters.input.priorityID = 2;
    killMonsters.input.useHandCursor = true;
    killMonsters.events.onInputDown.add(this.killMonstersFunction, this);
      //number of kill mob items
       nrKillMobItems = this.game.add.text(pw-430,-ph+75,nrKillItemsLeft,{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
        
    //add open chest spell
    openChest = this.game.make.sprite(pw-355 ,-ph+20 ,'chestSpell');
    openChest.inputEnabled = true;
    openChest.input.priorityID = 2;
    openChest.input.useHandCursor = true;
    openChest.events.onInputDown.add(this.openChestFunction, this);
        //number of open chests items
        nrOpenChestItems = this.game.add.text(pw-295,-ph+75,nrOpenItemsLeft,{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
    //add teleport Spell
    teleportSpell = this.game.make.sprite(pw-240 ,-ph+24 ,'teleportSpell');
    teleportSpell.inputEnabled = true;
    teleportSpell.input.priorityID = 2;
    teleportSpell.input.useHandCursor = true;
    teleportSpell.events.onInputDown.add(this.teleportFunction, this);
        //number of teleprt spell items
        nrTeleportSpellItems = this.game.add.text(pw-160,-ph+75,nrTeleportsLeft,{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
    //Add add coins button
    addCoinsSpell = this.game.make.sprite(pw-110 ,-ph+24 ,'rsz_banut');
    addCoinsSpell.inputEnabled = true;
    addCoinsSpell.input.priorityID = 2;
    addCoinsSpell.input.useHandCursor = true;
    addCoinsSpell.events.onInputDown.add(this.addCoinsFunction, this);
        //number of add coins spell items
        nrAddCoinsSpellItems = this.game.add.text(pw-40,-ph+75,nrAddCoinsLeft,{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
        
        //  Add the "close button" to the popup window image
    popup.addChild(closeButton);
    popup.addChild(killMonsters);
    popup.addChild(nrKillMobItems);
    popup.addChild(openChest);
    popup.addChild(nrOpenChestItems);
    popup.addChild(teleportSpell);
    popup.addChild(nrTeleportSpellItems);
    popup.addChild(addCoinsSpell);
    popup.addChild(nrAddCoinsSpellItems);

    //  Hide it awaiting a click
    popup.scale.set(0);
        
    },
    
    
    update:function(){
        button.x = this.game.camera.x + 1300;
        button.y = this.game.camera.y;  
        popup.x = this.game.camera.x + 750 ;
        popup.y = this.game.camera.y + 300 ;
        //afisarea numarului de key
        keysText.x = this.game.camera.x;
        keysText.y = this.game.camera.y;
        keysText.setText("[NUMBER OF KEYS] " + nrKeysUntilNow);
        
        //afisarea numarului de bucati de informatii
        numberOfInfoText.x = this.game.camera.x;
        numberOfInfoText.y = this.game.camera.y + 30;
        numberOfInfoText.setText("[NUMBER OF DATA PIECES] " + nrPiecesInfoUntilNow);
        
        
        //Afisarea numarului de iteme din inventar
        nrOpenChestItems.setText(nrOpenItemsLeft);
        nrTeleportSpellItems.setText(nrTeleportsLeft);
        nrKillMobItems.setText(nrKillItemsLeft);
        nrAddCoinsSpellItems.setText(nrAddCoinsLeft);
        
        //urmarirea documentelor
        for(i=0;i<nrenemies;i++){
            pieceOfInfoGhost[i].piece.x = enemysv2[i].ghost.x;
            pieceOfInfoGhost[i].piece.y = enemysv2[i].ghost.y;
        }
        for(i=0;i<nrenemiesMummyv2;i++){
            pieceOfInfoMummy[i].piece.x = enemiesMummyv2[i].mummy.x;
            pieceOfInfoMummy[i].piece.y = enemiesMummyv2[i].mummy.y;
        }
        for(i=0;i<5;i++){
            pieceOfInfoSkeleton[i].piece.x = enemiesSkeletonv2[i].skeleton.x;
            pieceOfInfoSkeleton[i].piece.y = enemiesSkeletonv2[i].skeleton.y;
        }
        
        
        
        
        //MONSTERS HP
                
         //afisare hp ghosts
        for(i=0;i<nrenemies;i++){
            if(enemiesGhost_Life[i]==0){
                ghostHP[i].alpha = 0;
            }
            else{
               ghostHP[i].setText("[HP] " + enemiesGhost_Life[i]);
               ghostHP[i].x = enemysv2[i].ghost.x-25;
               ghostHP[i].y = enemysv2[i].ghost.y-40;
            }
        }
        
        //AFISARE HP MUMMY
        for(i=0;i<nrenemiesMummyv2;i++){
            if(enemiesMummyv2_Life[i]==0){
                mummyHP[i].alpha = 0;
            }
            else{
                mummyHP[i].setText("[HP] " + enemiesMummyv2_Life[i]);
                mummyHP[i].x = enemiesMummyv2[i].mummy.x-20;
                mummyHP[i].y = enemiesMummyv2[i].mummy.y-40;
            }
        }
        
        //Afisare hp skeletons
        for(i=0;i<5;i++){
            if(enemiesSkeletonv2_Life[i]==0){
                skeletonHP[i].alpha = 0;
            }
            else{
                skeletonHP[i].setText("[HP] " + enemiesSkeletonv2_Life[i]);
                skeletonHP[i].x = enemiesSkeletonv2[i].skeleton.x+25;
                skeletonHP[i].y = enemiesSkeletonv2[i].skeleton.y;
            }
        }
        
        //Afisare hp chests
        for(i=0;i<5;i++){
            if(chest_life[i]==0){
                chestHP[i].alpha = 0;
            }
            else{
                chestHP[i].setText("[HP] " + chest_life[i]);
                chestHP[i].x = chest[i].x-25;
                chestHP[i].y = chest[i].y+25;
            }
        }
        
        //afisare my hp
        myHP.setText("[HP]" + element.value);
        myHP.x = player.x - 25;
        myHP.y = player.y - 40;
        
        
        this.physics.arcade.collide(player,this.groundLayer);      
        this.physics.arcade.collide(player,this.wallLayer);  
        this.physics.arcade.collide(player,npc1_v2); 
        this.physics.arcade.collide(player,npc2_v2); 
        this.physics.arcade.collide(portal_top1,player,this.goToLevel1,null,this);
        
        
        //COPY
        this.physics.arcade.collide(playerCopy,this.groundLayer);      
        this.physics.arcade.collide(playerCopy,this.wallLayer);  
        this.physics.arcade.collide(playerCopy,npc1_v2); 
        this.physics.arcade.collide(playerCopy,npc2_v2); 

     
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        //COPY
        playerCopy.body.velocity.x = 0;
        playerCopy.body.velocity.y = 0;
        
        
        if(element.value==0 && deadAnimPlayed==false){
                player.animations.play('dead');
                playerCopy.animations.play('dead');
                deadAnimPlayed = true;
            }
        
        if(element.value<102 && checkDeadPlayer==false)
        element.value+=0.5;
        
        if(controls.right.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;
            //copy
            playerCopy.animations.play('walkright');
            //player.scale.setTo(1,1);
            playerCopy.body.velocity.x += playerSpeed;
        }
  
        if(controls.left.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;  
            //COPY
            playerCopy.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            playerCopy.body.velocity.x -= playerSpeed;
        }
         
        
        if(controls.up.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
            //COPY
            playerCopy.body.velocity.y -= playerSpeed;
            
             playerCopy.animations.play('walkup');
         }
        
        if(controls.down.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
            //COPY
            playerCopy.body.velocity.y += playerSpeed;
            
             playerCopy.animations.play('walkdown');
         }
        
        
        //foloseste hacks(spells) la un itnerval de timp
        if(controls.one.isDown){
            if(this.game.time.now >= waitingTime1){
               this.killMonstersFunction();
               waitingTime1 = this.game.time.now + 1000;
            }
        }
        
        if(controls.two.isDown){
            if(this.game.time.now >= waitingTime2){
               this.openChestFunction();
               waitingTime2 = this.game.time.now + 1000;
            }
        }
        
        if(controls.three.isDown){
            if(this.game.time.now >= waitingTime3){
               this.teleportFunction();
               waitingTime3 = this.game.time.now + 1000;
            }
        }
        
        if(controls.four.isDown){
            if(this.game.time.now >= waitingTime4){
               this.addCoinsFunction();
               waitingTime4 = this.game.time.now + 1000;
            }
        }
        
        if(controls.openInventory.isDown && pressed==false){
            this.openWindow();
            pressed=true;
            
        }
        if(controls.openInventory.isDown && pressed==true){
            this.closeWindow();
            pressed=false;
        }
       
        
        
        
        if (this.game.input.activePointer.isDown && deadAnimPlayed==false)
        {
            if(this.game.input.activePointer.positionDown.x + this.game.camera.x > player.x+100 ){
                 player.animations.play('attackRight');
                 //playerCopy.animations.play('attackRight');
             }
            else if(this.game.input.activePointer.positionDown.x + this.game.camera.x < player.x-100){
                 player.animations.play('attackLeft');
                 //playerCopy.animations.play('attackLeft');
             }
            else if(this.game.input.activePointer.positionDown.y + this.game.camera.y > player.y){
                 player.animations.play('attackDown');
                 //playerCopy.animations.play('attackDown');
             }
            else {
                 player.animations.play('attackUp');
                 //playerCopy.animations.play('attackUp');
             }
           
        }
        
        else if(player.body.velocity.x ==0 && player.body.velocity.y ==0  && deadAnimPlayed==false){
            if(checkDeadPlayer==false){
               player.animations.play('idle');
               //COPY
               playerCopy.animations.play('idle');
            }
            
        }
        
        
        //intalnirea cu npc ul
         if(checkOverlap(player,npc1_v2)){
            //checkVisibleMark = false;
            if(aliasDialog!=Npc1Dialog_v2)
                Npc1Dialog_v2.pos=0;
            
            
             aliasDialog=Npc1Dialog_v2;
             if(aliasDialog.dialog[2].length > aliasDialog.pos)
             {
             document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
             if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
             document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
             if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
            document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
             if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
            document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
             if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
            document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
            }
             else
             {
                document.getElementById("ChatContent").innerHTML="";
                document.getElementById("ChatAnswer1").innerHTML="";
                document.getElementById("ChatAnswer2").innerHTML="";
                document.getElementById("ChatAnswer3").innerHTML="";
                document.getElementById("ChatAnswer4").innerHTML="";
            }
        }
        else Npc1Dialog_v2.pos=0;
        
        
        
        //verificarea coliziunii gloantelor de player     
        for(i=0;i<nrenemiesSkeletonv2;i++){
           if(checkOverlap(player,bullets[i])){
               element.value-=10;
                   if(element.value<=0)
                    {
                        if(checkDeadPlayer == false){
                           waitingTime = this.game.time.now + 1400;
                           checkDeadPlayer = true;
                        }
                    }
                   if (this.game.time.now > waitingTime && checkDeadPlayer == true){
                        this.resetPlayer();
                        element.value=100;
                        player.reset(300,6400);
                        playerCopy.reset(1000,6400);
                        checkDeadPlayer = false;
                        deadAnimPlayed = false;
                      
                    }
            bullet[i].kill();   
           }
        }
        
        
        
        var x = 0;
        
        
        
        for(i=0;i<nrenemies;i++){
            if(Phaser.Math.distance(player.x, player.y, enemysv2[i+nrenemies].ghost.x, enemysv2[i+nrenemies].ghost.y) <= 500 
                         && Phaser.Math.distance(enemysv2[i+nrenemies].originx, enemysv2[i+nrenemies].originy, player.x, player.y) < 500){  
                           this.game.physics.arcade.moveToXY(enemysv2[i+nrenemies].ghost, playerCopy.x,playerCopy.y, 250);
                        }
                       else 
                           { 
                            if(enemysv2[i+nrenemies].ghost.x != enemysv2[i+nrenemies].originx || enemysv2[i+nrenemies].ghost.y!= enemysv2[i+nrenemies].originy){  
                                this.game.physics.arcade.moveToXY(enemysv2[i+nrenemies].ghost, enemysv2[i+nrenemies].originx,enemysv2[i+nrenemies].originy, 200);
                            }
                            //enemysv2[i].cookie3.body.velocity.x = 0;
                            //enemysv2[i].cookie3.body.velocity.y = 0;
                               
                         }
        }
        
        
        //URMARIREA PLAYERULUI DE CATRE MOBI - ghosts
        for(i=0; i<nrenemies; i++)
            {
                 if(checkOverlap(player,enemysv2[i].ghost) && enemysv2[i].ghost.alive){
                   //enemysv2[i].ghost.animations.play('ghost_anim_atack', 8, true);  
                     if(this.game.input.activePointer.isDown){
                         enemiesGhost_Life[i]--;
                         if(enemiesGhost_Life[i]==0){
                            pieceOfInfoGhost[i].piece.alpha = 1;
                            ghostKilled[i]=true;
                            enemysv2[i].ghost.kill(); 
                            enemysv2[i + nrenemies].ghost.kill();
                         }
                     }
                   element.value-=1;
                   
                   if(element.value<=0)
                    {
                        if(checkDeadPlayer == false){
                           waitingTime = this.game.time.now + 1400;
                           checkDeadPlayer = true;
                        }
                    }
                    if (this.game.time.now > waitingTime && checkDeadPlayer == true){
                        this.resetPlayer();
                        element.value=100;
                        player.reset(300,6400);
                        playerCopy.reset(1000,6400);
                        checkDeadPlayer = false;
                        deadAnimPlayed = false;
                      
                    }
                     
                     
                }
                else{
                    //enemysv2[i].ghost.animations.play('ghost_anim_walk_down', 8, true);
                       if(Phaser.Math.distance(player.x, player.y, enemysv2[i].ghost.x, enemysv2[i].ghost.y) <= 500 
                         && Phaser.Math.distance(enemysv2[i].originx, enemysv2[i].originy, player.x, player.y) < 500){  
                           this.game.physics.arcade.moveToXY(enemysv2[i].ghost, player.x,player.y, 250);
                           this.game.physics.arcade.moveToXY(enemysv2[i+nrenemies].ghost, playerCopy.x,playerCopy.y, 250);
                        }
                       else 
                           { 
                            if(enemysv2[i].ghost.x != enemysv2[i].originx || enemysv2[i].ghost.y!= enemysv2[i].originy){  
                                this.game.physics.arcade.moveToXY(enemysv2[i].ghost, enemysv2[i].originx,enemysv2[i].originy, 200);
                                this.game.physics.arcade.moveToXY(enemysv2[i+nrenemies].ghost, enemysv2[i+nrenemies].originx,enemysv2[i+nrenemies].originy, 200);
                            }
                            //enemysv2[i].cookie3.body.velocity.x = 0;
                            //enemysv2[i].cookie3.body.velocity.y = 0;
                               
                         }
                      
                }
                
            }
        
        
        
        //urmrirea playerului de catre mobi - mummy
        for(i=0; i<nrenemiesMummyv2; i++){
            if(checkOverlap(player,enemiesMummyv2[i].mummy) && enemiesMummyv2[i].mummy.alive){
                   if(this.game.input.activePointer.isDown){
                      enemiesMummyv2_Life[i]--;
                      if(enemiesMummyv2_Life[i]==0){
                          pieceOfInfoMummy[i].piece.x = enemiesMummyv2[i].mummy.x+30;
                          pieceOfInfoMummy[i].piece.y = enemiesMummyv2[i].mummy.y;
                          pieceOfInfoMummy[i].piece.alpha = 1;
                          mummyKilled[i]=true;
                         enemiesMummyv2[i].mummy.kill(); 
                         enemiesMummyv2[i + nrenemiesMummyv2].mummy.kill();
                      }
                   }
                   element.value-=1;
                   if(element.value<=0)
                    {
                        if(checkDeadPlayer == false){
                           waitingTime = this.game.time.now + 1400;
                           checkDeadPlayer = true;
                        }
                    }
                   if (this.game.time.now > waitingTime && checkDeadPlayer == true){
                        this.resetPlayer();
                        element.value=100;
                        player.reset(300,6400);
                        playerCopy.reset(1000,6400);
                        checkDeadPlayer = false;
                        deadAnimPlayed = false;
                      
                    }

                     
                }
                else{
                       if(Phaser.Math.distance(player.x, player.y, enemiesMummyv2[i].mummy.x, enemiesMummyv2[i].mummy.y) <= 500 
                         && Phaser.Math.distance(enemiesMummyv2[i].originx, enemiesMummyv2[i].originy, player.x, player.y) < 500 ){  
                           this.game.physics.arcade.moveToXY(enemiesMummyv2[i].mummy, player.x,player.y, 250);
                           this.game.physics.arcade.moveToXY(enemiesMummyv2[i+nrenemiesMummyv2].mummy, playerCopy.x,playerCopy.y, 250);
                           //movment to player
                           if(player.x > enemiesMummyv2[i].mummy.x){
                               enemiesMummyv2[i].mummy.animations.play('mummy_anim_walk_right', 8, true);
                               enemiesMummyv2[i+nrenemiesMummyv2].mummy.animations.play('mummy_anim_walk_right', 8, true);
                           }
                           else {
                               enemiesMummyv2[i].mummy.animations.play('mummy_anim_walk_left', 8, true);
                               enemiesMummyv2[i + nrenemiesMummyv2].mummy.animations.play('mummy_anim_walk_left', 8, true);
                           }
                        }
                       else 
                           {   
                               if(enemiesMummyv2[i].mummy.x < enemiesMummyv2[i].originx-30 || enemiesMummyv2[i].mummy.x > enemiesMummyv2[i].originx+30 || 
                                  enemiesMummyv2[i].mummy.y < enemiesMummyv2[i].originy-30 || enemiesMummyv2[i].mummy.y > enemiesMummyv2[i].originy+30 ){
                                   this.game.physics.arcade.moveToXY(enemiesMummyv2[i].mummy, enemiesMummyv2[i].originx,enemiesMummyv2[i].originy, 100);
                                   this.game.physics.arcade.moveToXY(enemiesMummyv2[i+nrenemiesMummyv2].mummy,enemiesMummyv2[i+nrenemiesMummyv2].originx,
                                                              enemiesMummyv2[i+nrenemiesMummyv2].originy, 100);
                                   
                                   if(enemiesMummyv2[i].mummy.x < enemiesMummyv2[i].originx){
                                       enemiesMummyv2[i].mummy.animations.play('mummy_anim_walk_right', 8, true);
                                       enemiesMummyv2[i+nrenemiesMummyv2].mummy.animations.play('mummy_anim_walk_right', 8, true);
                                   }
                                   else{
                                       enemiesMummyv2[i].mummy.animations.play('mummy_anim_walk_left', 8, true);
                                       enemiesMummyv2[i+nrenemiesMummyv2].mummy.animations.play('mummy_anim_walk_left', 8, true);
                                   }
                               }
                            //enemysv2[i].cookie3.body.velocity.x = 0;
                            //enemysv2[i].cookie3.body.velocity.y = 0;
                              
                               
                         }
                      
                }
        }
        
        //URMARIREA PLAYERULUI DE MONSTRI - SKELETONI
        
        
        
        for(i=0; i<nrenemiesSkeletonv2; i++){
            if(checkOverlap(player,enemiesSkeletonv2[i].skeleton) && enemiesSkeletonv2[i].skeleton.alive){
                   if(this.game.input.activePointer.isDown){
//                         enemiesSkeletonv2[i].skeleton.kill(); 
//                         enemiesSkeletonv2[i + nrenemiesSkeletonv2].skeleton.kill();
                       enemiesSkeletonv2_Life[i]--;
                      if(enemiesSkeletonv2_Life[i]==0){
                         
                         pieceOfInfoSkeleton[i].piece.alpha = 1;
                         skeletonKilled[i]=true;
                        
                         enemiesSkeletonv2[i].skeleton.kill(); 
                         enemiesSkeletonv2[i + nrenemiesSkeletonv2].skeleton.kill();
                      }
                   }
                }
                else{
                       if(Phaser.Math.distance(player.x, player.y, enemiesSkeletonv2[i].skeleton.x, enemiesSkeletonv2[i].skeleton.y) <= 400 && enemiesSkeletonv2[i].skeleton.alive){  
                           //movment to player
                           enemiesSkeletonv2[i].skeleton.animations.play('skeleton_anim_attack_down', 8, true);
                           if (this.game.time.now > nextFire && bullets[i].countDead() > 0){
                              nextFire = this.game.time.now + fireRate;
                              bullet[i] = bullets[i].getFirstDead();
                              bullet[i].reset(enemiesSkeletonv2[i].skeleton.x , enemiesSkeletonv2[i].skeleton.y);
                              bullet[i].rotation = this.game.physics.arcade.angleToPointer(player);
           
                              //this.game.physics.arcade.moveToPointer(bullet, 2500);
                               this.game.physics.arcade.moveToXY(bullet[i], player.x,player.y, 500);
                              bullet[i].lifespan = 900;
                           }
                           
                           enemiesSkeletonv2[i+nrenemiesSkeletonv2].skeleton.animations.play('skeleton_anim_attack_down', 8, true);

                        }
                       else{   
                           enemiesSkeletonv2[i].skeleton.animations.play('skeleton_anim_idle', 8, true); 
                           enemiesSkeletonv2[i + nrenemiesSkeletonv2].skeleton.animations.play('skeleton_anim_idle', 8, true); 
                           
                       }
                      
                }
        }
        
        
        //OMORAREA CHESTURILOR
        for(i=0; i<5; i++){
            if(checkOverlap(player,chest[i]) ){
                if(this.game.input.activePointer.isDown){
                      chest_life[i]--;
                      if(chest_life[i]==0 && chestOpened[i] == false){
                           chest[i].animations.play('chest_open_anim', 7, true);
                           chest[i].animations.play('chest_opened_anim', 7, true);
                           //chestCopy[i].animations.play('chest_copy_open_anim', 7, true);
                           //chestCopy[i].animations.play('chest_copy_opened_anim', 7, true);
                           chestOpened[i] = true;
                           keys[i].key.alpha = 1;
//                           keys[i] = this.add.sprite(chest[i].x,chest[i].y+550,'key'); //check
//                           keys[i].anchor.setTo(0.5,0.5);
//                           this.physics.arcade.enable(keys[i]);
//                           keys[i].animations.add('keys_open_anim',[0]);
//                           keys[i].animations.play('chest_open_anim', 7, true);
//                           keys[i].body.collideWorldBounds = true;
//                           keys[i].body.immovable = true;
//                           keysText.setText("You have 1 key");
                          }
                }
            }
        }
        
        
        //ridicarea key-lor de pe jos
        for(i=0; i<5; i++){
            if(checkOverlap(player,keys[i].key) && chestOpened[i]){
                keys[i].key.kill();
                nrKeysUntilNow++;
                chestOpened[i] = false;
            }
        }
        
        //ridicarea Bucatilor de informatii de pe jos
        //Fantome
        for(i=0;i<nrenemies;i++){
            if(checkOverlap(player,pieceOfInfoGhost[i].piece) && ghostKilled[i]){
                pieceOfInfoGhost[i].piece.kill();
                nrPiecesInfoUntilNow++;
                ghostKilled[i] = false;
            }
        }
        //MUMII
        for(i=0;i<nrenemiesMummyv2;i++){
            if(checkOverlap(player,pieceOfInfoMummy[i].piece) && mummyKilled[i]){
                pieceOfInfoMummy[i].piece.kill();
                nrPiecesInfoUntilNow++;
                mummyKilled[i] = false;
            }
        }
        //SKELETONI
        for(i=0;i<5;i++){
            if(checkOverlap(player,pieceOfInfoSkeleton[i].piece) && skeletonKilled[i]){
                pieceOfInfoSkeleton[i].piece.kill();
                nrPiecesInfoUntilNow++;
                skeletonKilled[i] = false;
            }
        }
        
        for(i=0;i<5;i++){
            if(canTakeInfoSkeleton[i] && checkOverlap(player,pieceOfInfoSkeleton[i].piece)){
                pieceOfInfoSkeleton[i].piece.kill();
                nrPiecesInfoUntilNow++;
                skeletonKilled[i] = false;
                canTakeInfoSkeleton[i] = false;
            }
        }
        
        
      //MERSUL MONSTRILOR - ghosts
        var i;
        for(i=0;i<nrenemies + nrenemiesCopy;i++){
            if(enemysv2[i].ghost.y == enemysv2[i].originy && enemysv2[i].ghost.x == enemysv2[i].originx){
                enemysv2[i].ghost.animations.play('ghost_anim_walk_down', 8, true);
            }
//            else if(enemysv2[i].ghost.y == enemysv2[i].originy + 100){
//                enemysv2[i].ghost.animations.play('ghost_anim_walk_up', 8, true);
//            }
        }
        
        
               
        
        
    },
    
        resetPlayer : function(){
        this.spawn();
    },
    
    //SPAWN FUNCTION
    spawn : function(){    
        element = document.getElementById("HPBar");
        //alert(element.value);
        //respawn.forEach(function(spawnPoint){
        //    player.reset(spawnPoint.x,spawnPoint.y);
        //},this);
        isPlayer=true;
    },
    
    
    //WHEN HIT PORTAL JUMP TO LEVEL1 MAP
    goToLevelSecurityLeftRightMap :function(){
        this.state.start('LevelSecurityMainCamera');
    },
    goToLevel1 : function(){
        this.state.start('Level1');
    },
    
    destroyBullet : function(){
       bullet[i].kill();
    },
    
    openWindow : function() {
       
       if ((tween !== null && tween.isRunning) || popup.scale.x === 1){
        return;
       }
       //  Create a tween that will pop-open the window, but only if it's not already tweening or open
       tween = this.game.add.tween(popup.scale).to( { x: 1, y: 1 }, 1000, Phaser.Easing.Elastic.Out, true);
    },

    closeWindow : function() {
       
       if (tween && tween.isRunning || popup.scale.x === 0.1){
          return;
       }
    //  Create a tween that will close the window, but only if it's not already tweening or closed
    tween = this.game.add.tween(popup.scale).to( { x: 0, y: 0 }, 500, Phaser.Easing.Elastic.In, true);
    },
    
    togglePause : function() {

        this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

     },
    
    teleportFunction : function(){
        if(nrTeleportsLeft>=1){
           nrTeleportsLeft--;
           player.reset(300,500);
           playerCopy.reset(1140,500);
        }
    },
    
    openChestFunction : function(){
        if(nrOpenItemsLeft>=1){
            var i=-1;
            var indexChest=0;
            var minDistance = 99999999;
            for(i=0;i<5;i++){
                if(Phaser.Math.distance(player.x, player.y, chest[i].x, chest[i].y)<minDistance && chestOpened[i] == false){
                    minDistance = Phaser.Math.distance(player.x, player.y, chest[i].x, chest[i].y);
                    indexChest = i;
                }        
            }
            if(indexChest != -1){
                nrOpenItemsLeft--;
            }
            chest[indexChest].animations.play('chest_open_anim', 7, true);
            chest[indexChest].animations.play('chest_opened_anim', 7, true);
            //chestCopy[indexChest].animations.play('chest_copy_open_anim', 7, true);
            //chestCopy[indexChest].animations.play('chest_copy_opened_anim', 7, true);
            chestOpened[indexChest] = true;
            chest_life[indexChest] = 0;
            keys[indexChest].key.alpha = 1;            
        }
    },
    
    killMonstersFunction :function(){
        if(nrKillItemsLeft>=1){
            var i;
            var indexGhost=[];
            var indexMummy=[];
            var indexSkeleton =[];
            
            for(i=1;i<=3;i++){
                indexGhost[i] = -1;
                indexMummy[i] = -1;
                indexSkeleton[i] = -1;
            }
            
            var distMin1=99999999;
            var distMin2=99999999;
            var distMin3=99999999;
            
            
            for(i=0;i<nrenemies;i++){
                if(Phaser.Math.distance(player.x, player.y, enemysv2[i].ghost.x, enemysv2[i].ghost.y)<distMin1 && enemiesGhost_Life[i] >0 && ghostKilled[i] == false && pieceOfInfoGhost[i].piece.alive){
                    console.log("am gasit");
                    distMin3 = distMin2;
                    distMin2 = distMin1;
                    distMin1 = Phaser.Math.distance(player.x, player.y, enemysv2[i].ghost.x, enemysv2[i].ghost.y);
                    indexGhost[3] = indexGhost[2];
                    indexGhost[2] = indexGhost[1];
                    indexGhost[1] = i;
                }
                else if(Phaser.Math.distance(player.x, player.y, enemysv2[i].ghost.x, enemysv2[i].ghost.y)<distMin2 && enemiesGhost_Life[i] >0 && ghostKilled[i] == false && pieceOfInfoGhost[i].piece.alive){
                    distMin3 = distMin2;
                    distMin2 = Phaser.Math.distance(player.x, player.y, enemysv2[i].ghost.x, enemysv2[i].ghost.y);
                    indexGhost[3] = indexGhost[2];
                    indexGhost[2] = i;
                }
                else if(Phaser.Math.distance(player.x, player.y, enemysv2[i].ghost.x, enemysv2[i].ghost.y)<distMin3 && enemiesGhost_Life[i] >0 && ghostKilled[i] == false && pieceOfInfoGhost[i].piece.alive){
                    distMin3 = Phaser.Math.distance(player.x, player.y, enemysv2[i].ghost.x, enemysv2[i].ghost.y);
                    indexGhost[3] = i;
                }
            }
            
            
            
            
            for(i=0; i <nrenemiesMummyv2;i++){
                if(Phaser.Math.distance(player.x, player.y, enemiesMummyv2[i].mummy.x, enemiesMummyv2[i].mummy.y)<distMin1 && enemiesMummyv2_Life[i]>0 && mummyKilled[i] == false && pieceOfInfoMummy[i].piece.alive){
                    distMin3 = distMin2;
                    distMin2 = distMin1;
                    distMin1 = Phaser.Math.distance(player.x, player.y, enemiesMummyv2[i].mummy.x, enemiesMummyv2[i].mummy.y); 
                    indexMummy[3] = indexMummy[2];
                    indexMummy[2] = indexMummy[1];
                    indexMummy[1] = i;
                }
                
                else if(Phaser.Math.distance(player.x, player.y, enemiesMummyv2[i].mummy.x, enemiesMummyv2[i].mummy.y)<distMin2 && enemiesMummyv2_Life[i]>0 && mummyKilled[i] == false && pieceOfInfoMummy[i].piece.alive){
                    distMin3 = distMin2;
                    distMin2 = Phaser.Math.distance(player.x, player.y, enemiesMummyv2[i].mummy.x, enemiesMummyv2[i].mummy.y);
                    indexGhost[3] = indexGhost[2];
                    indexGhost[2] = i;
                    
                }
                
                else if(Phaser.Math.distance(player.x, player.y, enemiesMummyv2[i].mummy.x, enemiesMummyv2[i].mummy.y)<distMin3 && enemiesMummyv2_Life[i]>0 && mummyKilled[i] == false && pieceOfInfoMummy[i].piece.alive){
                    distMin3 = Phaser.Math.distance(player.x, player.y, enemiesMummyv2[i].mummy.x, enemiesMummyv2[i].mummy.y);
                    indexGhost[3] = i;
                }
            }
            
           
            
            for(i=0;i<nrenemiesSkeletonv2;i++){
                if(Phaser.Math.distance(player.x, player.y, enemiesSkeletonv2[i].skeleton.x, enemiesSkeletonv2[i].skeleton.y)<distMin1 && enemiesSkeletonv2_Life[i]>0 && skeletonKilled[i] == false && pieceOfInfoSkeleton[i].piece.alive){
                    
                    distMin3 = distMin2;
                    distMin2 = distMin1;
                    distMin1 = Phaser.Math.distance(player.x, player.y, enemiesSkeletonv2[i].skeleton.x, enemiesSkeletonv2[i].skeleton.y); 
                    indexSkeleton[3] = indexSkeleton[2];
                    indexSkeleton[2] = indexSkeleton[1];
                    indexSkeleton[1] = i;
                }
                
                else if(Phaser.Math.distance(player.x, player.y, enemiesSkeletonv2[i].skeleton.x, enemiesSkeletonv2[i].skeleton.y)<distMin2 && enemiesSkeletonv2_Life[i]>0 && skeletonKilled[i] == false && pieceOfInfoSkeleton[i].piece.alive){
                    distMin3 = distMin2;
                    distMin2 = Phaser.Math.distance(player.x, player.y, enemiesSkeletonv2[i].skeleton.x, enemiesSkeletonv2[i].skeleton.y);
                    indexSkeleton[3] = indexSkeleton[2];
                    indexSkeleton[2] = i;
                    
                }
                
                else if(Phaser.Math.distance(player.x, player.y, enemiesSkeletonv2[i].skeleton.x, enemiesSkeletonv2[i].skeleton.y)<distMin3 && enemiesSkeletonv2_Life[i]>0 && skeletonKilled[i] == false && pieceOfInfoSkeleton[i].piece.alive){
                    distMin3 = Phaser.Math.distance(player.x, player.y, enemiesSkeletonv2[i].skeleton.x, enemiesSkeletonv2[i].skeleton.y);
                    indexGhost[3] = i;
                }
            }
            
            
            
            var j,aux;
//            var top=[][];
//            for(i=0;i<=3;i++){
//                top[1][i*3 + 0] = indexGhost[i];
//                top[2][i*3 + 1] = indexMummy[i];
//                top[3][i*3 + 2] = indexSkeleton[i];    
//            }
//            for(i=0;i<10;i++){
//                for(j=i+1;j<9;j++){
//                    if(top[i/3+ 1]>top[j]){
//                        aux=top[i];
//                        top[i] = top[j];
//                        top[j] = aux;
//                    }
//                }
//            }
            
            var nrmobs = 3;
            if(nrKillItemsLeft>=1){
               for(i = 1; i <=3;i++){ 
                  if(indexGhost[i] >= 0 && nrmobs>=1){
                      pieceOfInfoGhost[indexGhost[i]].piece.alpha = 1;
                    
                      enemysv2[indexGhost[i]].ghost.kill(); 
                      //enemysv2[indexGhost[i] + nrenemies].ghost.kill();
                      ghostKilled[indexGhost[i]]=true;
                      enemiesGhost_Life[indexGhost[i]]=0;
                      console.log("da1");
                    
                      nrmobs--;
                  }
                
                  if(indexMummy[i] >= 0  && nrmobs>=1){
                      pieceOfInfoMummy[indexMummy[i]].piece.alpha = 1;
                      
                      enemiesMummyv2[indexMummy[i]].mummy.kill(); 
                      //enemiesMummyv2[indexMummy[i] + nrenemiesMummyv2].mummy.kill();
                      mummyKilled[indexMummy[i]]=true;
                      enemiesMummyv2_Life[indexMummy[i]]=0;
                      
                      console.log("da2");
                      nrmobs--;
                  }
                
                  if(indexSkeleton[i] >= 0  && nrmobs>=1){
                      pieceOfInfoSkeleton[indexSkeleton[i]].piece.alpha = 1;
                    
                      enemiesSkeletonv2[indexSkeleton[i]].skeleton.kill(); 
                     // enemiesSkeletonv2[indexSkeleton[i] + nrenemiesSkeletonv2].skeleton.kill();
                      mummyKilled[indexSkeleton[i]]=true;
                      enemiesSkeletonv2_Life[indexSkeleton[i]]=0;
                      
                      canTakeInfoSkeleton[indexSkeleton[i]] = true;
                    
                     console.log("da3");
                      nrmobs--;
                  }
                    
                }
                
                nrKillItemsLeft--;
            }
            
            
        }
    },
    
    addCoinsFunction : function(){
        if(nrAddCoinsLeft >= 1 ){
           gold+=500;
           repaintGold();
            nrAddCoinsLeft--;
        }
    
     }
    
};


//function togglePause() {
//
//    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;
//
//}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 99999999; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function checkOverlapCookie(spriteA,spriteB){
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();
    
    return Phaser.Rectangle.intersects(boundsA,boundsB);
}

function resetBullet(bullet) {
	// Destroy the laser
	bullet.kill();
}


function repaintGold()
{
    var canvas = document.getElementById("Gold");
    var ctx = canvas.getContext("2d");
     ctx.font = "20px Arial";
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillText(gold,50,15);
}

//function nextDialog(answernr)
//{
//    console.log(aliasDialog.pos+"  "+aliasDialog.dialog[2].length);
//    var nrAnswers=aliasDialog.dialog[1][aliasDialog.pos].length;
//    //if(aliasDialog.dialog[1][aliasDialog])
//    //console.log(nrAnswers);
//   if(aliasDialog.dialog[2].length > aliasDialog.pos)
//       {
//           var poz=aliasDialog.dialog[2][aliasDialog.pos];
//           while(aliasDialog.dialog[2][aliasDialog.pos]==poz)
//           aliasDialog.pos++;
//           aliasDialog.pos+=answernr;
//       }
//     console.log("After "+aliasDialog.pos);
//    /////CAZURI SPESIAL
//    
//    if(aliasDialog.dialog==npc1_v2.dialog && aliasDialog.pos==1 && gold==0)
//       aliasDialog.pos+=nrAnswers - answernr;
//    console.log("After S "+aliasDialog.pos);
//    
//    
//}



