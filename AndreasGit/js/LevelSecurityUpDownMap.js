
exclamMark = function(index,game,x,y){
    this.mark =  game.add.sprite(x,y,'exclamationMark');
    this.mark.anchor.setTo(0.5,0.5);
    this.mark.name = index.toString();
    game.physics.enable(this.mark,Phaser.Physics.ARCADE);
    this.mark.body.immovable = true;
    this.mark.body.collideWorldBounds = true;
    this.mark.body.allowGravity = false;
    
    this.markTween = game.add.tween(this.mark).to({
        y: this.mark.y + 20
    },2000,'Linear',true,0,100,true);
};

EnemyGhost = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.ghost =  game.add.sprite(x,y,'ghostMonster');
    this.ghost.anchor.setTo(0.5,0.5);
    this.ghost.name = index.toString();
    //game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.cookie3);
        game.physics.enable(this.ghost,Phaser.Physics.ARCADE);
        this.ghost.animations.add('ghost_anim');
        this.ghost.animations.add('ghost_anim_walk_up',[3,4,5]);
        this.ghost.animations.add('ghost_anim_walk_down',[0,1,2]);
        this.ghost.animations.add('ghost_anim_atack',[33,35,35,37,15]);
        //this.ghost.animations.play('ghost_anim', 8, true);
        this.ghost.body.collideWorldBounds = true;
        this.ghost.body.immovable = true;
        this.ghost.body.allowGravity = false;
    
    //this.cookie3.body.immovable = true;
    //this.cookie3.body.collideWorldBounds = true;
    //this.cookie3.body.allowGravity = false;

    this.ghostTween = game.add.tween(this.ghost).to({
        y: this.ghost.y + 70,
    },2000,'Linear',true,0,100,true); 
    
    //game.physics.arcade.moveToXY(this.cookie3, player.x,player.y, 200);
    
    
    //this.cookie3.reset(x,y);
};

EnemyMummy = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.mummy =  game.add.sprite(x,y,'mummyMonster');
    this.mummy.anchor.setTo(0.5,0.5);
    this.mummy.name = index.toString();
    //game.physics.enable(this.mummy,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.cookie3);
        game.physics.enable(this.mummy,Phaser.Physics.ARCADE);
        this.mummy.animations.add('mummy_anim');
        this.mummy.animations.add('mummy_anim_walk_left',[4,3,2,1,0,14,13,12,11,10,24,23,22,21,20,34,33,32]);
        this.mummy.animations.add('mummy_anim_walk_right',[5,6,7,8,9,15,16,17,18,19,25,26,27,28,29,35,36,37]);
        //this.mummy.animations.add('mummy_anim_atack',[33,35,35,37,15]);
        this.mummy.animations.play('mummy_anim_walk_left', 8, true);
        this.mummy.body.collideWorldBounds = true;
        this.mummy.body.immovable = true;
        this.mummy.body.allowGravity = false;
    
    //this.cookie3.body.immovable = true;
    //this.cookie3.body.collideWorldBounds = true;
    //this.cookie3.body.allowGravity = false;

//    this.mummyTween = game.add.tween(this.mummy).to({
//        x: this.mummy.x + 100,
//    },2000,'Linear',true,0,100,true); 
    
    //game.physics.arcade.moveToXY(this.mummy, player.x,player.y, 200);
    
    //this.mummy.reset(x,y);
};

EnemySkeleton = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.skeleton =  game.add.sprite(x,y,'skeletonMonster');
    this.skeleton.anchor.setTo(0.5,0.5);
    this.skeleton.name = index.toString();
    //game.physics.enable(this.skeleton,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.skeleton);
        game.physics.enable(this.skeleton,Phaser.Physics.ARCADE);
        this.skeleton.animations.add('skeleton_anim_idle',[182,183,184,185,186,187,186,185,184,183,182]);
        this.skeleton.animations.add('skeleton_anim_attack_up',[208,209,210,211,212,213,214,215,216,217,218,219,220]);
        this.skeleton.animations.add('skeleton_anim_attack_left',[221,222,223,224,225,226,227,228,229,230,231,232,233]);
        this.skeleton.animations.add('skeleton_anim_attack_down',[234,235,236,237,238,239,240,241,242,243,244,245,246]);
        this.skeleton.animations.add('skeleton_anim_attack_right',[247,248,249,250,251,252,253,254,255,256,257,258,259]);
        //this.mummy.animations.add('skeleton_anim_atack',[33,35,35,37,15]);
        this.skeleton.animations.play('skeleton_anim_idle', 8, true);
        this.skeleton.body.collideWorldBounds = true;
        this.skeleton.body.immovable = true;
        this.skeleton.body.allowGravity = false;

//    this.skeletonTween = game.add.tween(this.skeleton).to({
//        x: this.skeleton.x + 100,
//    },2000,'Linear',true,0,100,true); 
    
    //game.physics.arcade.moveToXY(this.skeleton, player.x,player.y, 200);
    
    //this.skeleton.reset(x,y);
};

newKey = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.key =  game.add.sprite(x,y,'key');
    this.key.anchor.setTo(0.5,0.5);
    this.key.name = index.toString();
        game.physics.enable(this.key,Phaser.Physics.ARCADE);
        //this.key.animations.add('key_anim',[0]);
        //this.key.animations.play('key_anim', 8, true);
        this.key.body.collideWorldBounds = true;
        this.key.body.immovable = true;
        this.key.body.allowGravity = false;
 
    this.keyTween = game.add.tween(this.key).to({
        y: this.key.y + 50,
    },2000,'Linear',true,0,100,true); 
    
};

PieceOfInformationGhost = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.piece =  game.add.sprite(x,y,'information');
    this.piece.anchor.setTo(0.5,0.5);
    this.piece.name = index.toString();
        game.physics.enable(this.piece,Phaser.Physics.ARCADE);
        this.piece.body.collideWorldBounds = true;
        this.piece.body.immovable = true;
        this.piece.body.allowGravity = false;
 
    this.pieceTween = game.add.tween(this.piece).to({
        y: this.piece.y + 20,
    },2000,'Linear',true,0,100,true); 
    
};

PieceOfInformationMummy = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.piece =  game.add.sprite(x,y,'information');
    this.piece.anchor.setTo(0.5,0.5);
    this.piece.name = index.toString();
        game.physics.enable(this.piece,Phaser.Physics.ARCADE);
        this.piece.body.collideWorldBounds = true;
        this.piece.body.immovable = true;
        this.piece.body.allowGravity = false;
 
    this.pieceTween = game.add.tween(this.piece).to({
        y: this.piece.y + 20,
    },2000,'Linear',true,0,100,true); 
    
};

PieceOfInformationSkeleton = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.piece =  game.add.sprite(x,y,'information');
    this.piece.anchor.setTo(0.5,0.5);
    this.piece.name = index.toString();
        game.physics.enable(this.piece,Phaser.Physics.ARCADE);
        this.piece.body.collideWorldBounds = true;
        this.piece.body.immovable = true;
        this.piece.body.allowGravity = false;
 
    this.pieceTween = game.add.tween(this.piece).to({
        y: this.piece.y + 20,
    },2000,'Linear',true,0,100,true); 
    
};



Phaser.Tilemap.prototype.setCollisionBetween = function (start, stop, collides, layer, recalculate) {

	if (collides === undefined) { collides = true; }
	if (layer === undefined) { layer = this.currentLayer; }
	if (recalculate === undefined) { recalculate = true; }

	layer = this.getLayer(layer);

	for (var index = start; index <= stop; index++)
	{
		if (collides)
		{
			this.collideIndexes.push(index);
		}
		else
		{
			var i = this.collideIndexes.indexOf(index);

			if (i > -1)
			{
				this.collideIndexes.splice(i, 1);
			}
		}
	}

	for (var y = 0; y < this.layers[layer].height; y++)
	{
		for (var x = 0; x < this.layers[layer].width; x++)
		{
			var tile = this.layers[layer].data[y][x];

			if (tile && tile.index >= start && tile.index <= stop)
			{
				if (collides)
				{
					tile.setCollision(true, true, true, true);
				}
				else
				{
					tile.resetCollision();
				}

				tile.faceTop = collides;
				tile.faceBottom = collides;
				tile.faceLeft = collides;
				tile.faceRight = collides;
			}
		}
	}

	if (recalculate)
	{
		//  Now re-calculate interesting faces
		this.calculateFaces(layer);
	}

	return layer;

};




Game.LevelSecurityUpDownMap = function(game){
   
};

var map;
var layer;
var music;
var fire;
var respawn;

var player;
var controls = {};
var playerSpeed = 1500;
var jumpTimer = 0;
var pauseKey;

var text;

var npc1;
var npc1_nr;
var npc2;
var npc2_nr = 80;
var npc3;
var npc3_nr = 70;

var Npc2Dialog = {pos :0 , dialog: null};
var aliasDialog ={ pos: 0 , dialog: null};
var Npc3Dialog = {pos :0 , dialog: null};


var enemys=[];//ghosts
var enemiesGhost_Life=[];
var cookies=[];//ghosts
var enemiesMummy=[];//mummy
var enemiesMummy_Life=[];
var nrenemies=15;//ghosts
var nrEnemiesMummy = 10;//mummy
var enemiesRemain=15;//ghosts

var enemysCopy=[];//ghosts
var cookiesCopy=[];//ghosts
var nrenemiesCopy=15;//ghosts
var enemiesCopyRemain=15;//ghosts

var enemysCopyMummy=[];//mummy
var cookiesCopyMummy=[];//mummy
var nrenemiesCopyMummy=10;//mummy
var enemiesCopyRemainMummy=10;//mummy

//Skeleton
var enemiesSkeleton=[];
var enemiesSkeletonCopy=[];
var nrEnemiesSkeleton = 5;
var nrEnemiesSkeletonCopy = 5;
var enemiesSkeleton_Life=[];

var exclamationMark1;
var exclamationMark2;
var exclamationMark3;

var playerCopy;

var portal1x;
var portal2x;

var element;
var isPlayer=false;

var chest=[];
var chestCopy=[];
var chest_life=[];

var bullets=[];
var bullet=[];
var fireRate = 1500;
var nextFire = 0;


var openChestTime = 1000;
var openChest = 0;
var chestOpened=[];

var waitingTime=0;
var checkDeadPlayer=false;
var deadAnimPlayed = false;

var keys=[];
var keysCopy=[];
var nrKeys=5;
var nrCopyKeys=5;

var nrKeysUntilNow = 0;
var nrPiecesInfoUntilNow = 0;

var keysText;
var numberOfInfoText;

var ghostHP=[];
var mummyHP=[];
var skeletonHP=[];
var chestHP=[];

var myHP = 0;
var pieceOfInfoMummy=[];
var pieceOfInfoGhost=[];
var pieceOfInfoSkeleton=[];

var ghostKilled=[];
var mummyKilled=[];
var skeletonKilled=[];

var bigwall;

var mainPieceOfInfo;
var pickedUpMainInfo = false;

var atentionText=[];
var atentionTime=0;
var appearedErrors = false;
var nrAtentionsLeft = 20;
var checkedAtention = false;


//
var currentGame;
var checkVisibleMark = true;
var canContinue = false; 
var canEnd = false;

var npc3End = false;

var checkedSpeech2 = false;


Game.LevelSecurityUpDownMap.prototype = {
    create: function(){
        currentGame = this;
        var i;
        for(i=0;i<nrenemies + nrenemiesCopy;i++){
            enemiesGhost_Life[i]=50;
            ghostKilled[i] = false;
        }
        for(i=0;i<nrEnemiesMummy + nrenemiesCopyMummy;i++){
            enemiesMummy_Life[i]=50;
            mummyKilled[i] = false;
        }
        for(i=0;i<5;i++){
            chest_life[i] = 200;
        }
        for(i=0;i<5;i++){
            enemiesSkeleton_Life[i]=25;
            skeletonKilled[i] = false;
        }
        
        //chests initial closed
        for(i=0;i<5;i++){
            chestOpened[i]=false;
        }
        
        
        this.stage.backgroundColor = '#3A5963';
        pausedgame = false;
        
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        //respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('upDownMap');             
       
        this.map.addTilesetImage('terrain');
        this.map.addTilesetImage('ground9');
        this.map.addTilesetImage('wallSprite');


                            
        this.groundLayer = this.map.createLayer('groundLayer');  
        this.wallLayer = this.map.createLayer('wallLayer'); 

        this.groundLayer.resizeWorld();
        
        
        // OBJECT LAYER
        
        
        //Afisare numar key 
        keysText = this.game.add.text(50,50,"You have 0 keys",{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
        //afisare numar de bucati de informatii
        numberOfInfoText = this.game.add.text(50,50,"You have 0 pieces of informations",{
            font: "24px Arial",
            fill: "#ff0044",
            align: "left"
        });
        
        //afisare HP GHOSTS
//        for(i=0;i<nrenemies;i++){
//            ghostHP[i] = this.game.add.text(50,50,enemiesGhost_Life[i],{
//            font: "12px Arial",
//            fill: "#ff0044",
//            align: "left"
//            });
//        }
        
        //AFISARE HP MUMMY
        for(i=0;i<nrEnemiesMummy;i++){
            mummyHP[i] = this.game.add.text(50,50,enemiesMummy_Life[i],{
            font: "12px Arial",
            fill: "#ff0044",
            align: "left"
            });
        }
        
        //Afisare hp skeleton
        for(i=0;i<5;i++){
            skeletonHP[i] = this.game.add.text(50,50,enemiesSkeleton_Life[i],{
            font: "12px Arial",
            fill: "#ff0044",
            align: "left"
            });
        }
        
        //afisare hp chests
        for(i=0;i<5;i++){
            chestHP[i] = this.game.add.text(50,50,chest_life[i],{
            font: "12px Arial",
            fill: "#ff0044",
            align: "left"
            });
        }
        
        //afisare my hp
            myHP= this.game.add.text(50,50,"100",{
            font: "12px Arial",
            fill: "#00ff44",
            align: "left"
            });
        
        for(i=0;i<25;i++){
            randomx=Math.floor(Math.random() * 1100) + 10;
            randomy=Math.floor(Math.random() * 700) + 10;
            atentionText[i] = this.game.add.text(randomx,randomy,"WARNING!!! XSS Attack! You have been infected!",{
            font: "32px Arial",
            fill: "#ff0044",
            align: "left"
            });
            atentionText[i].alpha = 0;
        }
        
        
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        //NPCs
        
        //NPC 1
        npc1 = this.add.sprite(150,195,'npc1');
        npc1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(npc1);
        //fireAnim1.animations.add('fireAnim1_anim');
        //fireAnim1.animations.play('fireAnim1_anim', 12, true);
        npc1.body.collideWorldBounds = true;
        npc1.body.immovable = true;
        
        //NPC 2
        npc2 = this.add.sprite(150,600,'npc1');
        npc2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(npc2);
        //fireAnim1.animations.add('fireAnim1_anim');
        //fireAnim1.animations.play('fireAnim1_anim', 12, true);
        npc2.body.collideWorldBounds = true;
        npc2.body.immovable = true;
        getDialog(npc2_nr,Npc2Dialog);
        
        //NPC 3
        npc3 = this.add.sprite(6450,195,'npc1');
        npc3.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(npc3);
        //fireAnim1.animations.add('fireAnim1_anim');
        //fireAnim1.animations.play('fireAnim1_anim', 12, true);
        npc3.body.collideWorldBounds = true;
        npc3.body.immovable = true;
        getDialog(npc3_nr,Npc3Dialog);
        
        //PORTAL TO NEXT CAMERA 2
        
        portal1x = this.add.sprite(6650,195,'portal1x');
        portal1x.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal1x);
        portal1x.animations.add('portal1x_anim',[0,1,2,1,0]);
        portal1x.animations.play('portal1x_anim', 12, true);
        portal1x.body.collideWorldBounds = true;
        portal1x.body.immovable = true;
        
        portal2x = this.add.sprite(6650,600,'portal1x');
        portal2x.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal2x);
        portal2x.animations.add('portal2x_anim',[0,1,2,1,0]);
        portal2x.animations.play('portal2x_anim', 12, true);
        portal2x.body.collideWorldBounds = true;
        portal2x.body.immovable = true;
        
//        bigWall1 = this.add.sprite(900,370,'bigwall');
//        bigWall1.anchor.setTo(0.5,0.5);
//        this.physics.arcade.enable(bigWall1);
//        bigWall1.body.collideWorldBounds = true;
//        bigWall1.body.immovable = true;
//        
//        bigWall2 = this.add.sprite(5900,370,'bigwall');
//        bigWall2.anchor.setTo(0.5,0.5);
//        this.physics.arcade.enable(bigWall2);
//        bigWall2.body.collideWorldBounds = true;
//        bigWall2.body.immovable = true;
        
        //CHESTS
        
        var incr1 = 1630 ;
        var skeletons_until_now=0;
        var skeletons_copy_until_now=0;
        var k;
        for(i=0;i<5;i++){
            //CHESTS
            chest[i] = this.add.sprite(incr1,120,'chest1');
            chest[i].anchor.setTo(0.5,0.5);
            this.physics.arcade.enable(chest[i]);
            chest[i].animations.add('chest_open_anim',[0,1,2,3,2,1]);
            chest[i].animations.add('chest_opened_anim',[3]);
            //chest[i].animations.play('chest_open_anim', 7, true);
            chest[i].body.collideWorldBounds = true;
            chest[i].body.immovable = true;
            //COPY CHESTS
            chestCopy[i] = this.add.sprite(incr1,480,'chest1');
            chestCopy[i].anchor.setTo(0.5,0.5);
            this.physics.arcade.enable(chestCopy[i]);
            chestCopy[i].animations.add('chest_copy_open_anim',[0,1,2,3,2,1]);
            chestCopy[i].animations.add('chest_copy_opened_anim',[3]);
            //chestCopy[i].animations.play('chest_copy_open_anim', 7, true);
            chestCopy[i].body.collideWorldBounds = true;
            chestCopy[i].body.immovable = true;
            chestCopy[i].alpha = 0;
            
            incr1 = incr1 + 1000;
        }
        
        //SKELETONS
        for(i=0;i<5;i++){
//            enemy=new EnemySkeleton(i*3+0,this.game,chest[i].x-80,chest[i].y);
//            enemiesSkeleton.push(enemy);
//            enemy=new EnemySkeleton(i*3+1,this.game,chest[i].x+80,chest[i].y);
//            enemiesSkeleton.push(enemy);
            enemy=new EnemySkeleton(i,this.game,chest[i].x,chest[i].y-60);
            enemiesSkeleton.push(enemy);
        }
        //SKELETONS COPY
        for(i=0;i<5;i++){
//            enemy=new EnemySkeleton(nrEnemiesSkeleton + i*3+0,this.game,chest[i].x-80,chest[i].y+400);
//            enemiesSkeleton.push(enemy);
//            enemy=new EnemySkeleton(nrEnemiesSkeleton + i*3+1,this.game,chest[i].x+80,chest[i].y+400);
//            enemiesSkeleton.push(enemy);
            enemy=new EnemySkeleton(nrEnemiesSkeleton + i,this.game,chest[i].x,chest[i].y+300);
            enemy.skeleton.alpha = 0;
            enemiesSkeleton.push(enemy);
        }
        
        
  
        
        
        //PLAYER
    
        
        this.map.setCollisionBetween(1, 100000, true, 'wallLayer'); 

        
        player = this.add.sprite(450,250,'soldatPlayer');
        player.anchor.setTo(0.5,0.5);
        
        
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        
        player.animations.add('walkdown',[240,241,242,243,244,245,246,247,248],13,true);
        player.animations.add('walkup',[192,193,194,195,196,197,198,199,200],13,true);
        player.animations.add('walkleft',[216,217,218,219,220,221,222,223,224],13,true);
        player.animations.add('walkright',[264,266,267,268,269,270,271,272],13,true);
        player.animations.add('attackUp',[529,532,535,538,541,544],20,true);
        player.animations.add('attackLeft',[601,604,607,610,613,616],20,true);
        player.animations.add('attackDown',[673,676,679,682,685],20,true);
        player.animations.add('attackRight',[745,748,751,754,757],20,true);
        
        player.animations.add('dead',[480,481,482,483,484,485],10,false,true);
        player.animations.add('idle',[673,676],4,true);
        
//        deadAnimation.killOnComplete = true;
//        deadAnimation.onComplete.add(function() {
//        console.log("dadada");
//        }, player);
       
       
        //player.events.onAnimationComplete.add(this.animCompleteCallback, this);
        
        
        
        
        //this.physics.arcade.enable(player);
        this.game.physics.enable(player, Phaser.Physics.ARCADE);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        //PlayerCopy
        
        playerCopy = this.add.sprite(450,650,'soldatPlayer');
        playerCopy.anchor.setTo(0.5,0.5);
        
        
        //PLAYER SPAWNS AT THE SET POSITION
        //this.spawn();
        
        
        playerCopy.animations.add('walkdown',[240,241,242,243,244,245,246,247,248],13,true);
        playerCopy.animations.add('walkup',[192,193,194,195,196,197,198,199,200],13,true);
        playerCopy.animations.add('walkleft',[216,217,218,219,220,221,222,223,224],13,true);
        playerCopy.animations.add('walkright',[264,266,267,268,269,270,271,272],13,true);
        playerCopy.animations.add('attackUp',[529,532,535,538,541,544],20,true);
        playerCopy.animations.add('attackLeft',[601,604,607,610,613,616],20,true);
        playerCopy.animations.add('attackDown',[673,676,679,682,685],20,true);
        playerCopy.animations.add('attackRight',[745,748,751,754,757],20,true);
        playerCopy.animations.add('dead',[480,481,482,483,484,485],13,false,true);
        playerCopy.animations.add('idle',[336,337,338,339,340,341,340,339,338,337],4,true);
        
//        deadAnimationCopy.killOnComplete = true;
//        deadAnimationCopy.onComplete.add(function() {
//        console.log("dadada");
//        }, playerCopy);
        
        
        //this.physics.arcade.enable(player);
        this.game.physics.enable(playerCopy, Phaser.Physics.ARCADE);
        this.camera.follow(playerCopy);
        playerCopy.body.collideWorldBounds = true;
        
        
        //CONTROLS
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
            shoot : this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR),
            
            
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(togglePause, this);
        
        
        //Monsters
        
        for(i=0; i<nrenemies; i++)
            {
                randomx=Math.floor(Math.random() * 4800) + 1500;
                randomy=Math.floor(Math.random() * 100) + 100;
                enemy=new EnemyGhost(i,this.game,randomx,randomy);
                enemy.ghost.alpha = 0;
                console.log(randomx+"  "+randomy);
                enemys.push(enemy);
                
            }
        var j = 0;    
            for(i=nrenemies; i<nrenemies + nrenemiesCopy; i++)
            {
                enemy=new EnemyGhost(i,this.game,enemys[j].originx,enemys[j].originy + 400);
                enemys.push(enemy);
                j++;
                
            }
        
        //making mummies
        for(j=0;j<nrEnemiesMummy;j++){
            randomx=Math.floor(Math.random() * 4800) + 1500;
                randomy=Math.floor(Math.random() * 100) + 100;
                enemy=new EnemyMummy(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                enemiesMummy.push(enemy);
        }
        
        j=0;
        for(i=nrEnemiesMummy; i<nrEnemiesMummy + nrenemiesCopyMummy; i++)
            {
                enemy=new EnemyMummy(i,this.game,enemiesMummy[j].originx,enemiesMummy[j].originy + 400);
                enemiesMummy.push(enemy);
                enemy.mummy.alpha = 0;
                j++;
                
            }
        
       
        
        exclamationMark1 = new exclamMark(1,this.game,135,50);
        exclamationMark2 = new exclamMark(2,this.game,135,460);
        exclamationMark3 = new exclamMark(3,this.game,135,50);
        
        for(i=0;i<nrEnemiesSkeleton;i++){
           bullets[i] = this.game.add.group();
           bullets[i].enableBody = true;
           bullets[i].physicsBodyType = Phaser.Physics.ARCADE;
           bullets[i].createMultiple(30,'arrow',0,false);                //check nr of bullets
           //bullets[i].setAll('anchor.x',0.5);
           //bullets[i].setAll('anchor.y',0.5);
        
           bullets[i].callAll('anchor.setTo', 'anchor', 0.5, 1.0);
           bullets[i].callAll('events.onOutOfBounds.add','events.onOutOfBounds', resetBullet);
           bullets[i].setAll('checkWorldBounds', true);
        }
        
        //KEY CREATION INVIZ
        for(i=0;i<5;i++){
            keys[i] = new newKey(i,this.game,chest[i].x,chest[i].y+450);
            keys[i].key.alpha = 0;
            
        }
        
        //Piece of INFORMATIONS CREATION INVIZ
        for(i=0;i<nrEnemiesMummy;i++){
            pieceOfInfoMummy[i] = new PieceOfInformationMummy(i,this.game,enemiesMummy[i].originx,enemiesMummy[i].originy);
            pieceOfInfoMummy[i].piece.alpha = 0;
        }
        for(i=0;i<nrenemies;i++){
            pieceOfInfoGhost[i] = new PieceOfInformationGhost(i,this.game,enemys[i].originx,enemys[i + nrenemies].originy);
            pieceOfInfoGhost[i].piece.alpha = 0;
        }
        for(i=0;i<5;i++){
            pieceOfInfoSkeleton[i] = new PieceOfInformationSkeleton(i,this.game,enemiesSkeleton[i].originx,enemiesSkeleton[i].originy);
            pieceOfInfoSkeleton[i].piece.alpha = 0;
        }
        
        mainPieceOfInfo = new PieceOfInformationGhost(200,this.game,650,250);
        
    },
    
    
    update:function(){
        
        
        //AFisarea erorr error error
        var t;
        if(pickedUpMainInfo){
                if(this.game.time.now >= atentionTime && nrAtentionsLeft>=1){
                    if(nrAtentionsLeft==1)checkVisibleMark = true;
                   atentionText[nrAtentionsLeft-1].alpha = 1;   
                    atentionTime = this.game.time.now + 150;
                    nrAtentionsLeft--;
                    
                }
        }
        
        if(element.value==0){
            element.value=100;
                        player.reset(250,250);
                        playerCopy.reset(250,650);
                        checkDeadPlayer = false;
                        deadAnimPlayed = false;
        }
        
      
        if(nrAtentionsLeft==0 && checkOverlap(player,npc1  ) &&  !canContinue){
            for(t=0;t<20;t++){
                atentionText[t].alpha = 0;
                
            }
            checkedAtention = true;
            canContinue = true;
            
        }
        
        if(nrKeysUntilNow == 5){
            checkVisibleMark=true;
        }
        
        
        //Updatare daca este quest la npc-uri
        exclamationMark1.mark.alpha= checkVisibleMark;
        exclamationMark2.mark.alpha= checkVisibleMark;
        exclamationMark3.mark.alpha= checkVisibleMark;
        
        
        //afisarea numarului de key
        keysText.x = this.game.camera.x;
        keysText.y = this.game.camera.y;
        keysText.setText("[NUMBER OF KEYS] " + nrKeysUntilNow);
        
        //afisarea numarului de bucati de informatii
        numberOfInfoText.x = this.game.camera.x;
        numberOfInfoText.y = this.game.camera.y + 50;
        numberOfInfoText.setText("[NUMBER OF DATA PIECES] " + nrPiecesInfoUntilNow);
        
        
        
        //MONSTERS HP
                
        // afisare hp ghosts
//        for(i=0;i<nrenemies;i++){
//            if(enemiesGhost_Life[i]==0){
//                ghostHP[i].alpha = 0;
//            }
//            else{
//               ghostHP[i].setText("[HP] " + enemiesGhost_Life[i]);
//               ghostHP[i].x = enemys[i].ghost.x-25;
//               ghostHP[i].y = enemys[i].ghost.y+360;
//            }
//        }
        
        //AFISARE HP MUMMY
        for(i=0;i<nrEnemiesMummy;i++){
            if(enemiesMummy_Life[i]==0){
                mummyHP[i].alpha = 0;
            }
            else{
                mummyHP[i].setText("[HP] " + enemiesMummy_Life[i]);
                mummyHP[i].x = enemiesMummy[i].mummy.x-25;
                mummyHP[i].y = enemiesMummy[i].mummy.y-40;
            }
        }
        
        //Afisare hp skeletons
        for(i=0;i<5;i++){
            if(enemiesSkeleton_Life[i]==0){
                skeletonHP[i].alpha = 0;
            }
            else{
                skeletonHP[i].setText("[HP] " + enemiesSkeleton_Life[i]);
                skeletonHP[i].x = enemiesSkeleton[i].skeleton.x+25;
                skeletonHP[i].y = enemiesSkeleton[i].skeleton.y;
            }
        }
        
        //Afisare hp chests
        for(i=0;i<5;i++){
            if(chest_life[i]==0){
                chestHP[i].alpha = 0;
            }
            else{
                chestHP[i].setText("[HP] " + chest_life[i]);
                chestHP[i].x = chest[i].x-25;
                chestHP[i].y = chest[i].y+25;
            }
        }
        
        //afisare my hp
        myHP.setText("[HP]" + element.value);
        myHP.x = player.x - 25;
        myHP.y = player.y - 40;
        
        
        //urmarire bucati de documente dupa monstri
        //urmarirea documentelor
        for(i=0;i<nrenemies;i++){
            pieceOfInfoGhost[i].piece.x = enemys[i].ghost.x;
            pieceOfInfoGhost[i].piece.y = enemys[i].ghost.y;
        }
        for(i=0;i<nrEnemiesMummy;i++){
            pieceOfInfoMummy[i].piece.x = enemiesMummy[i].mummy.x;
            pieceOfInfoMummy[i].piece.y = enemiesMummy[i].mummy.y;
        }
        for(i=0;i<5;i++){
            pieceOfInfoSkeleton[i].piece.x = enemiesSkeleton[i].skeleton.x;
            pieceOfInfoSkeleton[i].piece.y = enemiesSkeleton[i].skeleton.y;
        }
        
        this.physics.arcade.collide(player,this.groundLayer);      
        this.physics.arcade.collide(player,this.wallLayer);  
        this.physics.arcade.collide(player,npc1); 
        this.physics.arcade.collide(player,npc2); 
        this.physics.arcade.collide(player,npc3);
//        this.physics.arcade.collide(player, bigWall1);
//        this.physics.arcade.collide(playerCopy, bigWall1);
        this.physics.arcade.collide(portal1x,player,this.goToLevelSecurityLeftRightMap,null,this);
        
        
        //COPY
        this.physics.arcade.collide(playerCopy,this.groundLayer);      
        this.physics.arcade.collide(playerCopy,this.wallLayer);  
        this.physics.arcade.collide(playerCopy,npc1); 
        this.physics.arcade.collide(playerCopy,npc2); 

     
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        //COPY
        playerCopy.body.velocity.x = 0;
        playerCopy.body.velocity.y = 0;
        
        
        if(element.value==0 && deadAnimPlayed==false){
                player.animations.play('dead');
                playerCopy.animations.play('dead');
                deadAnimPlayed = true;
            }
        
        if(element.value<102 && checkDeadPlayer==false)
        element.value+=0.5;
        
        if(controls.right.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
            if(player.x>6350 || playerCopy.x > 6350){
                if(canEnd){
                    player.animations.play('walkright');
                    //player.scale.setTo(1,1);
                    player.body.velocity.x += playerSpeed;
                    //copy
                    playerCopy.animations.play('walkright');
                    //player.scale.setTo(1,1);
                    playerCopy.body.velocity.x += playerSpeed;
                }
            }
            else if(player.x>=800){
                if(pickedUpMainInfo && canContinue){
                    player.animations.play('walkright');
                    //player.scale.setTo(1,1);
                    player.body.velocity.x += playerSpeed;
                    //copy
                    playerCopy.animations.play('walkright');
                    //player.scale.setTo(1,1);
                    playerCopy.body.velocity.x += playerSpeed;
                }
            } 
            else{
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;
            //copy
            playerCopy.animations.play('walkright');
            //player.scale.setTo(1,1);
            playerCopy.body.velocity.x += playerSpeed;
            }
        }
  
        if(controls.left.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;  
            //COPY
            playerCopy.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            playerCopy.body.velocity.x -= playerSpeed;
        }
         
        
        if(controls.up.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
            //COPY
            playerCopy.body.velocity.y -= playerSpeed;
            
             playerCopy.animations.play('walkup');
         }
        
        if(controls.down.isDown && deadAnimPlayed==false && !this.game.input.activePointer.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
            //COPY
            playerCopy.body.velocity.y += playerSpeed;
            
             playerCopy.animations.play('walkdown');
         }
        
        
        
        
        
        if (this.game.input.activePointer.isDown && deadAnimPlayed==false)
        {
            if(this.game.input.activePointer.positionDown.x + this.game.camera.x > player.x+100 ){
                 player.animations.play('attackRight');
                 //playerCopy.animations.play('attackRight');
             }
            else if(this.game.input.activePointer.positionDown.x + this.game.camera.x < player.x-100){
                 player.animations.play('attackLeft');
                 //playerCopy.animations.play('attackLeft');
             }
            else if(this.game.input.activePointer.positionDown.y + this.game.camera.y > player.y){
                 player.animations.play('attackDown');
                 //playerCopy.animations.play('attackDown');
             }
            else {
                 player.animations.play('attackUp');
                 //playerCopy.animations.play('attackUp');
             }
           
        }
        
        else if(player.body.velocity.x ==0 && player.body.velocity.y ==0  && deadAnimPlayed==false){
            if(checkDeadPlayer==false){
               player.animations.play('idle');
               //COPY
               playerCopy.animations.play('idle');
            }
            
        }
        
        
        //check if main info piece is picked up
        if(checkOverlap(player,mainPieceOfInfo.piece)){
            pickedUpMainInfo = true;
            mainPieceOfInfo.piece.kill();
            
        }
        
        
        
        //NPC2 MESSAGES
        if(checkOverlap(player,npc1)){
            checkVisibleMark = false;
            if(aliasDialog!=Npc2Dialog)
                Npc2Dialog.pos=0;
            
            
             aliasDialog=Npc2Dialog;
             if(aliasDialog.dialog[2].length > aliasDialog.pos)
             {
             document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
             if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
             document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
             if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
            document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
             if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
            document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
             if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
            document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
            }
             else
             {
                document.getElementById("ChatContent").innerHTML="";
                document.getElementById("ChatAnswer1").innerHTML="";
                document.getElementById("ChatAnswer2").innerHTML="";
                document.getElementById("ChatAnswer3").innerHTML="";
                document.getElementById("ChatAnswer4").innerHTML="";
            }
        }
        else Npc2Dialog.pos=0;
        
        var checkEnd = true;
        for(i=0;i<nrenemies;i++){
            if(enemiesGhost_Life[i]>0)
                checkEnd = false;
        }
        for(i=0;i<nrEnemiesMummy;i++){
            if(enemiesMummy_Life[i]>0)
                checkEnd = false;
        }
        for(i=0;i<5;i++){
            if(enemiesSkeleton_Life[i]>0)
                checkEnd = false;
        }
        
        //NPC3 MESSAGES
        if(checkOverlap(player,npc3) && checkEnd){
            nrKeysUntilNow = 3;
            nrPiecesInfoUntilNow = 10;
            checkVisibleMark = false;
            if(aliasDialog!=Npc3Dialog)
                Npc3Dialog.pos=0;     
             aliasDialog=Npc3Dialog;


            if(Npc3Dialog.pos == 2){
                checkedSpeech2=true;    
            }
            
            if(Npc3Dialog.pos==4 && checkedSpeech2){
                 npc3.kill();
                npc3.x = portal1x.x + 200;
                 //this.game.physics.arcade.moveToXY(npc3, npc3.x+400,npc3.y, 9999);
                canEnd = true;
                
             }
             
             if(aliasDialog.dialog[2].length > aliasDialog.pos)
             {
             document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
             if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
             document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
             if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
            document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
             if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
            document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
             if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
            document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
            }
             else
             {
                document.getElementById("ChatContent").innerHTML="";
                document.getElementById("ChatAnswer1").innerHTML="";
                document.getElementById("ChatAnswer2").innerHTML="";
                document.getElementById("ChatAnswer3").innerHTML="";
                document.getElementById("ChatAnswer4").innerHTML="";
            }
        }
        else Npc3Dialog.pos=0;
        
        
        
        
        //verificarea coliziunii gloantelor de player     
        for(i=0;i<nrEnemiesSkeleton;i++){
           if(checkOverlap(player,bullets[i])){
               element.value-=10;
                   if(element.value<=0)
                    {
                        if(checkDeadPlayer == false){
                           waitingTime = this.game.time.now + 1400;
                           checkDeadPlayer = true;
                        }
                    }
                   if (this.game.time.now > waitingTime && checkDeadPlayer == true){
                        this.resetPlayer();
                        element.value=100;
                        player.reset(250,250);
                        playerCopy.reset(250,650);
                        checkDeadPlayer = false;
                        deadAnimPlayed = false;
                      
                    }
            bullet[i].kill();   
           }
        }
        
        
        
        var x = 0;
        
        
        //URMARIREA PLAYERULUI DE CATRE MOBI - ghosts
        for(i=0; i<nrenemies; i++)
            {
                 if(checkOverlap(player,enemys[i].ghost) && enemys[i].ghost.alive){
                   //enemys[i].ghost.animations.play('ghost_anim_atack', 8, true);  
                     if(this.game.input.activePointer.isDown){
                         enemiesGhost_Life[i]--;
                         if(enemiesGhost_Life[i]==0){
                            pieceOfInfoGhost[i].piece.alpha = 1;
                            ghostKilled[i]=true;
                            enemys[i].ghost.kill(); 
                            enemys[i + nrenemies].ghost.kill();
                         }
                     }
                   element.value-=1;
                   
                   if(element.value<=0)
                    {
                        if(checkDeadPlayer == false){
                           waitingTime = this.game.time.now + 1400;
                           checkDeadPlayer = true;
                        }
                    }
                    if (this.game.time.now > waitingTime && checkDeadPlayer == true){
                        this.resetPlayer();
                        element.value=100;
                        player.reset(250,250);
                        playerCopy.reset(250,650);
                        checkDeadPlayer = false;
                        deadAnimPlayed = false;
                      
                    }
                     
                     
                }
                else{
                    //enemys[i].ghost.animations.play('ghost_anim_walk_down', 8, true);
                       if(Phaser.Math.distance(player.x, player.y, enemys[i].ghost.x, enemys[i].ghost.y) <= 500 && player.y<=300 
                         && Phaser.Math.distance(enemys[i].originx, enemys[i].originy, player.x, player.y) < 500){  
                           this.game.physics.arcade.moveToXY(enemys[i].ghost, player.x,player.y, 250);
                           this.game.physics.arcade.moveToXY(enemys[i+nrenemies].ghost, playerCopy.x,playerCopy.y, 250);
                        }
                       else 
                           { 
                            if(enemys[i].ghost.x != enemys[i].originx || enemys[i].ghost.y!= enemys[i].originy){  
                                this.game.physics.arcade.moveToXY(enemys[i].ghost, enemys[i].originx,enemys[i].originy, 200);
                                this.game.physics.arcade.moveToXY(enemys[i+nrenemies].ghost, enemys[i+nrenemies].originx,enemys[i+nrenemies].originy, 200);
                            }
                            //enemys[i].cookie3.body.velocity.x = 0;
                            //enemys[i].cookie3.body.velocity.y = 0;
                               
                         }
                      
                }
                
            }
        
        
        
        //urmrirea playerului de catre mobi - mummy
        for(i=0; i<nrEnemiesMummy; i++){
            if(checkOverlap(player,enemiesMummy[i].mummy) && enemiesMummy[i].mummy.alive){
                   if(this.game.input.activePointer.isDown){
                      enemiesMummy_Life[i]--;
                      if(enemiesMummy_Life[i]==0){
                          pieceOfInfoMummy[i].piece.x = enemiesMummy[i].mummy.x+30;
                          pieceOfInfoMummy[i].piece.y = enemiesMummy[i].mummy.y;
                          pieceOfInfoMummy[i].piece.alpha = 1;
                          mummyKilled[i]=true;
                         enemiesMummy[i].mummy.kill(); 
                         enemiesMummy[i + nrEnemiesMummy].mummy.kill();
                      }
                   }
                   element.value-=1;
                   if(element.value<=0)
                    {
                        if(checkDeadPlayer == false){
                           waitingTime = this.game.time.now + 1400;
                           checkDeadPlayer = true;
                        }
                    }
                   if (this.game.time.now > waitingTime && checkDeadPlayer == true){
                        this.resetPlayer();
                        element.value=100;
                        player.reset(250,250);
                        playerCopy.reset(250,650);
                        checkDeadPlayer = false;
                        deadAnimPlayed = false;
                      
                    }

                     
                }
                else{
                       if(Phaser.Math.distance(player.x, player.y, enemiesMummy[i].mummy.x, enemiesMummy[i].mummy.y) <= 500 && player.y<=300 
                         && Phaser.Math.distance(enemiesMummy[i].originx, enemiesMummy[i].originy, player.x, player.y) < 500 ){  
                           this.game.physics.arcade.moveToXY(enemiesMummy[i].mummy, player.x,player.y, 250);
                           this.game.physics.arcade.moveToXY(enemiesMummy[i+nrEnemiesMummy].mummy, playerCopy.x,playerCopy.y, 250);
                           //movment to player
                           if(player.x > enemiesMummy[i].mummy.x){
                               enemiesMummy[i].mummy.animations.play('mummy_anim_walk_right', 8, true);
                               enemiesMummy[i+nrEnemiesMummy].mummy.animations.play('mummy_anim_walk_right', 8, true);
                           }
                           else {
                               enemiesMummy[i].mummy.animations.play('mummy_anim_walk_left', 8, true);
                               enemiesMummy[i + nrEnemiesMummy].mummy.animations.play('mummy_anim_walk_left', 8, true);
                           }
                        }
                       else 
                           {   
                               if(enemiesMummy[i].mummy.x < enemiesMummy[i].originx-30 || enemiesMummy[i].mummy.x > enemiesMummy[i].originx+30 || 
                                  enemiesMummy[i].mummy.y < enemiesMummy[i].originy-30 || enemiesMummy[i].mummy.y > enemiesMummy[i].originy+30 ){
                                   this.game.physics.arcade.moveToXY(enemiesMummy[i].mummy, enemiesMummy[i].originx,enemiesMummy[i].originy, 100);
                                   this.game.physics.arcade.moveToXY(enemiesMummy[i+nrEnemiesMummy].mummy,enemiesMummy[i+nrEnemiesMummy].originx,
                                                              enemiesMummy[i+nrEnemiesMummy].originy, 100);
                                   
                                   if(enemiesMummy[i].mummy.x < enemiesMummy[i].originx){
                                       enemiesMummy[i].mummy.animations.play('mummy_anim_walk_right', 8, true);
                                       enemiesMummy[i+nrEnemiesMummy].mummy.animations.play('mummy_anim_walk_right', 8, true);
                                   }
                                   else{
                                       enemiesMummy[i].mummy.animations.play('mummy_anim_walk_left', 8, true);
                                       enemiesMummy[i+nrEnemiesMummy].mummy.animations.play('mummy_anim_walk_left', 8, true);
                                   }
                               }
                            //enemys[i].cookie3.body.velocity.x = 0;
                            //enemys[i].cookie3.body.velocity.y = 0;
                              
                               
                         }
                      
                }
        }
        
        //URMARIREA PLAYERULUI DE MONSTRI - SKELETONI
        
        for(i=0; i<nrEnemiesSkeleton; i++){
            if(checkOverlap(player,enemiesSkeleton[i].skeleton) && enemiesSkeleton[i].skeleton.alive){
                   if(this.game.input.activePointer.isDown){
//                         enemiesSkeleton[i].skeleton.kill(); 
//                         enemiesSkeleton[i + nrEnemiesSkeleton].skeleton.kill();
                       enemiesSkeleton_Life[i]--;
                      if(enemiesSkeleton_Life[i]==0){
                         pieceOfInfoSkeleton[i].piece.x = enemiesSkeleton[i].skeleton.x+30;
                         pieceOfInfoSkeleton[i].piece.y = enemiesSkeleton[i].skeleton.y;
                         pieceOfInfoSkeleton[i].piece.alpha = 1;
                         skeletonKilled[i]=true;
                         enemiesSkeleton[i].skeleton.kill(); 
                         enemiesSkeleton[i + nrEnemiesSkeleton].skeleton.kill();
                      }
                   }
                }
                else{
                       if(Phaser.Math.distance(player.x, player.y, enemiesSkeleton[i].skeleton.x, enemiesSkeleton[i].skeleton.y) <= 300 && enemiesSkeleton[i].skeleton.alive){  
                           //movment to player
                           enemiesSkeleton[i].skeleton.animations.play('skeleton_anim_attack_down', 8, true);
                           if (this.game.time.now > nextFire && bullets[i].countDead() > 0){
                              nextFire = this.game.time.now + fireRate;
                              bullet[i] = bullets[i].getFirstDead();
                              bullet[i].reset(enemiesSkeleton[i].skeleton.x , enemiesSkeleton[i].skeleton.y);
                              bullet[i].rotation = this.game.physics.arcade.angleToPointer(player);
           
                              //this.game.physics.arcade.moveToPointer(bullet, 2500);
                               this.game.physics.arcade.moveToXY(bullet[i], player.x,player.y, 500);
                              bullet[i].lifespan = 900;
                           }
                           
                           enemiesSkeleton[i+nrEnemiesSkeleton].skeleton.animations.play('skeleton_anim_attack_down', 8, true);

                        }
                       else{   
                           enemiesSkeleton[i].skeleton.animations.play('skeleton_anim_idle', 8, true); 
                           enemiesSkeleton[i + nrEnemiesSkeleton].skeleton.animations.play('skeleton_anim_idle', 8, true); 
                           
                       }
                      
                }
        }
        
        
        //OMORAREA CHESTURILOR
        for(i=0; i<5; i++){
            if(checkOverlap(player,chest[i])){
                if(this.game.input.activePointer.isDown){
                      chest_life[i]--;
                      if(chest_life[i]==0){
                           chest[i].animations.play('chest_open_anim', 7, true);
                           chest[i].animations.play('chest_opened_anim', 7, true);
                           chestCopy[i].animations.play('chest_copy_open_anim', 7, true);
                           chestCopy[i].animations.play('chest_copy_opened_anim', 7, true);
                           chestOpened[i] = true;
                           keys[i].key.alpha = 1;
//                           keys[i] = this.add.sprite(chest[i].x,chest[i].y+550,'key'); //check
//                           keys[i].anchor.setTo(0.5,0.5);
//                           this.physics.arcade.enable(keys[i]);
//                           keys[i].animations.add('keys_open_anim',[0]);
//                           keys[i].animations.play('chest_open_anim', 7, true);
//                           keys[i].body.collideWorldBounds = true;
//                           keys[i].body.immovable = true;
//                           keysText.setText("You have 1 key");
                          }
                }
            }
        }
        
        
        //ridicarea key-lor de pe jos
        for(i=0; i<5; i++){
            if(checkOverlap(playerCopy,keys[i].key) && chestOpened[i]){
                keys[i].key.kill();
                nrKeysUntilNow++;
                chestOpened[i] = false;
            }
        }
        
        //ridicarea Bucatilor de informatii de pe jos
        //Fantome
        for(i=0;i<nrenemies;i++){
            if(checkOverlap(playerCopy,pieceOfInfoGhost[i].piece) && ghostKilled[i]){
                pieceOfInfoGhost[i].piece.kill();
                nrPiecesInfoUntilNow++;
                ghostKilled[i] = false;
            }
        }
        //MUMII
        for(i=0;i<nrEnemiesMummy;i++){
            if(checkOverlap(player,pieceOfInfoMummy[i].piece) && mummyKilled[i]){
                pieceOfInfoMummy[i].piece.kill();
                nrPiecesInfoUntilNow++;
                mummyKilled[i] = false;
            }
        }
        //SKELETONI
        for(i=0;i<5;i++){
            if(checkOverlap(player,pieceOfInfoSkeleton[i].piece) && skeletonKilled[i]){
                pieceOfInfoSkeleton[i].piece.kill();
                nrPiecesInfoUntilNow++;
                skeletonKilled[i] = false;
            }
        }
        
        
      //MERSUL MONSTRILOR - ghosts
        var i;
        for(i=0;i<nrenemies + nrenemiesCopy;i++){
            if(enemys[i].ghost.y == enemys[i].originy && enemys[i].ghost.x == enemys[i].originx){
                enemys[i].ghost.animations.play('ghost_anim_walk_down', 8, true);
            }
//            else if(enemys[i].ghost.y == enemys[i].originy + 100){
//                enemys[i].ghost.animations.play('ghost_anim_walk_up', 8, true);
//            }
        }
        
        
               
        
        
    },
    
        resetPlayer : function(){
        this.spawn();
    },
    
    //SPAWN FUNCTION
    spawn : function(){    
        element = document.getElementById("HPBar");
        //alert(element.value);
        //respawn.forEach(function(spawnPoint){
        //    player.reset(spawnPoint.x,spawnPoint.y);
        //},this);
        isPlayer=true;
    },
    
    
    //WHEN HIT PORTAL JUMP TO LEVEL1 MAP
    goToLevelSecurityLeftRightMap :function(){
        this.state.start('LevelSecurityLeftRightMap');
    },
    
   destroyBullet : function(){
       bullet[i].kill();
   },
    
   
    
    
};


function togglePause() {

    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 99999999; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function checkOverlapCookie(spriteA,spriteB){
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();
    
    return Phaser.Rectangle.intersects(boundsA,boundsB);
}

function resetBullet(bullet) {
	// Destroy the laser
	bullet.kill();
}

//function actionInput(nrAnswer){
//    
//    
//}


