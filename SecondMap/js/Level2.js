Game.Level2 = function(game){
   
};

var map;
var layer;
var music;
var fire;

var player;
var controls = {};
var playerSpeed = 150;
var jumpTimer = 0;
 

Game.Level2.prototype = {
    create: function(){
        this.stage.backgroundColor = '#3A5963';
        
        this.map = this.game.add.tilemap('map2');             
       
        this.map.addTilesetImage('nature_tiles4');
        this.map.addTilesetImage('deserthouses');
        this.map.addTilesetImage('miniboat');
        this.map.addTilesetImage('water');
        this.map.addTilesetImage('train');
        this.map.addTilesetImage('trees');
                            
        this.botLayer = this.map.createLayer('botLayer');     
        this.secondBotLayer = this.map.createLayer('secondBotLayer');
        this.topLayer = this.map.createLayer('topLayer');
        this.topTopLayer = this.map.createLayer('topTopLayer');
        this.topTopTopLayer = this.map.createLayer('topTopTopLayer');

        this.botLayer.resizeWorld();
        
        
        //MUSIC
        music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        
        // FIRE SPRITE
        fire = this.add.sprite(225,575,'fire');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fire);
        var fire_anim = fire.animations.add('fire_anim');
        fire.animations.play('fire_anim', 12, true);
        fire.body.collideWorldBounds = true;
        fire.body.immovable = true;
        
        // doar un cazan SPRITE
        doarcazan = this.add.sprite(1159,278,'doarcazan');
        //doarcazan.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doarcazan);
        var doarcazan_anim = doarcazan.animations.add('doarcazan_anim');
        doarcazan.animations.play('doarcazan_anim', 7, true);
        //doarcazan.body.collideWorldBounds = true;
        doarcazan.body.immovable = true;
        
        // FIRE 2 SPRITE
        fire2 = this.add.sprite(1285,275,'fire2');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fire2);
        var fire2_anim = fire2.animations.add('fire2_anim');
        fire2.animations.play('fire2_anim', 4, true);
        //fire2.body.collideWorldBounds = true;
        fire2.body.immovable = true;
    
        
        
        
        //PLAYER
    
        
        this.map.setCollisionBetween(1, 100000, true, 'topLayer'); 
        this.map.setCollisionBetween(1, 100000, true, 'topTopLayer');//!!!
        //this.map.setCollisionBetween(1, 100000, true, 'topTopTopLayer');
        
        player = this.add.sprite(300,300,'player');
        player.anchor.setTo(0,0);
        
        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        this.physics.arcade.enable(player);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
        }
        
        
        
      
        
     
        
    },
    
    update:function(){
         
        this.physics.arcade.collide(player,this.botLayer);      
        this.physics.arcade.collide(player,this.topLayer);
        this.physics.arcade.collide(player,this.topTopLayer);
        //this.physics.arcade.collide(player,this.topTopTopLayer);
       // this.physics.arcade.collide(player,fire);
      
        
        
        
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        
        if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;       
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
    },
    
    
};