Game.Level = function(game){
   
};

var map;
var layer;
var music;

var player;
var controls = {};
var playerSpeed = 150;
var jumpTimer = 0;
 

Game.Level.prototype = {
    create: function(){
        this.stage.backgroundColor = '#3A5963';
        
        this.map = this.game.add.tilemap('map');       
        
        this.map.addTilesetImage('tiles1');
        this.map.addTilesetImage('nature_tiles');
        this.map.addTilesetImage('nature_tiles2');
        this.map.addTilesetImage('nature_tiles3');
        this.map.addTilesetImage('nature_tiles4');
        this.map.addTilesetImage('ghost');
        
       
        
        this.bottomLayer = this.map.createLayer('bottomLayer');
        this.bottomLayer = this.map.createLayer('secondBottomCollideLayer');
        this.secondBottomLayer = this.map.createLayer('secondBottomLayer');
        this.topLayer = this.map.createLayer('topLayer');
        this.moreThenTopLayer = this.map.createLayer('moreThenTopLayer');
        this.moreMoreThenTopLayer = this.map.createLayer('moreMoreThenTopLayer');
        this.objectLayer = this.map.createLayer('objectLayer');
        
        //this.map.setCollisionByExclusion([0]);
        this.bottomLayer.resizeWorld();
        
        music = this.game.add.audio('soundKey');
        music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        
        //PLAYER
        

        this.map.setCollisionBetween(1, 100000, true, 'topLayer');  //!!!
        
        player = this.add.sprite(100,560,'player');
        player.anchor.setTo(0.5,0.5);
        
        player.animations.add('idle',[0,1],1,true);
        player.animations.add('jump',[2],1,true);
        player.animations.add('run',[3,4,5,6,7,8],7,true);
        this.physics.arcade.enable(player);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
        }
        
        
        
      
        
     
        
    },
    
    update:function(){
        
        this.physics.arcade.collide(player,this.moreMoreThenTopLayer);
        this.physics.arcade.collide(player,this.moreThenTopLayer);
        this.physics.arcade.collide(player,this.bottomLayer); 
        //this.physics.arcade.collide(player,this.secondBottomCollideLayer);
        //this.physics.arcade.collide(player,this.secondBottomLayer);
        
        this.physics.arcade.collide(player,this.topLayer);
        
        
        
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        
        if(controls.right.isDown){
            player.animations.play('run');
            player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;        }
         
      
        if(controls.left.isDown){
            player.animations.play('run');
            player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;       
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('jump');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('jump');
         }
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
    },
    
    
};