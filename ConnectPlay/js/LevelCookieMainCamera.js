

//////ids///
var id=10;
var npcId=1;
var mainNpcId=1;
var mainNpcDialog= {pos :0 , dialog: null};
var aliasDialog={ pos: 0 , dialog: null};

var stoneNpcId=2;
var stoneNpcDialog={pos:0,dialog:null};
//////ids
var map;
var layer;
var music;
var fire;
var respawn;

var player;
var controls = {};
var playerSpeed = 750;
var jumpTimer = 0;
var pauseKey;
var element;
var isPlayer=false;

var gold=0;
var vizTopMapNpc1=0;
var vizTopMapNpc2=0;
var vizNpc=0;
var goldNeeded=25;
var goldPosted=0;

var substr="$$$";


var checkDeadMonster = new Array(33); // same number like nrenemies + 1
var numberOfMonsters = 5;
var shootTime = 0;
var bullets;
var bullet;
var fireRate = 250;
var nextFire = 0;
var check_if_shot;
var mouseTouchDown = false;
var numberOfCookies;
var enemys=[];
var cookies=[];
var nrenemies=32;
var enemiesRemain=32;

var gravex=1700;
var gravey=900;
var countframe=0;
var resetframe=-1;
var anomalyframe=-1;


EnemyCookieType3 = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.cookie3 =  game.add.sprite(x,y,'cookiemonster2');
    this.cookie3.anchor.setTo(0.5,0.5);
    this.cookie3.name = index.toString();
    //game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.cookie3);
        game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
        this.cookie3.animations.add('cookie3_anim');
        this.cookie3.animations.play('cookie3_anim', 12, true);
        this.cookie3.body.collideWorldBounds = true;
        this.cookie3.body.immovable = true;
        this.cookie3.body.allowGravity = false;
    
    //this.cookie3.body.immovable = true;
    //this.cookie3.body.collideWorldBounds = true;
    //this.cookie3.body.allowGravity = false;

   /* this.cookie3Tween = game.add.tween(this.cookie3).to({
        y: 50,
    },2000,'Linear',true,0,100,true); */
    
    //game.physics.arcade.moveToXY(this.cookie3, player.x,player.y, 200);
    
    
    //this.cookie3.reset(x,y);
};

EnemyCookieType2 = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.cookie3 =  game.add.sprite(x,y,'finalCookieMob');
    this.cookie3.anchor.setTo(0.5,0.5);
    this.cookie3.name = index.toString();
    //game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.cookie3);
        game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
        this.cookie3.animations.add('cookie3_anim');
        this.cookie3.animations.play('cookie3_anim', 8, true);
        this.cookie3.body.collideWorldBounds = true;
        this.cookie3.body.immovable = true;
        this.cookie3.body.allowGravity = false;
    
    //this.cookie3.body.immovable = true;
    //this.cookie3.body.collideWorldBounds = true;
    //this.cookie3.body.allowGravity = false;

   /* this.cookie3Tween = game.add.tween(this.cookie3).to({
        y: 50,
    },2000,'Linear',true,0,100,true); */
    
    //game.physics.arcade.moveToXY(this.cookie3, player.x,player.y, 200);
    
    
    //this.cookie3.reset(x,y);
};

function getDialog(monster_id,npcDialog)
{
    //alert("incerc sa preiau");
    $.ajax({
            type: 'POST',
            url: 'newphp.php',
            data: {dialogId: monster_id},
            dataType: 'json',
            cache: false,
            success: function(result) {
                npcDialog.dialog=result;
                npcDialog.pos=npcDialog.dialog[2][0]-1;
            },
        });
}

Phaser.Tilemap.prototype.setCollisionBetween = function (start, stop, collides, layer, recalculate) {

	if (collides === undefined) { collides = true; }
	if (layer === undefined) { layer = this.currentLayer; }
	if (recalculate === undefined) { recalculate = true; }

	layer = this.getLayer(layer);

	for (var index = start; index <= stop; index++)
	{
		if (collides)
		{
			this.collideIndexes.push(index);
		}
		else
		{
			var i = this.collideIndexes.indexOf(index);

			if (i > -1)
			{
				this.collideIndexes.splice(i, 1);
			}
		}
	}

	for (var y = 0; y < this.layers[layer].height; y++)
	{
		for (var x = 0; x < this.layers[layer].width; x++)
		{
			var tile = this.layers[layer].data[y][x];

			if (tile && tile.index >= start && tile.index <= stop)
			{
				if (collides)
				{
					tile.setCollision(true, true, true, true);
				}
				else
				{
					tile.resetCollision();
				}

				tile.faceTop = collides;
				tile.faceBottom = collides;
				tile.faceLeft = collides;
				tile.faceRight = collides;
			}
		}
	}

	if (recalculate)
	{
		//  Now re-calculate interesting faces
		this.calculateFaces(layer);
	}

	return layer;

};

Game.LevelCookieMainCamera = function(game){
};

 

Game.LevelCookieMainCamera.prototype = {
    create: function(){
        this.stage.backgroundColor = '#3A5963';
        pausedgame = false;
        
         this.game.time.advancedTiming = true;
        var i;
        for(i=0;i<=nrenemies;i++){
            checkDeadMonster[i] = false;
        }
        numberOfCookies = 0;
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('CookieMainCamera');             
      
        this.map.addTilesetImage('city_ground');
        this.map.addTilesetImage('ground_cookie');
        this.map.addTilesetImage('cookietrees3'); 
        //this.map.addTilesetImage('cookiewalls'); 
        this.map.addTilesetImage('cookiehouse1');
        this.map.addTilesetImage('cookiehouse2');
        this.map.addTilesetImage('city_tiles_2');
        this.map.addTilesetImage('walls');
        this.map.addTilesetImage('city_buildings');
                            
        this.layer1 = this.map.createLayer('layer1');     
        this.layer2 = this.map.createLayer('layer2');
        this.layer3 = this.map.createLayer('layer3');
        this.layer4 = this.map.createLayer('layer4');
        this.layer5 = this.map.createLayer('layer5');
        this.buildings1 = this.map.createLayer('buildings1');
        this.buildings2 = this.map.createLayer('buildings2');

        this.layer1.resizeWorld();
        
   
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
              
        
        //PLAYER
    
        this.map.setCollisionBetween(1, 100000, true, 'layer2',true);
        this.map.setCollisionBetween(1, 100000, true, 'buildings1',true);
        this.map.setCollisionBetween(1, 100000, true, 'buildings2',true);

        
        player = this.add.sprite(1360,1750,'cookieplayer');
        player.anchor.setTo(0.5,0.5);
        
         console.log("Am ajuns aici");
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        
        player.animations.add('walkdown',[0,1,2,3,4,5,6],13,true);
        player.animations.add('walkup',[8,9,10,11,12,13,14],13,true);
        player.animations.add('walkleft',[16,17,18,19,20,21,22],13,true);
        player.animations.add('walkright',[24,25,26,27,28,29,30],13,true);
        player.animations.add('idle',[7],4,true);
        
        
        //this.physics.arcade.enable(player);
        this.game.physics.enable(player, Phaser.Physics.ARCADE);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        player.body.allowRotation = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
             shoot : this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR),
            
            
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(togglePause, this);
        
        //SPRITES
        
        //Main camera npc
        mainNpc = this.add.sprite(1710,1670,'mainNpc');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(mainNpc);
        //topMapNpc.animations.add('topMapNpc_anim');
        //topMapNpc.animations.play('topMapNpc_anim', 12, true);
        mainNpc.body.collideWorldBounds = true;
        mainNpc.body.immovable = true;
        getDialog(mainNpcId,mainNpcDialog);
       // mainNpcDialogposition=mainNpcDialogs[2][0];
        
        //Stone near npc
        stoneMainNpc = this.add.sprite(1800,1670,'stoneMainNpc');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(stoneMainNpc);
        //topMapNpc.animations.add('topMapNpc_anim');
        //topMapNpc.animations.play('topMapNpc_anim', 12, true);
        stoneMainNpc.body.collideWorldBounds = true;
        stoneMainNpc.body.immovable = true;
        getDialog(stoneNpcId,stoneNpcDialog);
        
        //TOP NPC LEFT
        topMapNpc1 = this.add.sprite(110,75,'topMapNpc');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(topMapNpc1);
        //topMapNpc.animations.add('topMapNpc_anim');
        //topMapNpc.animations.play('topMapNpc_anim', 12, true);
        topMapNpc1.body.collideWorldBounds = true;
        topMapNpc1.body.immovable = true;
 
        //TOP NPC RIGHT
        topMapNpc2 = this.add.sprite(2170,75,'topMapNpc');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(topMapNpc2);
        //topMapNpc.animations.add('topMapNpc_anim');
        //topMapNpc.animations.play('topMapNpc_anim', 12, true);
        topMapNpc2.body.collideWorldBounds = true;
        topMapNpc2.body.immovable = true;
        
        
        
        //COINS
        for(i=0; i<nrenemies; i++)
            {
                cookieAnimation = this.add.sprite(1000,650,'cookieAnimation');
                cookieAnimation.anchor.setTo(0.5,0.5);
                this.physics.arcade.enable(cookieAnimation);
                cookieAnimation.animations.add('cookieAnimation4_anim',[0,1,4,5,8,9,12,13]);
                cookieAnimation.animations.play('cookieAnimation4_anim', 12, true);
                cookieAnimation.body.collideWorldBounds = true;
                cookieAnimation.body.immovable = true;
                cookieAnimation.visible = false;
                cookies.push(cookieAnimation);
            }
        cookieAnimation1 = this.add.sprite(250,1100,'cookieAnimation');
        cookieAnimation1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(cookieAnimation1);
        cookieAnimation1.animations.add('cookieAnimation1_anim',[0,1,4,5,8,9,12,13]);
        cookieAnimation1.animations.play('cookieAnimation_anim', 12, true);
        cookieAnimation1.body.collideWorldBounds = true;
        cookieAnimation1.body.immovable = true;
        cookieAnimation1.visible = false;
        
        cookieAnimation2 = this.add.sprite(1000,1100,'cookieAnimation');
        cookieAnimation2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(cookieAnimation2);
        cookieAnimation2.animations.add('cookieAnimation2_anim',[0,1,4,5,8,9,12,13]);
        cookieAnimation2.animations.play('cookieAnimation2_anim', 12, true);
        cookieAnimation2.body.collideWorldBounds = true;
        cookieAnimation2.body.immovable = true;
        cookieAnimation2.visible = false;
        
        cookieAnimation3 = this.add.sprite(250,650,'cookieAnimation');
        cookieAnimation3.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(cookieAnimation3);
        cookieAnimation3.animations.add('cookieAnimation3_anim',[0,1,4,5,8,9,12,13]);
        cookieAnimation3.animations.play('cookieAnimation3_anim', 12, true);
        cookieAnimation3.body.collideWorldBounds = true;
        cookieAnimation3.body.immovable = true;
        cookieAnimation3.visible = false;
        
        cookieAnimation4 = this.add.sprite(1000,650,'cookieAnimation');
        cookieAnimation4.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(cookieAnimation4);
        cookieAnimation4.animations.add('cookieAnimation4_anim',[0,1,4,5,8,9,12,13]);
        cookieAnimation4.animations.play('cookieAnimation4_anim', 12, true);
        cookieAnimation4.body.collideWorldBounds = true;
        cookieAnimation4.body.immovable = true;
        cookieAnimation4.visible = false;
        
        //FOUNTAIN
        
        fountain1 = this.add.sprite(1820,2000,'fountain1');
        fountain1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fountain1);
        fountain1.animations.add('fountain1_anim',[0,1,2]);
        fountain1.animations.play('fountain1_anim', 12, true);
        fountain1.body.collideWorldBounds = true;
        fountain1.body.immovable = true;
        
        //DOORS
        doors1 = this.add.sprite(745,285,'doors1');
        doors1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doors1);
        doors1.animations.add('doors1_anim',[8,10,12,14]);
        //doors1.animations.play('doors1_anim', 7, true);
        doors1.body.collideWorldBounds = true;
        doors1.body.immovable = true;
        
        doors2 = this.add.sprite(2825,285,'doors1');
        doors2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doors2);
        doors2.animations.add('doors2_anim',[8,10,12,14]);
        //doors2.animations.play('doors2_anim', 7, true);
        doors2.body.collideWorldBounds = true;
        doors2.body.immovable = true;
        
        //Portals
        portal1 = this.add.sprite(785,1605,'portal1');
        portal1.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal1);
        portal1.animations.add('portal1_anim');
        portal1.animations.play('portal1_anim', 7, true);
        portal1.body.collideWorldBounds = true;
        portal1.body.immovable = true;
        
        portal2 = this.add.sprite(785,1340,'portal1');
        portal2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal2);
        portal2.animations.add('portal2_anim');
        portal2.animations.play('portal2_anim', 7, true);
        portal2.body.collideWorldBounds = true;
        portal2.body.immovable = true;
        
        portal3 = this.add.sprite(2615,1605,'portal1');
        portal3.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal3);
        portal3.animations.add('portal3_anim');
        portal3.animations.play('portal3_anim', 7, true);
        portal3.body.collideWorldBounds = true;
        portal3.body.immovable = true;
        
        portal4 = this.add.sprite(2615,1340,'portal1');
        portal4.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(portal4);
        portal4.animations.add('portal4_anim');
        portal4.animations.play('portal4_anim', 7, true);
        portal4.body.collideWorldBounds = true;
        portal4.body.immovable = true;
        
        
        
        //MONSTERS
        for(i=0; i<nrenemies/2; i++)
            {
                randomx=Math.floor(Math.random() * 1100) + 2100;
                randomy=Math.floor(Math.random() * 750) + 350;
                enemy=new EnemyCookieType3(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                enemys.push(enemy);
                
            }
        for(i=nrenemies/2; i<nrenemies; i++)
            {
                randomx=(Math.floor(Math.random() * 1100) + 100);
                randomy=(Math.floor(Math.random() * 750) + 350);
                enemy=new EnemyCookieType2(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                enemys.push(enemy);
            }
      /*  enemy1 =  new EnemyCookieType3(0,this.game,750 , 950);
        enemy2 =  new EnemyCookieType3(1,this.game,200 , 950);
        enemy3 =  new EnemyCookieType3(2,this.game,750 , 700);
        enemy4 =  new EnemyCookieType3(3,this.game,200 , 700);*/
        
        
      bullets = this.game.add.group();
      bullets.enableBody = true;
      bullets.physicsBodyType = Phaser.Physics.ARCADE;
      bullets.createMultiple(30,'bullet',0,false);                //check nr of bullets
      //bullets.setAll('anchor.x',0.5);
      //bullets.setAll('anchor.y',0.5);
        
      bullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
      bullets.callAll('events.onOutOfBounds.add','events.onOutOfBounds', resetBullet);
       bullets.setAll('checkWorldBounds', true);
      //bullets.setAll('outOfBoundsKill',true);    
      //bullets.setAll('CheckWorldBounds',true);
        
    },
    
    
    update:function(){
         
        //countframe++;
      //  if(countframe>1000000) countframe=0;
        
        this.physics.arcade.collide(player,this.layer1);      
        this.physics.arcade.collide(player,this.layer2);
        this.physics.arcade.collide(player,topMapNpc1);
        this.physics.arcade.collide(player,topMapNpc2);
        this.physics.arcade.collide(player,mainNpc);
        this.physics.arcade.collide(player,this.buildings1);
        this.physics.arcade.collide(player,this.buildings2);
        this.physics.arcade.collide(player,doors1); 
        this.physics.arcade.collide(player,doors2);  
        this.physics.arcade.collide(bullets,this.layer2,this.destroyBullet,null,this);
        this.physics.arcade.collide(bullets,this.layer3,this.destroyBullet,null,this);
        this.physics.arcade.collide(bullets,this.layer4,this.destroyBullet,null,this);
        this.physics.arcade.collide(bullets,this.layer5,this.destroyBullet,null,this);
        this.physics.arcade.collide(bullets,this.buildings1,this.destroyBullet,null,this);
        this.physics.arcade.collide(bullets,this.buildings2,this.destroyBullet,null,this);
        
        
           
        
        //player.rotation = this.game.physics.arcade.angleToPointer(player);

        
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        if(element.value<100)
        element.value+=0.5;
        
      if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed; 
            
        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;  
            
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
        
        //Check if all mobs from left room are dead and check player-door collision teleport
        var ok1 = true;
        for(i=nrenemies/2; i<nrenemies; i++)
            if(enemys[i].cookie3.alive)
                {
                 ok1 = false;            
                }
        if(ok1){
            doors1.animations.play('doors1_anim', 4, true);
        }
        if(ok1){
            if(checkOverlap(player,doors1)){
                if(player.y<doors1.y){
                    player.reset(doors1.x,doors1.y + 100);
                }
                else if(player.y>doors1.y){
                    player.reset(doors1.x,doors1.y - 100);
                }
            }
        }
        
        //Check if all mobs from Right room are dead and player-door collision teleport
        var ok2 = true;
        for(i=0; i<nrenemies/2; i++)
            if(enemys[i].cookie3.alive)
                {
                 ok2 = false;            
                }
        if(ok2)doors2.animations.play('doors2_anim', 4, true);
        if(ok2){
            if(checkOverlap(player,doors2)){
                if(player.y<doors2.y){
                    player.reset(doors2.x,doors2.y + 100);
                }
                else if(player.y>doors2.y){
                    player.reset(doors2.x,doors2.y - 100);
                }
            }
        }
        
        if(checkOverlap(player,portal1)){
            player.reset(portal2.x,portal2.y - 150);
        }
        if(checkOverlap(player,portal2)){
            player.reset(portal1.x,portal1.y + 150);
        }
        if(checkOverlap(player,portal3)){
            player.reset(portal4.x,portal4.y - 150);
        }
        if(checkOverlap(player,portal4)){
            player.reset(portal3.x,portal3.y + 150);
        }
        
        //if(controls.shoot.isDown)
            //this.shootBullet();
        
        //new shoot !!!!!!!!!!!!
        if (this.game.input.activePointer.isDown)
        {
           /* if(this.game.input.activePointer.x + 530 > player.x){
                player.animations.play('walkright');
            }
            else if(this.game.input.activePointer.x - 480 < player.x){
                player.animations.play('walkleft');
            }*/

            
            if (this.game.time.now > nextFire && bullets.countDead() > 0)
            {
            nextFire = this.game.time.now + fireRate;
            bullet = bullets.getFirstDead();
            bullet.reset(player.x - 5 , player.y + 45);
            bullet.rotation = this.game.physics.arcade.angleToPointer(player);
           
            this.game.physics.arcade.moveToPointer(bullet, 2500);
                bullet.lifespan = 100;
                //bullet.body.velocity.x = 0;
                //bullet.body.velocity.y = 0;
            }
        }
        
   
        
        //check monster attacks player
        for(i=0; i<nrenemies; i++)
            {
                 if(checkOverlap(player,enemys[i].cookie3) && enemys[i].cookie3.alive){
                   element.value-=2;
                   if(element.value==0)
                    {
                        this.resetPlayer();
                        element.value=100;
                        player.reset(1360,1750);
                    }
                }
                else{
                       if(Phaser.Math.distance(player.x, player.y, enemys[i].cookie3.x, enemys[i].cookie3.y) <= 700 && player.y<=1250 && player.y >=330
                         && Phaser.Math.distance(enemys[i].originx, enemys[i].originy, player.x, player.y) < 700)  
                              this.game.physics.arcade.moveToXY(enemys[i].cookie3, player.x,player.y, 400);
                       else 
                           {   
                            this.game.physics.arcade.moveToXY(enemys[i].cookie3, enemys[i].originx,enemys[i].originy, 200);
                            //enemys[i].cookie3.body.velocity.x = 0;
                            //enemys[i].cookie3.body.velocity.y = 0;
                         }
                      
                }
                
            }
        
        
        for(i=0; i<nrenemies; i++)
            {
                if(checkOverlap(bullets,enemys[i].cookie3) && enemys[i].cookie3.alive){
                    bullet.kill();
                    bullet.body.velocity.x = 0;
                    bullet.body.velocity.y = 0;
                    cookies[i].x=enemys[i].cookie3.x;
                    cookies[i].y=enemys[i].cookie3.y;
                    cookies[i].body.velocity.x = 0;
                    cookies[i].body.velocity.y = 0;
                   // this.game.physics.arcade.moveToXY(cookies[i], enemys[i].cookie3.x,enemys[i].cookie3.y, 200);
                    checkDeadMonster[i] = true;
                    //enemys[i].cookie3.body.velocity.x = 0;
                    //enemys[i].cookie3.body.velocity.y = 0;

                    enemys[i].cookie3.kill();
                    cookies[i].visible = true;
                    this.destroyBullet;
                    
                    }
                
            }
     
        for(i=0; i<nrenemies; i++)
            {
                if(cookies[i]!=null)
                if(checkOverlap(player,cookies[i])){
                numberOfCookies = numberOfCookies + 1;
                gold++;
                repaintGold();
                cookies[i].x=gravex;
                cookies[i].y=gravey;
               // cookies[i].visible=false;
                
                }
            }
    
        if(checkOverlap(player,topMapNpc1)){
            if(vizTopMapNpc1 == 0){
                vizTopMapNpc1=1;
                vizNpc =0;
               // document.getElementById("MSG").innerHTML = "Te cunosc.sa Ai primit 10 banuti";
                gold+=10;
                repaintGold();
            }
        }
         if(checkOverlap(player,topMapNpc2)){
             if(vizTopMapNpc2 == 0){
                vizTopMapNpc2 = 1;
                vizNpc = 0;
                //document.getElementById("MSG").innerHTML = "Salut straine.Ai primit 5 banuti";  
                gold+=5;
                repaintGold();
             }
        }
        
        document.getElementById("ChatContent").innerHTML="";
        document.getElementById("ChatAnswer1").innerHTML="";
        document.getElementById("ChatAnswer2").innerHTML="";
        document.getElementById("ChatAnswer3").innerHTML="";
        document.getElementById("ChatAnswer4").innerHTML="";
       
                 /*for(i=0; i<nrenemies; i++)
                            {
                                if(enemys[i].cookie3.x!=enemys[i].originx || enemys[i].cookie3.y!=enemys[i].originy)
                                      {
                                        enemys[i].cookie3.x=enemys[i].originx;
                                        enemys[i].cookie3.y=enemys[i].originy;
                                        //anomalyframe=countframe;
                                        //  console.log("Noua pozitie "+enemys[i].cookie3.x+"  " +enemys[i].cookie3.y);
                                      }
                            }
                        }*/

        /* if(countframe==anomalyframe) 
             console.log("Suntem in "+anomalyframe+" si am resetat in "+resetframe);*/
         if(checkOverlap(player,mainNpc)){
             
             aliasDialog=mainNpcDialog;
             if(aliasDialog.pos==1 && gold>0 ){
                vizNpc = 1;
                vizTopMapNpc1 =0;
                vizTopMapNpc2 =0;
                //document.getElementById("MSG").innerHTML = "Mersi pt donatie";
                 goldPosted+=gold;
                gold=0;
                 repaintGold();  
                 
                 for(i=0; i<nrenemies/2; i++)
                     if(!enemys[i].cookie3.alive)
                         {
                             enemys[i].cookie3.reset(enemys[i].originx,enemys[i].originy);
                             //enemys[i].cookie3.x=enemys[i].originx;
                             //enemys[i].cookie3.y=enemys[i].originy; 
                             doors2.animations.stop(null,true);

                         }
                    
            } 
             if(aliasDialog.dialog[2].length > aliasDialog.pos)
             {
                 if( aliasDialog.dialog[0][aliasDialog.pos].indexOf(substr) !== -1 )
                     {
                        // console.log(goldPosted);
                         document.getElementById("ChatContent").innerHTML="Server> "+
                             aliasDialog.dialog[0][aliasDialog.pos].replace(substr,(goldNeeded-goldPosted));
                     }
                 else
                     document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
             if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
             document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
             if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
            document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
             if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
            document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
             if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
            document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3]; 
            }
             else
             {
                document.getElementById("ChatContent").innerHTML="";
                document.getElementById("ChatAnswer1").innerHTML="";
                document.getElementById("ChatAnswer2").innerHTML="";
                document.getElementById("ChatAnswer3").innerHTML="";
                document.getElementById("ChatAnswer4").innerHTML="";
            }
                                
        }
        else mainNpcDialog.pos=0;
         
        if(checkOverlap(player,stoneMainNpc)){
            if(aliasDialog!=stoneNpcDialog)
                stoneNpcDialog.pos=0;
             aliasDialog=stoneNpcDialog;
             if(aliasDialog.dialog[2].length > aliasDialog.pos)
             {
             document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
             if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
             document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
             if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
            document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
             if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
            document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
             if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
            document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
            }
             else
             {
                document.getElementById("ChatContent").innerHTML="";
                document.getElementById("ChatAnswer1").innerHTML="";
                document.getElementById("ChatAnswer2").innerHTML="";
                document.getElementById("ChatAnswer3").innerHTML="";
                document.getElementById("ChatAnswer4").innerHTML="";
            }
        }
        else stoneNpcDialog.pos=0;
     
               
             
    },
    resetPlayer : function(){
        this.spawn();
    },
    resetPlayerLeftDoorUp : function(){
        player.reset(717,91);
    },
    resetPlayerLeftDoorDown : function(){
        player.reset(717,350);
    },
    resetPlayerRightDoorUp : function(){
        player.reset(2800,91);
    },
    resetPlayerRightDoorDown : function(){
        player.reset(2800,350);
    },
    spawn : function(){
          
        element = document.getElementById("HPBar");
        //alert(element.value);
        respawn.forEach(function(spawnPoint){
            player.reset(spawnPoint.x,spawnPoint.y);
        },this);
        isPlayer=true;
    },

    render:function() {
	this.game.debug.text(this.game.time.fps, 2, 14, "#33DBFF");
        if(isPlayer)
            {
                //alert(player);
                this.game.debug.spriteInfo(player, 32, 130,"#33DBFF");
                
            }
    
    },
    shootBullet :function(){
        if(this.time.now > shootTime){
            bullet = bullets.getFirstExists(false);
            if(bullet){
                bullet.reset(player.x,player.y);
                
                bullet.body.velocity.x += 550;
                shootTime = this.time.now + 50;
                
                
            }
        }
   },
    
   destroyBullet : function(){
       bullet.kill();
   }
    
     
};

function nextDialog(answernr)
{
    console.log(aliasDialog.pos+"  "+aliasDialog.dialog[2].length);
    var nrAnswers=aliasDialog.dialog[1][aliasDialog.pos].length;
    //if(aliasDialog.dialog[1][aliasDialog])
    console.log(nrAnswers);
   if(aliasDialog.dialog[2].length > aliasDialog.pos)
       {
           var poz=aliasDialog.dialog[2][aliasDialog.pos];
           while(aliasDialog.dialog[2][aliasDialog.pos]==poz)
           aliasDialog.pos++;
           aliasDialog.pos+=answernr;
       }
     console.log("After "+aliasDialog.pos);
    /////CAZURI SPESIAL
    
    if(aliasDialog.dialog==mainNpcDialog.dialog && aliasDialog.pos==1 && gold==0)
       aliasDialog.pos+=nrAnswers - answernr;
    console.log("After S "+aliasDialog.pos);
    
    
}


function repaintGold()
{
    var canvas = document.getElementById("Gold");
    var ctx = canvas.getContext("2d");
     ctx.font = "20px Arial";
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillText(gold,50,15);
}

function checkOverlapCookie(spriteA,spriteB){
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();
    
    return Phaser.Rectangle.intersects(boundsA,boundsB);
}
function togglePause() {

    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

}
function resetBullet(bullet) {
	// Destroy the laser
	bullet.kill();
}


