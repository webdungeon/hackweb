

EnemyDragon = function(index,game,x,y){
    this.dragon =  game.add.sprite(x,y,'dragon1');
    this.dragon.anchor.setTo(0.5,0.5);
    this.dragon.name = index.toString();
    game.physics.enable(this.dragon,Phaser.Physics.ARCADE);
    this.dragon.body.immovable = true;
    this.dragon.body.collideWorldBounds = true;
    this.dragon.body.allowGravity = false;
    
    this.dragonTween = game.add.tween(this.dragon).to({
        y: this.dragon.y + 30
    },2000,'Linear',true,0,100,true);
}


Game.Level3 = function(game){
   
};

var map;
var layer;
var music;
var fire;
var respawn;

var player;
var controls = {};
var playerSpeed = 150;
var jumpTimer = 0;

var enemy1;

var shootTime = 0;
var bullets;
var pauseKey;

 

Game.Level3.prototype = {
    create: function(){
        
        this.stage.backgroundColor = '#3A5963';
        
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('map3');  
        
        this.map.addTilesetImage('ground6');
        this.map.addTilesetImage('ground4');
        this.map.addTilesetImage('trees');
        this.map.addTilesetImage('nature_tiles2');
        
       
     //this.map.addTilesetImage('ground');
          //this.map.addTilesetImage('treeslv3');
        //this.map.addTilesetImage('house');
          //this.map.addTilesetImage('dragon1');
      
                            
        this.botLayer = this.map.createLayer('botLayer');  
        this.botLayer = this.map.createLayer('botBotLayer');  
        this.botLayer = this.map.createLayer('topLayer');  
         //this.botLayer = this.map.createLayer('botBotLayer');
         //this.topLayer = this.map.createLayer('topLayer');
   

        this.botLayer.resizeWorld();
        
        
        // OBJECT LAYER
        
              //HERE IS THE START POSITION
        this.map.createFromObjects('objectLayer',1829 , '' ,0,true,false,respawn);
        
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
    
        //DOOR BACK TO Level 2
        doors2 = this.add.sprite(255,230,'doors2');
        doors2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doors2);
        var doors2_anim = doors2.animations.add('doors2_anim',[0,4,7,10]);
        doors2.animations.play('doors2_anim', 1, true);
        doors2.body.collideWorldBounds = true;
        doors2.body.immovable = true;
        
        
        
        //PLAYER
    
        
        this.map.setCollisionBetween(1, 100000, true, 'topLayer'); 
        
        
        player = this.add.sprite(0,0,'player');
        player.anchor.setTo(0.5,0.5);
        
        
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        this.physics.arcade.enable(player);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
            shoot : this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR),
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(togglePause, this);
        
        
        
      bullets = this.game.add.group();
      bullets.enableBody = true;
      bullets.physicsBodyType = Phaser.Physics.ARCADE;
      bullets.createMultiple(10,'bullet',0,false);                //check nr of bullets
      bullets.setAll('anchor.x',0.5);
      bullets.setAll('anchor.y',0.5);
        
        bullets.setAll('scale.x',0.5);
      bullets.setAll('scale.y',0.5);
         bullets.setAll('outOfBoundsKill',true);    
        bullets.setAll('CheckWorldBounds',true);
        
       
        
      enemy1 =  new EnemyDragon(0,this.game,1300 , 1300);
        enemy2 =  new EnemyDragon(0,this.game,600 , 600);
        enemy3 =  new EnemyDragon(0,this.game,700 , 1000);
        enemy4 =  new EnemyDragon(0,this.game,1000 , 300);
        enemy5 =  new EnemyDragon(0,this.game,900 , 1500);
        
        
    },
    
    update:function(){
        
        //this.physics.arcade.collide(player,this.botLayer);      
        this.physics.arcade.collide(player,this.topLayer);
        this.physics.arcade.collide(player,enemy1); 
  
        this.physics.arcade.collide(player,doors2,this.goToLevel2,null,this);
      
        
        
        
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        
        if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;       
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
        
        
        if(controls.shoot.isDown)
            this.shootBullet();
        
        if(checkOverlap(player,enemy1.dragon)){
            //this.resetPlayer();
            player.reset(1829,0);
        }
     
        
        if(checkOverlap(bullets,enemy1.dragon)){
            enemy1.dragon.kill();
            bullets.visible = false;
            
        }
     //   if(checkOverlap(player,enemy2.dragon)){
     //       this.resetPlayer();
     //   }
        
        if(checkOverlap(bullets,enemy2.dragon)){
            enemy2.dragon.kill();
        }
      //  if(checkOverlap(player,enemy3.dragon)){
      //      this.resetPlayer();
      //  }
        
        if(checkOverlap(bullets,enemy3.dragon)){
            enemy3.dragon.kill();
        }
      //  if(checkOverlap(player,enemy4.dragon)){
      //      this.resetPlayer();
      //  }
        
        if(checkOverlap(bullets,enemy4.dragon)){
            enemy4.dragon.kill();
        }
     //   if(checkOverlap(player,enemy5.dragon)){
     //       this.resetPlayer();
     //   }
        
        if(checkOverlap(bullets,enemy5.dragon)){
            enemy5.dragon.kill();
        }
        
              
        
        
        
    },
    
    //RESET PLAYER FUNCTION
    
    resetPlayer : function(){
        this.spawn();
    },
    
    
    //SPAWN FUNCTION
    spawn : function(){
        respawn.forEach(function(spawnPoint){
            player.reset(spawnPoint.x,spawnPoint.y);
        },this);
    },
    
    
    //WHEN HIT FIRE JUMP TO LEVEL2 MAP
    goToLevel2 :function(){
        this.state.start('Level2');
    },
    
    shootBullet :function(){
        if(this.time.now > shootTime){
            bullet = bullets.getFirstExists(false);
            if(bullet){
                bullet.reset(player.x,player.y);
                bullet.body.velocity.x += 250;
                shootTime = this.time.now + 50;
                
            }
        }
  },
    
    
    
    
    
    
    

    
};


function checkOverlap(spriteA,spriteB){
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();
    
    return Phaser.Rectangle.intersects(boundsA,boundsB);
}

function togglePause() {

    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

}

