create or replace type t_col is object
(
nume varchar2(25),
last_map number(2),
coins number(4),
nivel number(2),
experience number(5)
);
/
create or replace type t_nested_table is table of t_col;
/

create or replace package p_account as

function rank_table(playerlv savegame.player_level%type,maplv map.nivel%type) return t_nested_table;
function exp_monter_earned(playerlv savegame.player_level%type, maplv map.nivel%type, monsterlv monster_type.nivel%type, penality float) return float;
end p_account;
/

create or replace package body p_account as

function rank_table(playerlv savegame.player_level%type,maplv map.nivel%type) return t_nested_table as
v_ret t_nested_table;
v_row t_col;
cursor c_rank is
select p.username as "name" ,m.nivel as "MapLevel",player_coins as "Coins" ,max(player_experience) as "Exp" ,max(player_level) as "Level" from savegame s
join player p on p.id=s.player_id 
join map m on s.map_id=m.id
group by p.username,player_coins,player_id,m.nivel
order by 5 desc ,4 desc;

type t_rank is table of c_rank%rowtype;
r_rank t_rank;


begin
  v_ret := t_nested_table();
  open c_rank;
  fetch c_rank bulk collect into r_rank;
  close c_rank;
  for indx in r_rank.first..r_rank.last loop
  if(playerlv is not null and maplv is not null) then
      if(r_rank(indx)."MapLevel" = maplv and r_rank(indx)."Level"=playerlv ) then
         v_ret.extend;
         v_ret(v_ret.count) := t_col(r_rank(indx)."name",r_rank(indx)."MapLevel",r_rank(indx)."Coins",r_rank(indx)."Level",r_rank(indx)."Exp");
      end if;
  else if(playerlv is not null)then
          if( r_rank(indx)."Level"=playerlv ) then
              v_ret.extend;
              v_ret(v_ret.count) := t_col(r_rank(indx)."name",r_rank(indx)."MapLevel",r_rank(indx)."Coins",r_rank(indx)."Level",r_rank(indx)."Exp");
          end if;
       else if (maplv is not null) then
                if(maplv= r_rank(indx)."MapLevel") then
                v_ret.extend;
                 v_ret(v_ret.count) := t_col(r_rank(indx)."name",r_rank(indx)."MapLevel",r_rank(indx)."Coins",r_rank(indx)."Level",r_rank(indx)."Exp");
                 end if;
            else 
                v_ret.extend;
                 v_ret(v_ret.count) := t_col(r_rank(indx)."name",r_rank(indx)."MapLevel",r_rank(indx)."Coins",r_rank(indx)."Level",r_rank(indx)."Exp");
            end if;
        end if;
    end if;
  --dbms_output.put_line(r_rank(indx).player_coins||' '||r_rank(indx)."Level");
  end loop;

  return v_ret;
end;

function exp_monter_earned(playerlv savegame.player_level%type, maplv map.nivel%type, monsterlv monster_type.nivel%type, penality float) return float as
base_exp float :=2500.0;
monster_diff float;
map_diff float;
begin
base_exp:=base_exp/playerlv*2;
monster_diff := (playerlv - monsterlv) * 0.015;
map_diff := (playerlv - maplv) * 0.01;
dbms_output.put_line(monster_diff||' '||map_diff);
dbms_output.put_line(base_exp);
if(monster_diff < 0 ) then
base_exp := base_exp + base_exp * monster_diff * -1;
else if(monster_diff >0 ) then base_exp :=base_exp - base_exp * monster_diff ;
      end if;
end if;
dbms_output.put_line(base_exp);
if(map_diff < 0 ) then
base_exp := base_exp + base_exp * monster_diff * -1;
else if(map_diff >0 ) then base_exp :=base_exp - base_exp * monster_diff ;
      end if;
end if;
dbms_output.put_line(base_exp);
if(penality > 0 ) then
base_exp := base_exp - base_exp *penality;
end if;
return base_exp;
end ;


end p_account;
/