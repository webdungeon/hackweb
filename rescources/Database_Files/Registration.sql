create or replace package p_Registration as
function login(name player.username%type , pass player.password%type) return player.id%type ;
function register(name player.username%type , pass player.password%type, mail player.email%type) return player.id%type;
function register_cookie(name player.username%type) return player.id%type;
procedure cleanup ;

end p_Registration;
/

create or replace package body p_Registration as

function register(name player.username%type , pass player.password%type, mail player.email%type) return player.id%type is
v_id player.id%type;
v_temp_id player.id%type;
v_date player.creation_date%type;
begin

loop
 v_temp_id := trunc(dbms_random.value(1,999999999));
  select count(id) into v_id from savegame where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id:=v_temp_id;
  
select SYSTIMESTAMP into v_date from dual;
insert into player values(v_id,name,pass,mail,v_date);
return v_id;
end;

function register_cookie(name player.username%type) return player.id%type is
v_id player.id%type;
begin
v_id:= register(name,null,null);
return v_id;
end;

procedure cleanup is
v_dif int;
begin
delete from player where
(EXTRACT (DAY FROM (SYSTIMESTAMP-creation_date))*24*60*60+
EXTRACT (HOUR FROM (SYSTIMESTAMP-creation_date))*60*60+
EXTRACT (MINUTE FROM (SYSTIMESTAMP-creation_date))*60+
EXTRACT (SECOND FROM (SYSTIMESTAMP-creation_date))) >=600
and password is null;
end;

function login(name player.username%type , pass player.password%type) return player.id%type  as
v_id player.id%type;
v_check  int;
begin
select count(id) into v_check from player where username=name and password=pass;
if( v_check = 0 ) then
v_id := -1;
else
select id into v_id from player where username=name and password=pass;
end if;
return v_id;
end;

end p_Registration;
