Game.Preloader = function(game){
    this.preloadBar = null;
};

Game.Preloader.prototype = {
    preload:function(){
        this.preloadBar = this.add.sprite(this.world.centerX,this.world.centerY,'preloadBar');
 
        this.preloadBar.anchor.setTo(0.5,0.5);
        this.time.advancedTiming = true ;
        this.load.setPreloadSprite(this.preloadBar);
        
        //LOAD ALL ASSETS
        this.load.tilemap('map', 'assets//tilemaps/level1.json', null, Phaser.Tilemap.TILED_JSON);
              
        this.load.image('tiles1','assets/level1Images/level1img3.png');
        this.load.image('nature_tiles','assets/level1Images/nature_tiles.png');
        this.load.image('nature_tiles2','assets/level1Images/nature_tiles2.png');
        this.load.image('nature_tiles3','assets/level1Images/nature_tiles3.png');
        this.load.image('nature_tiles4','assets/level1Images/nature_tiles4.png');
        this.load.image('ghost','assets/level1Images/ghost.png');
        
        this.load.spritesheet('player','assets/level1Images/player.png',24,26);
        
        this.game.load.audio("soundKey", "assets/sounds/oliver.mp3");
        
    },
    
    create:function(){
       
    this.state.start('Level');
}
    
    
};