Game.Preloader = function(game){
    this.preloadBar = null;
};

Game.Preloader.prototype = {
    preload:function(){
        this.preloadBar = this.add.sprite(this.world.centerX,this.world.centerY,'preloadBar');
 
        this.preloadBar.anchor.setTo(0.5,0.5);
        this.time.advancedTiming = true ;
        this.load.setPreloadSprite(this.preloadBar);
        
        //LOAD ALL ASSETS
        this.load.tilemap('map', 'assets//tilemaps/level1.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.tilemap('map2', 'assets//tilemaps/level2.json', null, Phaser.Tilemap.TILED_JSON);
        
              
        this.load.image('tiles1','assets/level1Images/level1img3.png');
        this.load.image('nature_tiles','assets/level1Images/nature_tiles.png');
        this.load.image('nature_tiles2','assets/level1Images/nature_tiles2.png');
        this.load.image('nature_tiles3','assets/level1Images/nature_tiles3.png');
        this.load.image('nature_tiles4','assets/level1Images/nature_tiles4.png');
        this.load.image('ghost','assets/level1Images/ghost.png');
        
       
        //level 2
        this.load.image('deserthouses','assets/level2Images/deserthouses.png');
        this.load.image('miniboat','assets/level2Images/miniboat.png');
        this.load.image('water','assets/level2Images/water.png');
        this.load.image('train','assets/level2Images/train.png');
        this.load.image('trees','assets/level2Images/trees.png');
        
        
        
        //PLAYER
        //this.load.spritesheet('player','assets/level1Images/player.png',24,26);  //girl player
        //this.load.spritesheet('player','assets/level2Images/player6.png',107,96);   //horse player
        //this.load.spritesheet('player','assets/level2Images/dragonplayer.png',192,192);  //dragon player
        this.load.spritesheet('player','assets/level2Images/player8.png',32,64);  // normal player
        
        
        
        
        // ANIMATIONS
        this.load.spritesheet('fire','assets/level2Images/fire.png',64,59,3);
        
        this.load.spritesheet('doarcazan','assets/level2Images/doarcazan.png',31.75,41,3);
        
        this.load.spritesheet('fire2','assets/level2Images/fire2.png',64,64,3);
        
        
        
        //SOUNDS
        this.game.load.audio("soundKey", "assets/sounds/oliver.mp3");
        
    },
    
    create:function(){
       
    this.state.start('Level2');
}
    
    
};