declare 

type namesarray is VARRAY(100) of skill.skill_type%type;
skillsnames namesarray;
total int;
begin

skillsnames:= namesarray('HTML','CSS','JavaScrip','Ajax','Rest','Others');
total:=skillsnames.count;
FOR i in 1 .. total LOOP 
      insert into t_skill_types values(skillsnames(i));
   END LOOP; 


end;


declare
type namesarray IS VARRAY(500000) OF VARCHAR2(30); 
 multenume namesarray;
 total int;
begin
multenume := namesarray('Aaliyah','Aaron','Aarushi','Abagail','Abbey','Abbi','Abbie','Abby','Abdul','Abdullah'
,'Abe','Abel','Abi','Abia','Abigail','Abraham','Abram','Abree','Abrianna','Abriel'
,'Abrielle','Aby','Acacia','Ace','Ada','Adair','Adalia','Adaline','Adalyn','Adam'
,'Adan','Addie','Addilyn','Addison','Addison','Ade','Adelaide','Adele','Adelene','Adelia'
,'Adelina','Adeline','Adelynn','Aden','Adison','Adnan','Adon','Adonis','Adora','Adreanna'
,'Adrian','Adriana','Adrianna','Adrianne','Adriel','Adrienne','Ady','Aerona','Aeryn','Agatha'
,'Aggie','Agnes','Ahmad','Ahmed','Aida','Aidan','Aiden','Aileen','Ailsa','Aimee'
,'Aine','Ainsleigh','Ainsley','Ainsley','Aisha','Aislin','Aisling','Aislinn','Aislynn','Ajay'
,'Al','Alain','Alaina','Alan','Alana','Alanis','Alanna','Alannah','Alaric','Alaska'
,'Alastair','Alayah','Alayna','Alba','Albany','Albany','Albert','Alberta','Albertina','Alberto'
,'Albie','Albus','Alden','Aldo','Aldric','Aldrich','Aldrin','Aleah','Alec','Alecia'
,'Aled','Aleisha','Alejandra','Alejandro','Alen','Alena','Alesandro','Alessandra','Alessia','Alex'
,'Alex','Alexa','Alexander','Alexandra','Alexandria','Alexia','Alexis','Alexis','Alexus','Alfie'
,'Alfonse','Alfonso','Alfred','Alfredo','Ali','Ali','Alia','Aliana','Alice','Alicia'
,'Alina','Alisa','Alisha','Alison','Alissa','Alistair','Alivia','Aliyah','Aliza','Alize'
,'Alka','Allan','Allen','Allie','Allison','Ally','Allyson','Alma','Alois','Alondra'
,'Alonzo','Aloysius','Alphonso','Alton','Alvin','Alya','Alycia','Alyshialynn','Alyson','Alyssa'
,'Alysse','Alyssia','Amalia','Amanda','Amandine','Amani','Amara','Amari','Amari','Amaris'
,'Amaryllis','Amaya','Amber','Amberly','Amberlyn','Ambrose','Amelia','Amelie','America','Amethyst'
,'Ami','Amie','Amily','Amina','Amir','Amirah','Amit','Amity','Amon','Amos'
,'Amy','Amya','Ana','Anabel','Anabelle','Anahi','Anais','Anamaria','Anand','Ananya'
,'Anastasia','Anderson','Andie','Andre','Andrea','Andreas','Andres','Andrew','Andromeda','Andy'
,'Anemone','Angel','Angel','Angela','Angelia','Angelica','Angelina','Angeline','Angelique','Angelo'
,'Angie','Angus','Anika','Anisa','Anissa','Anita','Aniya','Aniyah','Anjali','Ann'
,'Anna','Annabel','Annabella','Annabelle','Annabeth','Annalisa','Annalise','Annamaria','Anne','Anneke'
,'Annemarie','Annette','Annie','Annika','Annmarie','Anoushka','Ansel','Anson','Anthea','Anthony'
,'Antoinette','Anton','Antonia','Antonio','Antony','Anuja','Anusha','Anushka','Anwen','Anya'
,'Aoibhe','Aoibheann','Aoife','Aphrodite','Apollo','Apple','April','Aqua','Arabella','Arabelle'
,'Aran','Archer','Archibald','Archie','Arden','Aretha','Ari','Aria','Ariadne','Ariana'
,'Ariane','Arianna','Arianne','Ariel','Ariella','Arielle','Arin','Arisha','Arjun','Arleen'
,'Arlen','Arlene','Arlette','Arlo','Arman','Armando','Arnie','Arnold','Aron','Arran'
,'Arrie','Art','Artemis','Arthur','Arturo','Arun','Arwen','Arwin','Arya','Asa'
,'Asad','Ash','Asha','Ashanti','Ashby','Ashe','Asher','Ashia','Ashlee','Ashleigh'
,'Ashley','Ashley','Ashlie','Ashlyn','Ashlynn','Ashton','Ashton','Ashtyn','Ashvini','Ashwin'
,'Asia','Asma','Aspen','Aspen','Aster','Aston','Astoria','Astra','Astrid','Aswin'
,'Athalia','Athan','Athena','Athene','Atlanta','Atticus','Aubreanna','Aubree','Aubrey','Aubrey'
,'Aubriana','Aubrielle','Auburn','Auden','Audra','Audrey','Audriana','Audric','Audrina','Audwin'
,'August','Augustina','Augustine','Augustus','Aura','Aurelia','Aurora','Austen','Austin','Autumn'
,'Ava','Avaline','Avalon','Aven','Avery','Avery','Avia','Avon','Avriella','Avril'
,'Axel','Axton','Aya','Ayaan','Ayana','Ayanna','Ayden','Ayesha','Ayisha','Ayla'
,'Ayrton','Azalea','Azaria','Azariah','Bailey','Bailey','Barack','Barbara','Barbie','Barclay'
,'Barnaby','Barney','Barrett','Barron','Barry','Bart','Bartholomew','Basil','Bastian','Baxter'
,'Bay','Bay','Baylee','Baylor','Bea','Bear','Beatrice','Beatrix','Beau','Beauregard'
,'Becca','Beccy','Beck','Beckett','Becky','Belinda','Bella','Bellamy','Bellatrix','Belle'
,'Ben','Benedict','Benita','Benjamin','Benji','Benjy','Bennett','Bennie','Benny','Benson'
,'Bentley','Bently','Benton','Berenice','Bernadette','Bernard','Bernardo','Bernice','Bernie','Bert'
,'Bertha','Bertie','Bertram','Beryl','Bess','Bessie','Beth','Bethan','Bethanie','Bethany'
,'Betsy','Bettina','Betty','Bev','Bev','Bevan','Beverly','Bevin','Bevis','Bexley'
,'Beyonce','Bianca','Bill','Billie','Billy','Birchard','Bjorn','Bladen','Blain','Blaine'
,'Blair','Blair','Blaire','Blaise','Blake','Blake','Blakely','Blanche','Blane','Blaze'
,'Blaze','Blessing','Bliss','Bloom','Blossom','Blue','Blythe','Bob','Bobbi','Bobbie'
,'Bobby','Bobby','Bodie','Bogdan','Bonita','Bonnie','Bonquesha','Booker','Boris','Boston'
,'Bowen','Boyd','Brad','Braden','Bradford','Bradley','Bradwin','Brady','Braeden','Braelyn'
,'Braiden','Bram','Branden','Brandi','Brandon','Brandy','Branson','Brantley','Brax','Braxton'
,'Bray','Brayan','Brayden','Braydon','Braylee','Braylen','Braylon','Brayson','Brea','Breanna'
,'Breck','Brecken','Breckin','Bree','Breeze','Brenda','Brendan','Brenden','Brendon','Brenna'
,'Brennan','Brennon','Brent','Brentley','Brenton','Bret','Brett','Brevin','Brevyn','Bria'
,'Brian','Briana','Brianna','Brianne','Briar','Brice','Bridget','Bridgette','Bridie','Bridie'
,'Brie','Brie','Briella','Brielle','Brig','Brighton','Brigid','Brigitte','Briley','Brinley'
,'Brinley','Briony','Brisa','Bristol','Britney','Britt','Brittany','Brittney','Brock','Brod'
,'Broden','Broderick','Brodie','Brodie','Brody','Brogan','Brogan','Bronagh','Bronson','Bronte'
,'Bronwen','Bronwyn','Brook','Brook','Brooke','Brooke','Brooklyn','Brooklyn','Brooklynn','Brooks'
,'Bruce','Bruno','Bryan','Bryanna','Bryant','Bryce','Bryden','Brydon','Brylee','Bryleigh'
,'Bryler','Bryn','Bryn','Brynlee','Brynn','Bryon','Bryony','Bryson','Bryton','Buck'
,'Buddy','Bunny','Bunty','Burt','Burton','Buster','Butch','Byron','Cadby','Cade'
,'Caden','Cadence','Cael','Caelan','Caesar','Cai','Caiden','Caila','Cailin','Cailyn'
,'Cain','Caitlan','Caitlin','Caitlyn','Caius','Cal','Cale','Caleb','Caleigh','Calhoun'
,'Cali','Calista','Callan','Callen','Callie','Calliope','Callista','Callum','Calum','Calvin'
,'Calypso','Cam','Cambree','Cambria','Camden','Camden','Camelia','Cameron','Cameron','Cami'
,'Camila','Camilla','Camille','Campbell','Campbell','Camron','Camry','Camryn','Candace','Candice'
,'Candis','Candy','Cane','Caoimhe','Caolan','Caprice','Cara','Careen','Carenza','Carey'
,'Carin','Carina','Caris','Carissa','Carl','Carla','Carlene','Carley','Carlie','Carlisle'
,'Carlos','Carlotta','Carlton','Carly','Carlyn','Carlynn','Carmel','Carmela','Carmen','Carol'
,'Carole','Carolina','Caroline','Carolyn','Carrie','Carsen','Carson','Carsten','Carter','Carter'
,'Cary','Carys','Case','Casey','Casey','Cash','Cason','Casper','Caspian','Cass'
,'Cassandra','Cassi','Cassia','Cassidy','Cassie','Cassiopeia','Cassius','Castiel','Castor','Cat'
,'Catalina','Catarina','Cate','Catelyn','Caterina','Cathal','Cathalina','Catherine','Cathleen','Cathryn'
,'Cathy','Catlin','Cato','Catrina','Catriona','Cavan','Cayden','Caydon','Cayla','Cayleigh'
,'Cayson','Ceanna','Cece','Cecelia','Cecil','Cecile','Cecilia','Cecily','Cedric','Celeste'
,'Celestia','Celestine','Celia','Celina','Celine','Celise','Ceri','Cerise','Cerys','Cesar'
,'Chad','Chadwick','Chance','Chandler','Chanel','Chanelle','Channing','Chantal','Chantel','Chantelle'
,'Charis','Charissa','Charity','Charla','Charleigh','Charlene','Charles','Charlette','Charley','Charley'
,'Charlie','Charlie','Charlize','Charlotte','Charlton','Charmaine','Chas','Chase','Chastity','Chauncey'
,'Chayton','Chaz','Che','Chelsea','Chelsey','Chenai','Chenille','Cher','Cheri','Cherie'
,'Cherish','Cherry','Cheryl','Chesney','Chester','Chevy','Cheyanne','Cheyenne','Chiara','Chip'
,'Chloe','Chole','Chris','Chris','Chrissy','Christa','Christabel','Christal','Christelle','Christen'
,'Christi','Christian','Christiana','Christiane','Christie','Christina','Christine','Christopher','Christy','Chrysanthemum'
,'Chrystal','Chuck','Chyanne','Cia','Cian','Ciara','Ciaran','Cicely','Cici','Ciel'
,'Ciera','Cierra','Cilla','Cillian','Cindy','Clair','Claire','Clancy','Clara','Clarabelle'
,'Clare','Clarence','Clarice','Claris','Clarissa','Clarisse','Clarity','Clark','Clary','Claude'
,'Claudette','Claudia','Claudine','Clay','Clayton','Clea','Clemence','Clement','Clementine','Cleo'
,'Cleopatra','Cletus','Cliff','Clifford','Clifton','Clint','Clinton','Clive','Clodagh','Cloe'
,'Clotilde','Clover','Clovis','Clyde','Coby','Coco','Cody','Cohen','Colby','Cole'
,'Coleen','Colette','Colin','Colleen','Collin','Colm','Colt','Colten','Colton','Conan'
,'Conlan','Conner','Connie','Connor','Conor','Conrad','Constance','Constantine','Cooper','Cora'
,'Coral','Coralie','Coraline','Corbin','Cordelia','Corey','Corey','Cori','Corina','Corinne'
,'Cormac','Cornelia','Cornelius','Corra','Cortney','Cory','Cosette','Courtney','Craig','Cressida'
,'Crispin','Cristal','Cristian','Cristina','Cristobal','Crosby','Cruz','Crystal','Cullen','Curt'
,'Curtis','Cuthbert','Cyndi','Cynthia','Cyra','Cyril','Cyrus','Dabria','Dacey','Dacey'
,'Dacia','Daelyn','Dagmar','Dahlia','Daina','Daire','Daisy','Dakota','Dakota','Dale'
,'Dale','Dalen','Dallas','Dalon','Dalton','Damarion','Damaris','Damian','Damien','Damion'
,'Damon','Dan','Dana','Dana','Danae','Dane','Danette','Dani','Danica','Daniel'
,'Daniela','Daniella','Danielle','Danika','Danny','Dante','Daphne','Dara','Dara','Daragh'
,'Darby','Darcey','Darcie','Darcy','Darcy','Daren','Daria','Darian','Dariel','Darin'
,'Dario','Darius','Darla','Darlene','Darnell','Darragh','Darrel','Darrell','Darren','Darrin'
,'Darryl','Darryn','Darwin','Daryl','Dash','Dashawn','Dasia','Dave','David','Davida'
,'Davin','Davina','Davion','Davis','Dawn','Dawson','Dax','Daxon','Daxter','Daxton'
,'Dayle','Daylen','Daylon','Dayna','Daysha','Dayton','Deacon','Dean','Deana','Deandra'
,'Deandre','Deann','Deanna','Deanne','Deb','Debbie','Debby','Debora','Deborah','Debra'
,'Declan','Dede','Dee','Dee','Deedee','Deena','Deepak','Deidre','Deirdre','Deja'
,'Deklan','Delana','Delaney','Delanie','Delany','Delbert','Delia','Delilah','Delina','Della'
,'Delores','Delphine','Delvin','Demeter','Demetria','Demetrius','Demi','Dempsey','Dena','Denice'
,'Denis','Denise','Dennis','Denny','Denny','Denver','Denzel','Deon','Derek','Derica'
,'Derik','Dermot','Derrick','Des','Deshaun','Deshawn','Desiree','Desmond','Dessa','Destinee'
,'Destiny','Dev','Devin','Devlin','Devon','Devyn','Dewayne','Dewey','Dex','Dexter'
,'Diamond','Diana','Diane','Dianna','Dianne','Diarmuid','Dick','Dicky','Didi','Dido'
,'Diego','Diesel','Digby','Dilan','Dillon','Dilys','Dimitri','Dina','Dinah','Dinesh'
,'Dino','Dion','Dionne','Dior','Dirk','Dixie','Dixon','Django','Dmitri','Dolley'
,'Dolly','Dolores','Dom','Dominic','Dominick','Dominique','Don','Donal','Donald','Donna'
,'Donnie','Donny','Donovan','Dora','Doreen','Dorian','Doriana','Dorinda','Doris','Dorla'
,'Dorothy','Dory','Dot','Dottie','Doug','Dougie','Douglas','Doyle','Drake','Draven'
,'Drayden','Drew','Drew','Drusilla','Duane','Dudley','Dug','Duke','Dulce','Dulcie'
,'Duncan','Dustin','Dusty','Dwayne','Dwight','Dylan','Dympna','Eabha','Eadie','Eamon'
,'Earl','Earnest','Eason','Easton','Eathan','Eben','Ebenezer','Ebony','Echo','Ed'
,'Eddie','Eddy','Eden','Eden','Edgar','Edie','Edison','Edith','Edlyn','Edmund'
,'Edna','Edouard','Edric','Edsel','Edson','Eduardo','Edward','Edwardo','Edwin','Edwina'
,'Edyn','Effie','Efrain','Efren','Egan','Egon','Eibhlin','Eileen','Eilidh','Eilish'
,'Eimear','Eireann','Eisley','Elaina','Elaine','Elana','Elbert','Eldon','Eleanor','Electra'
,'Elektra','Elen','Elena','Eleonora','Eli','Elian','Eliana','Elias','Elicia','Elida'
,'Elijah','Elin','Elina','Elinor','Eliot','Elisa','Elisabeth','Elise','Elisha','Elissa'
,'Eliza','Elizabeth','Ella','Elle','Ellen','Ellery','Elliana','Ellie','Ellington','Elliot'
,'Elliott','Ellis','Ellis','Elly','Elmer','Elmo','Elodie','Elody','Eloise','Elon'
,'Elora','Elouise','Elroy','Elsa','Elsie','Elspeth','Elton','Elva','Elvina','Elvira'
,'Elvis','Elwood','Elwyn','Ely','Elysia','Elyssa','Elyza','Emanuel','Emanuela','Ember'
,'Emelda','Emelia','Emeline','Emely','Emer','Emerald','Emerson','Emerson','Emery','Emery'
,'Emet','Emi','Emil','Emilee','Emilia','Emiliano','Emilie','Emilio','Emily','Emlyn'
,'Emma','Emmalee','Emmaline','Emmalyn','Emmanuel','Emmanuelle','Emmeline','Emmerson','Emmet','Emmett'
,'Emmie','Emmy','Emory','Emory','Ena','Ender','Enid','Enna','Ennio','Enoch'
,'Enrique','Enya','Enzo','Eoghan','Eoin','Ephraim','Eric','Erica','Erick','Erik'
,'Erika','Erin','Eris','Ernest','Ernestine','Ernesto','Ernie','Errol','Ervin','Erwin'
,'Eryn','Esmay','Esme','Esmeralda','Esparanza','Esperanza','Esteban','Estee','Estelle','Ester'
,'Esther','Estrella','Ethan','Ethel','Ethen','Etienne','Euan','Eudora','Euen','Eugene'
,'Eugenie','Eunice','Euphemia','Eustace','Eva','Evaline','Evan','Evangelina','Evangeline','Evangelos'
,'Eve','Evelin','Evelina','Evelyn','Evelyn','Everett','Everly','Evie','Evita','Evy'
,'Ewan','Eyan','Ezekiel','Ezio','Ezmae','Ezra','Fabian','Fabienne','Fabio','Fabrizia'
,'Faisal','Faith','Fallon','Fanny','Farah','Farley','Farrah','Fatima','Fawn','Fay'
,'Faye','Febian','Felicia','Felicity','Felipe','Felix','Fenton','Ferdinand','Fergal','Fergus'
,'Fern','Fernand','Fernanda','Fernando','Ferris','Ffion','Fia','Fidel','Fifi','Filbert'
,'Finbar','Findlay','Finlay','Finley','Finn','Finnian','Finnigan','Fion','Fiona','Fionn'
,'Fiora','Fletcher','Fleur','Flick','Flinn','Flo','Flor','Flora','Florence','Florian'
,'Floss','Flossie','Flower','Floyd','Flynn','Ford','Forest','Forrest','Foster','Fox'
,'Fran','Frances','Francesca','Francesco','Francine','Francis','Francisco','Francoise','Frank','Frankie'
,'Frankie','Franklin','Franklyn','Fraser','Frazer','Fred','Freda','Freddie','Freddy','Frederica'
,'Frederick','Fredrick','Freya','Frida','Fritz','Fynn','Gabbie','Gabby','Gabe','Gabriel'
,'Gabriela','Gabriella','Gabrielle','Gael','Gaelan','Gage','Gaia','Gaige','Gail','Gale'
,'Galen','Gannon','Gareth','Garman','Garnet','Garrett','Garrison','Garry','Garth','Gary'
,'Gaston','Gavin','Gayle','Gaynor','Geena','Gemma','Gena','Gene','Genesis','Genevieve'
,'Genna','Geoff','Geoffrey','George','Georgette','Georgia','Georgie','Georgina','Geraint','Gerald'
,'Geraldine','Gerard','Gerardo','Geri','Germain','Germaine','Gerry','Gert','Gertie','Gertrude'
,'Gethin','Gia','Gian','Gianna','Gibson','Gideon','Gigi','Gil','Gilbert','Gilberto'
,'Gilda','Giles','Gill','Gillian','Gina','Ginger','Ginnie','Ginny','Gino','Giorgio'
,'Giovanna','Giovanni','Gisela','Giselle','Gisselle','Gladys','Glen','Glenda','Glenn','Glenys'
,'Gloria','Glyndwr','Glynis','Glynn','Godfrey','Godric','Godwin','Golda','Goldie','Gonzalo'
,'Gordon','Grace','Gracelyn','Gracie','Grady','Graeme','Graham','Grainne','Granger','Grant'
,'Gray','Grayson','Greg','Gregg','Gregor','Gregory','Greta','Gretchen','Grey','Greyson'
,'Griffin','Griselda','Grover','Guadalupe','Guido','Guillermo','Guinevere','Gulliver','Gunnar','Gunner'
,'Gus','Gustav','Gustavo','Guy','Gwain','Gwen','Gwendolyn','Gwyneth','Gwynn','Habiba'
,'Haden','Hadley','Hadley','Haiden','Hailee','Hailey','Hal','Haleigh','Haley','Halle'
,'Halley','Hallie','Hamilton','Hamish','Han','Hank','Hanna','Hannah','Hannibal','Hans'
,'Harlan','Harleigh','Harley','Harley','Harmony','Harold','Harper','Harriet','Harris','Harrison'
,'Harry','Harvey','Hassan','Hattie','Haven','Hayden','Hayden','Hayes','Haylee','Hayley'
,'Hazel','Hazeline','Heath','Heather','Heaven','Hector','Heidi','Helen','Helena','Helene'
,'Helga','Helina','Hendrik','Hendrix','Henley','Henri','Henrietta','Henry','Hepsiba','Hera'
,'Herbert','Herbie','Herman','Hermine','Hermione','Hester','Heston','Hetty','Hezekiah','Hilary'
,'Hilary','Hilda','Hildegard','Hillary','Hiram','Holden','Hollie','Holly','Homer','Honesty'
,'Honey','Honor','Honour','Hope','Horace','Horatio','Hortense','Howard','Hubert','Huck'
,'Hudson','Huey','Hugh','Hugo','Humberto','Humphrey','Hunter','Huw','Huxley','Hyacinth');
total:=multenume.count;
 FOR i in 1 .. total LOOP 
      insert into t_names values(multenume(i));
   END LOOP; 
end;


declare
type namesarray IS VARRAY(500000) OF VARCHAR2(30); 
 multenume namesarray;
 total int;
begin
multenume := namesarray('Hywel','Iain','Ian','Ianthe','Ianto','Ibrahim','Ichabod','Ida','Idris','Iesha'
,'Ieuan','Ieystn','Iggy','Ignacio','Igor','Ike','Ila','Ilene','Iliana','Ilona'
,'Ilse','Imani','Imelda','Immy','Imogen','Imran','Ina','India','Indiana','Indie'
,'Indigo','Indira','Ines','Ingrid','Inigo','Iona','Ira','Ira','Irena','Irene'
,'Irina','Iris','Irma','Irvin','Irving','Irwin','Isa','Isaac','Isabel','Isabell'
,'Isabella','Isabelle','Isadora','Isaiah','Isaias','Isha','Ishaan','Ishmael','Isiah','Isidora'
,'Isidore','Isis','Isla','Ismael','Isobel','Isolde','Israel','Itzel','Ivan','Ivana'
,'Ivor','Ivy','Iwan','Iyanna','Izabella','Izidora','Izzie','Izzy','Jace','Jacinda'
,'Jacinta','Jack','Jackie','Jackie','Jackson','Jacob','Jacoby','Jacqueline','Jacquelyn','Jacques'
,'Jacqui','Jad','Jada','Jade','Jaden','Jaden','Jadon','Jadyn','Jaelynn','Jagger'
,'Jago','Jai','Jaida','Jaiden','Jaiden','Jaime','Jaime','Jaimie','Jaina','Jak'
,'Jake','Jakob','Jalen','Jamal','James','Jameson','Jamie','Jamie','Jamison','Jamiya'
,'Jan','Jan','Jana','Janae','Jancis','Jane','Janella','Janelle','Janessa','Janet'
,'Janette','Jania','Janice','Janie','Janine','Janis','Janiya','January','Jaqueline','Jared'
,'Jaret','Jariel','Jarod','Jaron','Jarrett','Jarrod','Jarvis','Jase','Jasmin','Jasmine'
,'Jason','Jasper','Javid','Javier','Javon','Jax','Jaxon','Jaxson','Jay','Jaya'
,'Jayce','Jayda','Jayden','Jayden','Jaydon','Jayla','Jayleen','Jaylen','Jaylene','Jaylin'
,'Jaylinn','Jaylon','Jaylynn','Jayne','Jayson','Jazlyn','Jazmin','Jazmine','Jazz','Jean'
,'Jeana','Jeanette','Jeanine','Jeanna','Jeanne','Jeannette','Jeannie','Jeannine','Jeb','Jebediah'
,'Jed','Jedediah','Jediah','Jedidiah','Jeevan','Jeff','Jefferson','Jeffery','Jeffrey','Jeffry'
,'Jem','Jemima','Jemma','Jen','Jena','Jenelle','Jenessa','Jenna','Jennette','Jenni'
,'Jennie','Jennifer','Jenny','Jensen','Jensen','Jenson','Jerald','Jeraldine','Jeremiah','Jeremy'
,'Jeri','Jericho','Jermaine','Jerome','Jerri','Jerrion','Jerrold','Jerry','Jersey','Jeslyn'
,'Jess','Jess','Jessa','Jesse','Jesse','Jessica','Jessie','Jessie','Jesus','Jet'
,'Jet','Jethro','Jett','Jevan','Jevin','Jewel','Jeydon','Jill','Jillian','Jim'
,'Jimmie','Jimmy','Jina','Jo','Joachim','Joan','Joann','Joanna','Joanne','Joaquin'
,'Job','Joby','Jocelyn','Jock','Jodi','Jodie','Jody','Jody','Joe','Joel'
,'Joelle','Joey','Joffrey','Johan','Johann','Johanna','John','Johnathan','Johnathon','Johnnie'
,'Johnny','Jojo','Joleen','Jolene','Jolie','Jon','Jonah','Jonas','Jonathan','Jonathon'
,'Joni','Jonquil','Jonty','Jordan','Jordan','Jordana','Jordon','Jordy','Jordyn','Jorge'
,'Jorja','Jose','Joseline','Joselyn','Joseph','Josephina','Josephine','Josh','Joshua','Josiah'
,'Josie','Joss','Josue','Journey','Jovan','Joy','Joya','Joyce','Juan','Juanita'
,'Judah','Judas','Judd','Jude','Jude','Judith','Judy','Jules','Julia','Julian'
,'Juliana','Julianna','Julianne','Julie','Julienne','Juliet','Juliette','Julio','Julissa','Julius'
,'July','Juna','June','Juniper','Juno','Justice','Justice','Justin','Justina','Justine'
,'Kacey','Kacy','Kade','Kaden','Kadence','Kady','Kaelyn','Kai','Kaiden','Kaidence'
,'Kailey','Kailin','Kailyn','Kaine','Kaitlin','Kaitlyn','Kaitlynn','Kale','Kalea','Kaleb'
,'Kaleigh','Kalem','Kali','Kalia','Kalin','Kalista','Kaliyah','Kallie','Kamala','Kameron'
,'Kami','Kamil','Kamryn','Kane','Kaori','Kara','Karen','Kari','Karin','Karina'
,'Karis','Karissa','Karl','Karla','Karlee','Karly','Karolina','Karson','Karsten','Karter'
,'Karyn','Kasey','Kash','Kason','Kasper','Kassandra','Kassidy','Kassie','Kat','Katara'
,'Katarina','Kate','Katelyn','Katelynn','Katerina','Katharine','Katherine','Kathleen','Kathryn','Kathy'
,'Katia','Katie','Katlyn','Katniss','Katrin','Katrina','Katy','Katya','Kay','Kay'
,'Kaya','Kayden','Kayden','Kaydence','Kaye','Kayla','Kayle','Kaylee','Kayleigh','Kaylen'
,'Kayley','Kaylie','Kaylin','Kayne','Kayson','Kean','Keana','Keanu','Keara','Keaton'
,'Kedrick','Keegan','Keelan','Keeley','Keelin','Keely','Keenan','Keira','Keisha','Keith'
,'Kelby','Kelis','Kellan','Kellen','Kelley','Kelli','Kellie','Kellin','Kelly','Kelly'
,'Kelsey','Kelsie','Kelvin','Ken','Kenan','Kendall','Kendall','Kendra','Kendrick','Kenley'
,'Kenna','Kennedy','Kennedy','Kenneth','Kenny','Kensey','Kent','Kenton','Kenzie','Keon'
,'Kera','Keri','Kerian','Kerri','Kerry','Kerry','Kesha','Kevin','Keyon','Khalid'
,'Khalil','Khloe','Kia','Kian','Kiana','Kiara','Kiefer','Kiera','Kieran','Kieron'
,'Kierra','Kiersten','Kiki','Kiley','Killian','Kim','Kim','Kimberlee','Kimberley','Kimberly'
,'Kimbriella','Kimmy','Kingsley','Kingston','Kinley','Kinsey','Kinsley','Kip','Kira','Kiran'
,'Kirby','Kirk','Kirsten','Kirstin','Kirsty','Kit','Kit','Kitty','Kizzy','Klarissa'
,'Klaus','Klay','Kloe','Knox','Kobe','Koby','Kody','Kolby','Konnor','Konrad'
,'Kora','Kori','Kourtney','Kris','Kris','Krish','Krista','Kristen','Kristi','Kristian'
,'Kristie','Kristin','Kristina','Kristine','Kristoff','Kristopher','Kristy','Krystal','Kurt','Kurtis'
,'Kya','Kyan','Kyden','Kye','Kyla','Kylar','Kyle','Kylee','Kyleigh','Kylen'
,'Kyler','Kylie','Kyra','Kyran','Kyrin','Kyron','Kyson','Lacey','Lacey','Lachlan'
,'Lacie','Lacy','Ladonna','Laila','Lainey','Lake','Lakyn','Lala','Lamar','Lambert'
,'Lamont','Lana','Lance','Lancelot','Landen','Lando','Landon','Landyn','Lane','Laney'
,'Langdon','Langston','Lani','Lara','Larissa','Lark','Larry','Lars','Latisha','Latoya'
,'Laura','Laurel','Lauren','Laurence','Lauretta','Laurie','Laurie','Lauryn','Lavana','Lavender'
,'Lavinia','Lawrence','Lawson','Layla','Layna','Layne','Layton','Lea','Leaf','Leah'
,'Leander','Leandra','Leandro','Leann','Leanna','Leanne','Lebron','Leda','Ledger','Lee'
,'Lee','Leela','Leena','Leia','Leif','Leigh','Leigh','Leigha','Leighton','Leila'
,'Leilani','Lela','Leland','Lemuel','Len','Lena','Lenard','Lennie','Lennon','Lennox'
,'Lenny','Lenora','Lenore','Leo','Leon','Leona','Leonard','Leonardo','Leonel','Leonie'
,'Leonora','Leopold','Leora','Leroy','Les','Lesley','Leslie','Leslie','Lesly','Lester'
,'Leticia','Letitia','Lettie','Leuan','Lev','Leven','Levi','Levy','Lewis','Lex'
,'Lexi','Lexia','Lexie','Lexis','Leyla','Leyton','Lia','Liah','Liam','Liana'
,'Lianne','Liara','Libbie','Libby','Liberty','Lidia','Lief','Liesl','Lila','Lilac'
,'Lilah','Lili','Lilian','Liliana','Lilita','Lilith','Lillia','Lillian','Lillie','Lilly'
,'Lily','Lina','Lincoln','Linda','Linden','Lindon','Lindsay','Lindsay','Lindsey','Lindy'
,'Link','Linley','Linus','Lionel','Lisa','Lisandro','Lisbeth','Lisette','Liv','Livia'
,'Livvy','Liz','Liza','Lizbeth','Lizette','Lizzie','Lizzy','Llewelyn','Lloyd','Lochlan'
,'Logan','Logan','Lois','Loki','Lola','Lolita','London','London','Lonnie','Lora'
,'Loran','Lorcan','Lorelei','Loren','Loren','Lorena','Lorenzo','Loretta','Lori','Lorie'
,'Loris','Lorna','Lorraine','Lorri','Lorrie','Lottie','Lotus','Lou','Lou','Louella'
,'Louie','Louis','Louisa','Louise','Lourdes','Lowell','Luann','Luca','Lucas','Lucia'
,'Lucian','Luciana','Luciano','Lucie','Lucien','Lucille','Lucinda','Lucky','Lucky','Lucretia'
,'Lucy','Luigi','Luis','Luisa','Lukas','Luke','Lula','Lulu','Luna','Lupita'
,'Luther','Luz','Lydia','Lyla','Lyle','Lynda','Lyndon','Lyndsey','Lynette','Lynn'
,'Lynn','Lynne','Lynnette','Lynsey','Lyra','Lyric','Lysander','Mabel','Macey','Macie'
,'Mack','Mackenzie','Macy','Madalyn','Maddie','Maddison','Maddox','Maddy','Madeleine','Madeline'
,'Madelyn','Madge','Madison','Madisyn','Madonna','Madyson','Mae','Maeve','Magda','Magdalena'
,'Magdalene','Maggie','Magnus','Maia','Maine','Maira','Maire','Mairead','Maisie','Maison'
,'Maisy','Maja','Makayla','Makenna','Makenzie','Malachi','Malakai','Malala','Malcolm','Maleah'
,'Malena','Mali','Malia','Malik','Malina','Malinda','Mallory','Malloy','Malory','Malvin'
,'Mandy','Manny','Manuel','Manuela','Mara','Marc','Marcel','Marcela','Marcella','Marcelle'
,'Marcelo','Marci','Marcia','Marcie','Marco','Marcos','Marcus','Marcy','Margaret','Margarita'
,'Margaux','Marge','Margery','Margie','Margo','Margot','Margret','Maria','Mariah','Mariam'
,'Marian','Mariana','Marianna','Marianne','Maribel','Marie','Mariela','Mariella','Marigold','Marik'
,'Marilyn','Marina','Mario','Marion','Marion','Maris','Marisa','Marisol','Marissa','Maritza'
,'Marjorie','Mark','Marla','Marlee','Marlena','Marlene','Marley','Marley','Marlon','Marnie'
,'Marquis','Marsha','Marshall','Martha','Martin','Martina','Marty','Martyn','Marvin','Mary'
,'Maryam','Maryann','Marybeth','Maryjane','Masie','Mason','Massimo','Mat','Mateo','Mathew'
,'Mathilda','Mathilde','Matilda','Matt','Matthew','Matthias','Mattie','Maude','Maura','Maureen'
,'Maurice','Mauricio','Maverick','Mavis','Max','Maxim','Maxime','Maximilian','Maximus','Maxine'
,'Maxwell','May','Maya','Maybell','Mayson','Mazie','Mckayla','Mckenna','Mckenzie','Mckenzie'
,'Mea','Meadow','Meagan','Meera','Meg','Megan','Meghan','Mehdi','Mehtab','Mei'
,'Mekhi','Mel','Mel','Melania','Melanie','Melina','Melinda','Melissa','Melody','Melvin'
,'Melvina','Memphis','Mercedes','Mercy','Meredith','Merick','Merida','Merissa','Mervin','Mervyn'
,'Meryl','Mhairi','Mia','Mica','Micah','Michael','Michaela','Micheal','Michele','Michelle'
,'Mick','Mickey','Miguel','Mika','Mikaela','Mikayla','Mike','Mikey','Mikhaela','Mila'
,'Milan','Mildred','Milena','Miles','Miley','Millard','Miller','Millicent','Millie','Milly'
,'Milo','Milton','Mim','Mimi','Mina','Mindy','Minerva','Minnie','Mira','Mirabel'
,'Mirabella','Mirabelle','Miracle','Miranda','Miriam','Mirielle','Misha','Missie','Misty','Mitch'
,'Mitchell','Mitt','Mitzi','Modesty','Moe','Mohamed','Mohammad','Mohammed','Moira','Moises'
,'Mollie','Molly','Mona','Monica','Monika','Monique','Monroe','Monroe','Montana','Monte'
,'Montgomery','Montserrat','Monty','Morag','Mordecai','Morgan','Morgan','Morgana','Morris','Moses'
,'Moxie','Moya','Muhammad','Muriel','Murphy','Murray','Mya','Mycroft','Myfanwy','Myla'
,'Mylene','Myles','Mylo','Myra','Myrna','Myron','Myrtle','Nadene','Nadia','Nadine'
,'Naja','Nala','Nana','Nancy','Nanette','Naomi','Narelle','Nash','Nasir','Natalia'
,'Natalie','Natasha','Nate','Nath','Nathan','Nathanael','Nathaniel','Naya','Nayeli','Neal'
,'Ned','Neel','Nehemiah','Neil','Nell','Nellie','Nelly','Nelson','Nemo','Nena'
,'Nerissa','Nerys','Nesbit','Nessa','Nestor','Netty','Nevaeh','Neve','Neveah','Neville'
,'Nevin','Newt','Newton','Nia','Niall','Niamh','Nichelle','Nichola','Nicholas','Nichole'
,'Nick','Nicki','Nickolas','Nicky','Nicky','Nico','Nicola','Nicolas','Nicole','Nicolette'
,'Nieve','Nigel','Nigella','Nihal','Nik','Nika','Niki','Nikita','Nikki','Niklaus'
,'Niko','Nikolai','Nikolas','Nila','Nile','Nils','Nina','Nisha','Nishka','Nita'
,'Noah','Noam','Noe','Noel','Noella','Noelle','Noely','Noemi','Nola','Nolan'
,'Nora','Norah','Norbert','Noreen','Norma','Norman','Norris','Norton','Nova','Nyla'
,'Nyle','Nyles','Nyx','Oakes','Oakley','Oasis','Obed','Oberon','Ocean','Oceana'
,'Octavia','Octavio','Odalis','Odalys','Odele','Odelia','Odette','Ofelia','Oisin','Olaf'
,'Olga','Oli','Olin','Olive','Oliver','Olivia','Ollie','Olly','Olwen','Olwyn'
,'Olympia','Omar','Ondina','Onna','Oona','Oonagh','Opal','Ophelia','Oprah','Ora'
,'Oran','Oriana','Orianna','Orion','Orla','Orlaith','Orlando','Orson','Oscar','Osric'
,'Osvaldo','Oswald','Otis','Otto','Owain','Owen','Ozzie','Ozzy','Pablo','Paco'
,'Paddy','Padraig','Page','Paige','Paisley','Palmer','Paloma','Pam','Pamela','Pandora'
,'Pansy','Paola','Paolo','Paris','Parker','Pascal','Pat','Patience','Patrice','Patricia'
,'Patrick','Patsy','Patti','Patty','Paul','Paula','Paulette','Paulina','Pauline','Paxton'
,'Payton','Payton','Peace','Pearce','Pearl','Pedro','Peggy','Penelope','Penny','Peony'
,'Pepper','Percy','Peregrine','Perla','Perrie','Perry','Persephone','Petar','Pete','Peter'
,'Petra','Petunia','Peyton','Peyton','Phebian','Phil','Philip','Philippa','Philippe','Phillip'
,'Phillipa','Philomena','Phineas','Phoebe','Phoenix','Phoenix','Phyllis','Pierce','Piers','Pierson'
,'Pip','Piper','Pippa','Pixie','Polly','Pollyanna','Poppy','Porter','Portia','Poul'
,'Prakash','Precious','Presley','Preslie','Preston','Primrose','Prince','Princess','Princeton','Priscilla'
,'Priya','Promise','Prudence','Prue','Queenie','Quentin','Quiana','Quincy','Quinlan','Quinn'
,'Quinn','Quinton','Quintrell','Rabia','Rachael','Rachel','Rachelle','Racquel','Rae','Raegan'
,'Raelyn','Rafael','Rafferty','Raheem','Rahul','Raiden','Raina','Raine','Raj','Rajesh'
,'Ralph','Ram','Rameel','Ramon','Ramona','Ramsey','Ramsha','Randal','Randall','Randi'
,'Randolph','Randy','Rani','Rania','Raoul','Raphael','Raquel','Rashad','Rashan','Rashid'
,'Raul','Raven','Raven','Ravi','Ray','Raya','Raydon','Raylan','Raymond','Rayna'
,'Rayne','Reagan','Reanna','Reanne','Rebecca','Rebekah','Red','Reece','Reed','Reef'
,'Reese','Reese','Reg','Regan','Reggie','Regina','Reginald','Rehan','Reid','Reilly'
,'Reilly','Reina','Remco','Remi','Remington','Remy','Ren','Rena','Renae','Renata'
,'Rene','Rene','Renee','Renesmee','Reuben','Rex','Reyna','Reynaldo','Reza','Rhea'
,'Rhett','Rhian','Rhianna','Rhiannon','Rhoda','Rhodri','Rhona','Rhonda','Rhydian','Rhys'
,'Ria','Rian','Rianna','Ricardo','Rich','Richard','Richelle','Richie','Richmond','Rick'
,'Rickey','Ricki','Rickie','Ricky','Rico','Rider','Ridley','Rigby','Rihanna','Rik'
,'Riker','Rikki','Riley','Riley','Rina','Rio','Riordan','Rita','Riven','River'
,'River','Riya','Roan','Roanne','Rob','Robbie','Robby','Robert','Roberta','Roberto'
,'Robin','Robin','Robson','Robyn','Rocco','Rochelle','Rocio','Rock','Rocky','Rod'
,'Roddy','Roderick','Rodger','Rodney','Rodolfo','Rodrigo','Rogelio','Roger','Rohan','Roisin'
,'Roland','Rolanda','Rolando','Rolf','Roman','Romeo','Ron','Ronald','Ronan','Ronda'
,'Roni','Ronna','Ronnie','Ronny','Roosevelt','Rory','Rosa','Rosalie','Rosalina','Rosalind'
,'Rosalinda','Rosaline','Rosalynn','Rosamund','Rosanna','Roscoe','Rose','Roseanne','Rosella','Roselle'
,'Rosemarie','Rosemary','Rosetta','Rosie','Rosita','Roslyn','Ross','Rosy','Rowan','Rowan'
,'Rowena','Roxana','Roxanne','Roxie','Roxy','Roy','Royce','Rozlynn','Ruairi','Ruben'
,'Rubin','Ruby','Rudolph','Rudy','Rue','Rufus','Rupert','Russ','Russell','Rusty'
,'Ruth','Rutherford','Ruthie','Ryan','Ryanne','Rydel','Ryden','Ryder','Ryker','Rylan'
,'Ryland','Ryle','Rylee','Ryleigh','Ryley','Rylie','Sabina','Sabine','Sable','Sabrina'
,'Sacha','Sade','Sadhbh','Sadie','Saffron','Safire','Safiya','Sage','Sage','Sahara'
,'Said','Saige','Saira','Sally','Salma','Salman','Salome','Salvador','Salvatore','Sam'
,'Sam','Samantha','Samara','Samia','Samir','Samira','Sammie','Sammy','Sammy','Samson'
,'Samuel','Sandeep','Sandra','Sandy','Sandy','Sania','Sanjay','Santiago','Saoirse','Saphira'
,'Sapphire','Sara','Sarah','Sarina','Sariya','Sascha','Sasha','Sasha','Saskia','Saul'
,'Savanna','Savannah','Sawyer','Scarlet','Scarlett','Scot','Scott','Scottie','Scotty','Seamus'
,'Sean','Seanna','Seb','Sebastian','Sebastianne','Sebastien','Sebestian','Selah','Selena','Selene'
,'Selicia','Selina','Selma','Senuri','September','Serafina','Seraphina','Seren','Serena','Serenity'
,'Sergio','Seth','Seymour','Shadrach','Shaelyn','Shakira','Shamira','Shana','Shanaya','Shane'
,'Shani','Shania','Shanice','Shannon','Shannon','Shantell','Shari','Sharon','Shary','Shaun'
,'Shauna','Shawn','Shawn','Shawna','Shawnette','Shay','Shayla','Shayna','Shayne','Shea'
,'Shea','Sheba','Sheena','Sheila','Shelby','Sheldon','Shelia','Shelley','Shelly','Shelton'
,'Sheri','Sheridan','Sherlock','Sherman','Sherri','Sherrie','Sherry','Sherwin','Sheryl','Shiloh'
,'Shirley','Shivani','Shona','Shonagh','Shreya','Shyann','Shyla','Sian','Sid','Sidney'
,'Sidney','Sienna','Sierra','Sigourney','Silas','Silvia','Simeon','Simon','Simone','Simran'
,'Sindy','Sinead','Siobhan','Sissy','Sky','Sky','Skye','Skylar','Skylar','Skyler'
,'Skyler','Skylyn','Slade','Sloane','Snow','Sofia','Sofie','Sol','Solomon','Sondra'
,'Sonia','Sonja','Sonny','Sonya','Sophia','Sophie','Sophy','Soren','Sorrel','Spencer'
,'Spike','Spring','Stacey','Stacey','Staci','Stacia','Stacie','Stacy','Stacy','Stan'
,'Stanford','Stanley','Star','Starla','Stefan','Stefania','Stefanie','Stella','Steph','Stephan'
,'Stephanie','Stephen','Sterling','Steve','Steven','Stevie','Stevie','Stewart','Stone','Storm'
,'Storm','Struan','Stuart','Sue','Sufyan','Sugar','Suki','Sullivan','Summer','Susan'
,'Susanna','Susannah','Susanne','Susie','Sutton','Suzanna','Suzanne','Suzette','Suzie','Suzy'
,'Sven','Sybil','Sydney','Sylvester','Sylvia','Sylvie','Syrus','Tabatha','Tabitha','Tadhg'
,'Taelyn','Tagan','Tahlia','Tai','Tailynn','Tala','Talia','Talitha','Taliyah','Tallulah'
,'Talon','Tam','Tamara','Tamera','Tami','Tamia','Tamika','Tammi','Tammie','Tammy'
,'Tamra','Tamsin','Tane','Tania','Tanika','Tanisha','Tanner','Tanya','Tara','Tariq'
,'Tarquin','Taryn','Tasha','Tasmin','Tate','Tatiana','Tatum','Tavis','Tawana','Tay'
,'Taya','Tayah','Tayden','Taye','Tayla','Taylah','Tayler','Taylor','Taylor','Teagan'
,'Teague','Ted','Teddy','Teegan','Tegan','Teigan','Temperance','Tenille','Teo','Terence'
,'Teresa','Teri','Terrance','Terrell','Terrence','Terri','Terrie','Terry','Terry','Tess'
,'Tessa','Tevin','Tex','Thad','Thaddeus','Thalia','Thea','Theia','Thelma','Theo'
,'Theodora','Theodore','Theon','Theophilus','Theresa','Therese','Thom','Thomas','Thomasina','Thor'
,'Tia','Tiago','Tiana','Tiara','Tiberius','Tiegan','Tiernan','Tiffany','Tiger','Tillie'
,'Tilly','Tim','Timmy','Timothy','Tina','Tinsley','Tisha','Titania','Tito','Titus'
,'Tobias','Tobin','Toby','Tod','Todd','Tom','Tomas','Tommie','Tommy','Toni'
,'Tonia','Tony','Tonya','Tora','Tori','Torin','Toryn','Trace','Tracey','Tracey'
,'Traci','Tracie','Tracy','Tracy','Travis','Tray','Tremaine','Trent','Trenton','Trevon'
,'Trevor','Trey','Treyden','Tricia','Trina','Trinity','Trip','Trish','Trisha','Trista'
,'Tristan','Tristen','Triston','Trixie','Trixy','Troy','Trudy','Truman','Tucker','Tula'
,'Tulip','Turner','Ty','Tylan','Tyler','Tyra','Tyrell','Tyren','Tyrese','Tyron'
,'Tyrone','Tyson','Ulrica','Ulrich','Ulysses','Uma','Umar','Una','Unice','Uriah'
,'Uriel','Ursula','Usama','Val','Val','Valentin','Valentina','Valentine','Valentino','Valeria'
,'Valerie','Valery','Van','Vance','Vanessa','Vasco','Vaughn','Veda','Velma','Venetia'
,'Venus','Vera','Verena','Verity','Vernon','Veronica','Vesper','Vic','Vicki','Vickie'
,'Vicky','Victor','Victoria','Vidal','Vienna','Vihan','Vijay','Vikram','Vince','Vincent'
,'Vinnie','Viola','Violet','Violetta','Virgil','Virginia','Virginie','Vishal','Vivi','Vivian'
,'Vivian','Viviana','Vivien','Vivienne','Vlad','Vladimir','Vonda','Wade','Walker','Wallace'
,'Wallis','Wally','Walt','Walter','Wanda','Ward','Warren','Waverley','Waylon','Wayne'
,'Weldon','Wendell','Wendi','Wendy','Wes','Wesley','Westin','Weston','Whitney','Wilbert'
,'Wilbur','Wiley','Wilfred','Wilhelm','Wilhelmina','Will','Willa','Willam','Willamina','Willard'
,'Willem','William','Willie','Willis','Willow','Wilma','Wilmer','Wilson','Windsor','Winifred'
,'Winnie','Winnifred','Winona','Winston','Winter','Wolf','Wolfgang','Woodrow','Woody','Wyatt'
,'Wynne','Wynona','Xander','Xandra','Xandria','Xanthe','Xavia','Xavier','Xaviera','Xena'
,'Xenia','Xerxes','Xia','Ximena','Xochil','Xochitl','Yahir','Yana','Yanna','Yara'
,'Yardley','Yasmin','Yasmina','Yasmine','Yazmin','Yehudi','Yelena','Yesenia','Yessica','Yestin'
,'Yolanda','York','Yoselin','Youssef','Ysabel','Yula','Yulissa','Yuri','Yusuf','Yvaine'
,'Yves','Yvette','Yvonne','Zac','Zach','Zachariah','Zachary','Zachery','Zack','Zackary'
,'Zackery','Zada','Zadie','Zaheera','Zahra','Zaiden','Zain','Zaine','Zaira','Zak'
,'Zakia','Zali','Zander','Zandra','Zane','Zara','Zarah','Zaria','Zaya','Zayden'
,'Zayla','Zaylen','Zayn','Zayne','Zeb','Zebedee','Zebulon','Zed','Zeke','Zelda'
,'Zelida','Zelina','Zelma','Zena','Zendaya','Zeph','Zeth','Zia','Zig','Ziggy'
,'Zina','Zinnia','Zion','Zita','Ziva','Zoe','Zoey','Zohar','Zola','Zoltan'
,'Zora','Zoran','Zoya','Zula','Zuri','Zuriel','Zyana','Zylen');
total:=multenume.count;
 FOR i in 1 .. total LOOP 
      insert into t_prenames values(multenume(i));
   END LOOP; 
end;

 create or replace function RandomString(p_Characters varchar2, p_length number)
    return varchar2
    is
     l_res varchar2(256);
    begin
      select substr(listagg(substr(p_Characters, level, 1)) within group(order by dbms_random.value), 1, p_length)
        into l_res
        from dual
     connect by level <= length(p_Characters);
     return l_res;
   end;
   /
insert into camera values(1,12,32,1,mycoord(2,4,0),null,null,mycoord(2,4,1),null,null,null,null,3,1,3);

--savegame
declare
v_temp_id savegame.id%type;
v_id savegame.id%type;
v_player_id savegame.player_id%type;
v_map_id savegame.map_id%type;
v_camera_id savegame.camera_id%type;
v_player_position savegame.player_position%type;
v_player_hp savegame.player_hp%type;
v_player_coins savegame.player_coins%type;
v_player_experience savegame.player_experience%type;
v_player_level savegame.player_level%type;
v_player_base_power savegame.player_base_power%type;
v_item1_id savegame.item1_id%type;
v_item2_id savegame.item1_id%type;
v_item3_id savegame.item1_id%type;
v_item4_id savegame.item1_id%type;
v_creation_date savegame.creation_date%type;
v_count int:=0;
begin
loop

  loop
 v_temp_id := trunc(dbms_random.value(1,50000));
  select count(id) into v_id from savegame where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id:=v_temp_id;
  
   select id into v_player_id from 
  (
  select id from player order by dbms_random.value
  )where rownum<=1;
  
  select id into v_map_id from 
  (
  select id from map order by dbms_random.value
  )where rownum<=1;
  
  select id into v_camera_id from 
  (
  select id from camera order by dbms_random.value
  )where rownum<=1;
  
 v_player_position:=mycoord(dbms_random.value(1,30),dbms_random.value(1,30),0);
 v_player_hp:=dbms_random.value(1,300);
 v_player_coins:=dbms_random.value(1,200);
 v_player_experience:=dbms_random.value(1,2000);
 v_player_level:=dbms_random.value(1,20);
 v_player_base_power:=dbms_random.value(1,50);
 
  select id into v_item1_id from 
  (
  select id from item order by dbms_random.value
  )where rownum<=1;
  
   select id into v_item2_id from 
  (
  select id from item order by dbms_random.value
  )where rownum<=1;
   select id into v_item3_id from 
  (
  select id from item order by dbms_random.value
  )where rownum<=1;
   select id into v_item4_id from 
  (
  select id from item order by dbms_random.value
  )where rownum<=1;
  
   v_creation_date := TO_DATE ( TRUNC(  DBMS_RANDOM.VALUE(TO_CHAR(DATE '2016-01-01','J')  ,TO_CHAR(DATE '2017-12-31','J')  ) ),'J' );
   
   insert into savegame values(v_id,v_player_id,v_map_id,v_camera_id,v_player_position,v_player_hp,v_player_coins,v_player_experience,v_player_level,
   v_player_base_power,v_item1_id,v_item2_id,v_item3_id,v_item4_id,v_creation_date);
 
  

v_count:=v_count+1;
exit when v_count=5000;
end loop;

end;


  

--skill
declare
v_temp_id skill.id%type;
v_id skill.id%type;
v_savegame_id skill.savegame_id%type;
v_skill_type skill.skill_type%type;
v_skill_point skill.skill_point%type;
v_skill_max_points skill.skill_max_points%type;
v_require_skill_id skill.require_skill_id%type;
v_count int:=0;
begin
loop

 loop
 v_temp_id := trunc(dbms_random.value(1,100));
  select count(id) into v_id from savegame where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id:=v_temp_id;
  
    select id into v_savegame_id from 
  (
  select id from savegame order by dbms_random.value
  )where rownum<=1;
  
select name into v_skill_type from 
  (
  select name from t_skill_types order by dbms_random.value
  )where rownum<=1;

v_skill_point:=dbms_random.value(1,5);
v_skill_max_points:=dbms_random.value(5,7);
insert into skill values(v_id,v_savegame_id,v_skill_type,v_skill_point,v_skill_max_points,null);
  

v_count:=v_count +1;
exit when v_count=20;
end loop;
end;

--monster_onmap
declare
v_id monster_onmap.monster_id%type;
v_camera_id monster_onmap.camera_id%type;
v_position monster_onmap.position%type;
v_health monster_onmap.health%type;
v_alive monster_onmap.health%type;
v_count int :=0;

begin

loop
   select id into v_id from 
  (
  select id from monster_type order by dbms_random.value
  )where rownum<=1;
     select id into v_camera_id from 
  (
  select id from camera order by dbms_random.value
  )where rownum<=1;
  
  v_position:=mycoord(dbms_random.value(1,30),dbms_random.value(1,30),0);
  
  v_health:=dbms_random.value(1,500);
  v_alive:=dbms_random.value(0,1);
   DBMS_OUTPUT.PUT_LINE( v_id||'  '||v_camera_id||'  '||v_alive||'  '||v_health );
  insert into monster_onmap values(v_id,v_camera_id,v_position,v_health,v_alive);

v_count:=v_count+1;
exit when v_count=100;
end loop;
end;

--monster_type
declare 
v_temp_id monster_type.id%type;
v_id monster_type.id%type;
v_max_hp monster_type.max_hp%type;
v_base_power monster_type.base_power%type;
v_item_id monster_type.item_id%type;
v_count int:=0;
begin
loop 

loop
  v_temp_id := trunc(dbms_random.value(1,20));
  select count(id) into v_id from monster_type where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id:=v_temp_id;
  v_max_hp:=dbms_random.value(1,5000);
  v_base_power :=dbms_random.value(1,100);
  select id into v_item_id from
  (
  select id from item order by dbms_random.value
  )where rownum<=1;
  
  DBMS_OUTPUT.PUT_LINE( v_id||'  '||v_max_hp||'  '||v_base_power||'  '||v_item_id );
  insert into monster_type values ( v_id,v_max_hp,v_base_power,v_item_id );
 

v_count:=v_count+1;
exit when v_count=10;
end loop;

end;

--item populate
declare
v_temp_id item.id%type;
v_id item.id%type;
v_power item.power%type;
v_accuracy item.power%type;
v_nr_shots item.nr_shots%type;
v_price item.price%type;
v_name item.name%type;
v_require_skill_id item.require_skill_id%type;
v_count int:=0;

begin
loop

loop
  v_temp_id := trunc(dbms_random.value(1,100));
  select count(id) into v_id from item where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id:=v_temp_id;
  
  v_power := dbms_random.value(1,500);
  v_accuracy:=dbms_random.value(1,200);
  v_nr_shots:=dbms_random.value(1,8);
  v_price :=dbms_random.value(10,5000);
  select name into v_name from
  (
  select name from t_names order by dbms_random.value
  )where rownum<=1;
  select id into v_require_skill_id from
  (
  select id from skill order by dbms_random.value
  )where rownum<=1;
  
 -- DBMS_OUTPUT.Put_line(v_id || '  '||v_power ||'   '||v_accuracy||'  '||v_nr_shots||'  '||v_price);
  insert into item values(v_id,v_power,v_accuracy,v_nr_shots,v_price,v_name,null,v_require_skill_id); 
v_count:=v_count+1;
exit when v_count=50;
end loop;
end;
--camera populate
declare
v_temp_id camera.id%type;
v_id camera.id%type;
v_length camera.length%type;
v_width camera.width%type;
v_map_id camera.map_id%type;
v_howmanydoors int;
v_ifdoor int;
v_door_n camera.door_n%type;
v_door_s camera.door_n%type;
v_door_e camera.door_n%type;
v_door_v camera.door_n%type;
v_neighboor_n_id camera.neighboor_n_id%type;
v_neighboor_s_id camera.neighboor_n_id%type;
v_neighboor_e_id camera.neighboor_n_id%type;
v_neighboor_v_id camera.neighboor_n_id%type;
v_monster_number camera.monster_number%type;
v_monster_min_id camera.monster_min_id%type;
v_monster_max_id camera.monster_min_id%type;
v_count int:=0;
v_orientare int;
begin
loop
 loop
  v_temp_id := trunc(dbms_random.value(1,100));
  select count(id) into v_id from camera where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id:=v_temp_id;
  v_length :=dbms_random.value(3,20);
  v_width :=dbms_random.value(3,20);
  select id into v_map_id from 
  (
  select id from map
  order by dbms_random.value
  )where rownum=1;
  
  v_howmanydoors:=dbms_random.value(2,4);
  loop
  
  v_ifdoor:=dbms_random.value(0,1);
  if(v_ifdoor=1) then
  v_door_n := mycoord(dbms_random.value(1,30),dbms_random.value(1,30),dbms_random.value(0,1) );
    select id into v_neighboor_n_id from 
  (
  select id from camera
  order by dbms_random.value
  )where rownum=1;
  v_howmanydoors:=v_howmanydoors -1;
  end if;
    v_ifdoor:=dbms_random.value(0,1);
  if(v_ifdoor=1) then
  v_door_s := mycoord(dbms_random.value(1,30),dbms_random.value(1,30),dbms_random.value(0,1) );
      select id into v_neighboor_s_id from 
  (
  select id from camera
  order by dbms_random.value
  )where rownum=1;
  v_howmanydoors:=v_howmanydoors -1;
  end if;
    v_ifdoor:=dbms_random.value(0,1);
  if(v_ifdoor=1) then
  v_door_e := mycoord(dbms_random.value(1,30),dbms_random.value(1,30),dbms_random.value(0,1) );
      select id into v_neighboor_e_id from 
  (
  select id from camera
  order by dbms_random.value
  )where rownum=1;
  v_howmanydoors:=v_howmanydoors -1;
  end if;
    v_ifdoor:=dbms_random.value(0,1);
  if(v_ifdoor=1) then
  v_door_v := mycoord(dbms_random.value(1,30),dbms_random.value(1,30),dbms_random.value(0,1) );
      select id into v_neighboor_v_id from 
  (
  select id from camera
  order by dbms_random.value
  )where rownum=1;
  v_howmanydoors:=v_howmanydoors -1;
  end if;
  
  exit when v_howmanydoors<1;
  end loop;
  v_monster_number:=dbms_random.value(3,10);
  
  
  v_monster_min_id:=trunc(to_char(dbms_random.value(1,5)));
  v_monster_max_id:=trunc(to_char(dbms_random.value(5,10)));
  
  DBMS_OUTPUT.PUT_LINE( v_id||'  ' || v_length|| '   '|| '   ' || v_width || '   ' || v_map_id ||'   '|| v_neighboor_n_id || '  '|| v_neighboor_s_id|| '   ');
  DBMS_OUTPUT.PUT_LINE( v_neighboor_e_id||'  ' || v_neighboor_v_id|| '   '|| '   ' || v_monster_number || '   ' || v_monster_min_id ||'   '|| v_monster_max_id );
  insert into camera values(v_id,v_length,v_width,v_map_id,v_door_n,v_door_s,v_door_e,v_door_v,v_neighboor_n_id,v_neighboor_s_id,v_neighboor_e_id,v_neighboor_v_id,v_monster_number,v_monster_min_id,v_monster_max_id);
  v_count:=v_count+1;
exit when v_count =30;
end loop;
end;


--map populate
declare
v_temp_id map.id%type;
v_id map.id%type;
v_nivel map.nivel%type;
v_valoare map.valoare%type;
v_count int :=0;
begin
loop

  loop
  v_temp_id := trunc(dbms_random.value(1,20));
  select count(id) into v_id from map where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id:=v_temp_id;
  v_nivel:=dbms_random.value(1,10);
  v_valoare:=dbms_random.value(1,10);
  DBMS_OUTPUT.PUT_Line(v_id||'  '||v_nivel||'   '||v_valoare);
  insert into map values(v_id,v_nivel,v_valoare);

v_count := v_count +1;
exit when v_count=10;
end loop;
end;


--player population
declare
v_count int := 0;
v_id player.id%type;
v_temp_id player.id%type;
v_username player.username%type;
v_password player.password%type;
v_email player.email%type;
v_date player.creation_date%type;
begin
loop

  loop
  v_temp_id := trunc(dbms_random.value(1,50000));
  select count(id) into v_id from player where id=v_temp_id;
  exit when v_id=0;
  end loop;
  v_id :=v_temp_id;
  select name into v_username from 
  (
  select name from t_names order by dbms_random.value
  )where rownum<=1;
   select name into v_password from 
  (
  select name from t_names order by dbms_random.value
  )where rownum<=1;
     select name into v_email from 
  (
  select name from t_names order by dbms_random.value
  )where rownum<=1;
  v_email := v_email || '@' || randomstring('aeioubcdfghjkprstvaeiou',4) || '.com';
  
  v_date := TO_DATE ( TRUNC(  DBMS_RANDOM.VALUE(TO_CHAR(DATE '2016-01-01','J')  ,TO_CHAR(DATE '2017-12-31','J')  ) ),'J' );
  insert into player values( v_id,v_username,v_password,v_email,v_date);
--DBMS_OUTPUT.PUT_LINE(' id nou : ' || v_id || ' '|| v_username || '  '||v_password || '   '||v_email || '  '|| v_date );
v_count := v_count +1;

exit when v_count=4000;
end loop;
end;

