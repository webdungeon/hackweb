<?php
//Oracle DB user name
$username = 'webdungeon';

// Oracle DB user password
$password = 'WebDungeon';

// Oracle DB connection string

$cookie_name='cookie_login';
$connection_string = 'localhost/xe';

//Connect to an Oracle database
$GLOBALS['connection'] = oci_connect(
$username,
$password,
$connection_string
);

/*If (!$connection)
echo ' connection failed<br>';
else
echo 'connection succesfull<br>';*/

function Register($name_p,$password_p,$email_p)
{
    $stid = oci_parse($GLOBALS['connection'], 'BEGIN :id := p_registration.register( :name_p, :password_p, :email_p); END; ');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_bind_by_name($stid, ":id" , $id,10);
    oci_bind_by_name($stid , ":name_p",$name_p);
    oci_bind_by_name($stid , ":password_p",$password_p);
    oci_bind_by_name($stid , ":email_p",$email_p);

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_free_statement($stid);

    $verify = oci_parse($connection, 'Select * from player where id=:id ');
    if (!$verify)
    {
        $e = oci_error($connection);
        trigger_error(htmlentities($e['message'],ENT_QUOTES),E_USER_ERROR);
    }
    oci_bind_by_name($verify,":id",$id);

    $r = oci_execute($verify);
    if(!$r)
    {
        $e=oci_error($verify);
        trigger_error(htmlentities($e['message'],ENT_QUOTES),E_USER_ERROR);
    }

    // Fetch the results of the query
 print $id;
        // Fetch the results of the query
        while (($row = oci_fetch_array($verify, OCI_BOTH + OCI_RETURN_NULLS))!= false) {
                print "Welcome ". $row[1]."<br>";
            print "\n"; 
        }
    return $id;
    oci_free_statement($verify);
}

function Login($name_p,$password_p)
{
    $stid = oci_parse($GLOBALS['connection'], 'BEGIN :id := p_registration.login( :name_p, :password_p); END; ');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_bind_by_name($stid , ":id", $id,10);
    oci_bind_by_name($stid , ":name_p",$name_p);
    oci_bind_by_name($stid , ":password_p",$password_p);

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    
    oci_free_statement($stid);
    if($id == -1)
         echo 'Nu ti cunosc <br>';
    else 
    {
        $verify = oci_parse($GLOBALS['connection'], 'Select * from player where id=:id ');
        if (!$verify)
        {
            $e = oci_error($GLOBALS['connection']);
            trigger_error(htmlentities($e['message'],ENT_QUOTES),E_USER_ERROR);
        }
        oci_bind_by_name($verify, ":id",$id);  

        $r = oci_execute($verify);
        if(!$r)
        {
            $e=oci_error($verify);
            trigger_error(htmlentities($e['message'],ENT_QUOTES),E_USER_ERROR);
        }

        // Fetch the results of the query
       print $id;
        // Fetch the results of the query
        while (($row = oci_fetch_array($verify, OCI_BOTH + OCI_RETURN_NULLS))!= false) {
                print "Welcome ". $row[1]."<br>";
            print "\n"; 
        }

        oci_free_statement($verify);
        return $id;
    }
}
    
function LoginAsCookie($name_p)
{
    $stid = oci_parse($GLOBALS['connection'], 'BEGIN :id := p_Registration.register_cookie( :name_p); END; ');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_bind_by_name($stid , ":id", $id,10);
    oci_bind_by_name($stid , ":name_p",$name_p);

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    
    oci_free_statement($stid);
    if($id == -1)
         echo 'Nu ti cunosc <br>';
    else 
    {
        $verify = oci_parse($GLOBALS['connection'], 'Select * from player where id=:id ');
        if (!$verify)
        {
            $e = oci_error($GLOBALS['connection']);
            trigger_error(htmlentities($e['message'],ENT_QUOTES),E_USER_ERROR);
        }
        oci_bind_by_name($verify, ":id",$id);  

        $r = oci_execute($verify);
        if(!$r)
        {
            $e=oci_error($verify);
            trigger_error(htmlentities($e['message'],ENT_QUOTES),E_USER_ERROR);
        }
        print $id;
        // Fetch the results of the query
        while (($row = oci_fetch_array($verify, OCI_BOTH + OCI_RETURN_NULLS))!= false) {
                print "Welcome ". $row[1]."<br>";
            print "\n"; 
        }
       // print "</table>\n";

        oci_free_statement($verify);
        return $id;
    }
        //cookie set up
        
}
function GetUsername($id)
{
    $stid = oci_parse($GLOBALS['connection'], 'select username from player where id=:id ');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_bind_by_name($stid , ":id", $id,10);

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    $row = oci_fetch_array($stid, OCI_BOTH + OCI_RETURN_NULLS);
    oci_free_statement($stid);
    return $row[0];
}
function ChangeUsername($id,$name)
{
    $stid = oci_parse($GLOBALS['connection'], 'update player set username=:name where id=:id ');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_bind_by_name($stid , ":id", $id,10);
    oci_bind_by_name($stid , ":name", $name,10);

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    $row = oci_fetch_array($stid, OCI_BOTH + OCI_RETURN_NULLS);
    oci_free_statement($stid);
}

function ChangePassword($id,$name)
{
    $stid = oci_parse($GLOBALS['connection'], 'update player set password=:name where id=:id ');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_bind_by_name($stid , ":id", $id,10);
    oci_bind_by_name($stid , ":name", $name,10);

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    $row = oci_fetch_array($stid, OCI_BOTH + OCI_RETURN_NULLS);
    oci_free_statement($stid);
}

function Clean()
{
     $stid = oci_parse($GLOBALS['connection'], 'BEGIN p_registration.cleanup; END; ');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
     oci_free_statement($stid);
}

function RankIt($map,$player)
{
    if($map == 0) $map_p=NULL;
    if($player==0) $player_p=NULL;
    $stid = oci_parse($GLOBALS['connection'], 'select * from table(p_account.rank_table(:map,:player)) order by 4 desc,5 desc');
    if (!$stid) {
        $e = oci_error($GLOBALS['connection']);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    oci_bind_by_name($stid, ":map",$map_p);
    oci_bind_by_name($stid, ":player",$player_p);

    // Perform the logic of the query
    $r = oci_execute($stid);
    if (!$r) {
        $e = oci_error($stid);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    print "    <table style=\"width:100%\">
                    <tr>
                        <th id=\"first\"><a herf=\"#\" id=\"butonRankT\" class=\"tabalButons\"> Rank</a></th>
                        <th><a herf=\"#\" id=\"butonUser\" class=\"tabalButons\"> User Name</a></th> 
                        <th><a herf=\"#\" id=\"butonCoins\" class=\"tabalButons\"> Coins</a></th>
                        <th><a herf=\"#\" id=\"butonLast\" class=\"tabalButons\"> Last Map</a></th>
                        <th><a herf=\"#\" id=\"butonLevel\" class=\"tabalButons\"> Level</a></th>
                        <th><a herf=\"#\" id=\"butonExperience\" class=\"tabalButons\"> Experience</a></th>
                   </tr> ";
    // Fetch the results of the query
   // print "<table border='1'>\n";
    $nr = 0;
    while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
        print "<tr>\n";
        $nr = $nr+1;
        print "<td>".$nr."</td>" ;
        foreach ($row as $item) {   
            print " <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
        }
        print "</tr>\n";
    }
    print "</table>\n";

    oci_free_statement($stid);

}

?>