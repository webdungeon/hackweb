function openNav() {

    document.getElementById("myNav").style.height = "100%";
    document.getElementById("TableOverlay").style.height = "0%";
    $("#register").hide();
    $("#login").hide();
    $("#login_cookie").hide();
    
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
    
}

function openTable(){
    document.getElementById("myNav").style.height = "0%";
    document.getElementById("TableOverlay").style.height = "100%";

}
function Filter() 
{
$.ajax({
    url:'oci_functions.php',
    type: 'post',
    data: {"filter" : "1"},
    success: function(response) {alert(response);}
});
}


$(document).ready(function(){
    $("#buttonRegister").click(function(){
        $("#register").toggle();
    });
    $("#butonLogIn").click(function(){
        $("#login").toggle();
    });
    $("#butonPlay").click(function(){
        $("#login_cookie").toggle();
    });
    $("#butonRank").click(function(){
        openTable();
    });
    $("body").keydown(function(event){ 
        if(27 == event.which)
            if(document.getElementById("myNav").style.height == "100%")
                closeNav();
            else openNav();
    });
    
});