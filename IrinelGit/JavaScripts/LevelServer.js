Game.LevelServer = function(game){  
};

////id
var webId1=11;
var webDialog1= {pos :0 , dialog: null};
var webId2=13;
var webDialog2= {pos :0 , dialog: null};
var webId3=15;
var webDialog3= {pos :0 , dialog: null};
var webId4=17;
var webDialog4= {pos :0 , dialog: null};
var aliasDialog={ pos: 0 , dialog: null};
var definedPortal = false;

var restId1=18;
var restDialog1= {pos :0 , dialog: null};
var restId2=20;
var restDialog2= {pos :0 , dialog: null};


var bdId =12;
var bdDialog={ pos: 0 , dialog: null};

var apiId =14;
var apiDialog={ pos: 0 , dialog: null};

var formId =16;
var formDialog={ pos: 0 , dialog: null};

var uriId =19;
var uriDialog={ pos: 0 , dialog: null};
////id
var moveOn = 0;
var map;
var layer;
var music;
var respawn;
var missionWEB = 0;
var foundBD = 0;
var foundAPI = 0;
var missionREST = 0;
var absolut= 0;
var player2;
var beenOnWeb = 0;
var beenOnRest = 0;

var isPlayer = 0;
var player;
var controls = {};
var playerSpeed = 1200;
var jumpTimer = 0;
var walls;
var pauseKey;
var element = document.getElementById("HPBar");
var misWebServer=0;
var vizrestServer=0;
var element;
var checkDeadBugsRoot = [];
var checkDeadBugsRobots = [];
var checkDeadBugsSuper = [];

var nrenemies = 10;
var nrSuperEnemies = 6;
var nrRootEnemys = 6;
var enemy;
var nextFire = 0;
var fireRate = 250;
var enemys=[];
var superEnemys=[];
var rootEnemys=[];
var changeBody = 0;
var coins = [];
var gravex = 1;
var gravey = 1;
var CurrentMap ;

var checkChange=0;

EnemyBugType = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.bug =  game.add.sprite(x,y,'robots');
    this.bug.scale.setTo(2);
    this.bug.anchor.setTo(0.5,0.5);
    this.bug.name = index.toString();
    
    //game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
    
        //game.physics.arcade.enable(this.cookie3);
    if(index%(nrenemies/2) == 0 ){
        game.physics.enable(this.bug,Phaser.Physics.ARCADE);
        this.bug.animations.add('walkLeft',[12,13,14],12,true);
        this.bug.animations.add('walkRight',[24,25,26],12,true);
        this.bug.animations.add('walkUp',[36,37,38],9,true);
        this.bug.animations.add('walkDown',[0,1,2],10,true);
        this.bug.animations.add('stay',[1],13,true);
        //this.bug.animations.play('walkUp');
        this.bug.body.collideWorldBounds = true;
        this.bug.body.immovable = true;
        this.bug.body.allowGravity = false;
    }
    else if(index%(nrenemies/2) == 1){
        game.physics.enable(this.bug,Phaser.Physics.ARCADE);
        this.bug.animations.add('walkLeft',[18,19,20],12,true);
        this.bug.animations.add('walkRight',[30,31,32],12,true);
        this.bug.animations.add('walkUp',[42,43,44],9,true);
        this.bug.animations.add('walkDown',[6,7,8],10,true);
        this.bug.animations.add('stay',[1],13,true);
        this.bug.animations.play('walkRight');
        this.bug.body.collideWorldBounds = true;
        this.bug.body.immovable = true;
        this.bug.body.allowGravity = false;
        
    }
    else if(index%(nrenemies/2) == 2){
        game.physics.enable(this.bug,Phaser.Physics.ARCADE);
        this.bug.animations.add('walkLeft',[21,22,23],12,true);
        this.bug.animations.add('walkRight',[33,34,35],12,true);
        this.bug.animations.add('walkUp',[45,46,47],9,true);
        this.bug.animations.add('walkDown',[9,10,11],10,true);
        this.bug.animations.add('stay',[1],13,true);
        this.bug.animations.play('walkRight');
        this.bug.body.collideWorldBounds = true;
        this.bug.body.immovable = true;
        this.bug.body.allowGravity = false;    
    }
    else if(index%(nrenemies/2) == 3){
        game.physics.enable(this.bug,Phaser.Physics.ARCADE);
        this.bug.animations.add('walkLeft',[60,61,62],12,true);
        this.bug.animations.add('walkRight',[72,73,74],12,true);
        this.bug.animations.add('walkUp',[84,85,86],9,true);
        this.bug.animations.add('walkDown',[48,49,50],10,true);
        this.bug.animations.add('stay',[1],13,true);
        this.bug.animations.play('walkRight');
        this.bug.body.collideWorldBounds = true;
        this.bug.body.immovable = true;
        this.bug.body.allowGravity = false;
        
    }
    else if(index%(nrenemies/2) == 4){
        game.physics.enable(this.bug,Phaser.Physics.ARCADE);
        this.bug.animations.add('walkLeft',[63,64,65],12,true);
        this.bug.animations.add('walkRight',[75,76,77],12,true);
        this.bug.animations.add('walkUp',[87,88,89],9,true);
        this.bug.animations.add('walkDown',[51,52,53],10,true);
        this.bug.animations.add('stay',[1],13,true);
        this.bug.animations.play('walkRight');
        this.bug.body.collideWorldBounds = true;
        this.bug.body.immovable = true;
        this.bug.body.allowGravity = false;
    }
    
    //this.cookie3.body.immovable = true;
    //this.cookie3.body.collideWorldBounds = true;
    //this.cookie3.body.allowGravity = false;

   /* this.cookie3Tween = game.add.tween(this.cookie3).to({
        y: 50,
    },2000,'Linear',true,0,100,true); */
    
    //game.physics.arcade.moveToXY(this.cookie3, player.x,player.y, 200);
    
    
    //this.cookie3.reset(x,y);
};
EnemySuperBugType = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.bug =  game.add.sprite(x,y,'robot2');
    this.bug.scale.setTo(2);
    this.bug.anchor.setTo(0.5,0.5);
    this.bug.name = index.toString();
    //game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
    game.physics.enable(this.bug,Phaser.Physics.ARCADE);
    this.bug.body.collideWorldBounds = true;
    this.bug.animations.add('walkLeft',[3,4,5],12,true);
    this.bug.animations.add('walkRight',[6],12,true);
    this.bug.animations.add('walkUp',[10,11,12],9,true);
    this.bug.animations.add('walkDown',[0,1,2],10,true);
    this.bug.animations.add('stay',[1],13,true);
    //this.bug.animations.play('walkUp');
    this.bug.body.immovable = true;
    this.bug.body.allowGravity = false;
}
EnemyRootBugType = function(index,game,x,y){
    this.originx=x;
    this.originy=y;
    this.bug =  game.add.sprite(x,y,'robot2');
    this.bug.anchor.setTo(0.5,0.5);
    this.bug.name = index.toString();
    //game.physics.enable(this.cookie3,Phaser.Physics.ARCADE);
    game.physics.enable(this.bug,Phaser.Physics.ARCADE);
    this.bug.body.collideWorldBounds = true;
    this.bug.animations.add('walkLeft',[3,4,5],12,true);
    this.bug.animations.add('walkRight',[6],12,true);
    this.bug.animations.add('walkUp',[10,11,12],9,true);
    this.bug.animations.add('walkDown',[0,1,2],10,true);
    this.bug.animations.add('stay',[1],13,true);
    //this.bug.animations.play('walkUp');
    this.bug.body.immovable = true;
    this.bug.body.allowGravity = false;
}

Phaser.Tilemap.prototype.setCollisionBetween = function (start, stop, collides, layer, recalculate) {

	if (collides === undefined) { collides = true; }
	if (layer === undefined) { layer = this.currentLayer; }
	if (recalculate === undefined) { recalculate = true; }

	layer = this.getLayer(layer);

	for (var index = start; index <= stop; index++)
	{
		if (collides)
		{
			this.collideIndexes.push(index);
		}
		else
		{
			var i = this.collideIndexes.indexOf(index);

			if (i > -1)
			{
				this.collideIndexes.splice(i, 1);
			}
		}
	}

	for (var y = 0; y < this.layers[layer].height; y++)
	{
		for (var x = 0; x < this.layers[layer].width; x++)
		{
			var tile = this.layers[layer].data[y][x];

			if (tile && tile.index >= start && tile.index <= stop)
			{
				if (collides)
				{
					tile.setCollision(true, true, true, true);
				}
				else
				{
					tile.resetCollision();
				}

				tile.faceTop = collides;
				tile.faceBottom = collides;
				tile.faceLeft = collides;
				tile.faceRight = collides;
			}
		}
	}

	if (recalculate)
	{
		//  Now re-calculate interesting faces
		this.calculateFaces(layer);
	}

	return layer;

};

Game.LevelServer.prototype = {
    create: function(){
        //alert(element.value);
        CurrentMap = this;
        this.id =11;
        this.stage.backgroundColor = '#3A5963';
      
        //OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        respawn = this.game.add.group();
        
        this.map = this.game.add.tilemap('ServerMap');  // se incarca mapa     
        this.map.addTilesetImage('Terrain_Irinel'); // imaginile folosita
        this.map.addTilesetImage('terainVH');
        
        this.main = this.map.createLayer('main');
        this.walls = this.map.createLayer('walls');
        
        //this.test = this.map.createLayer('test');
        //this.speech = this.map.createLayer('speech');

        
        //this.map.setCollisionByExclusion([0]);
        this.main.resizeWorld();
        
        // OBJECT LAYER
        
        this.map.createFromObjects('objectLayer',1829 , '' ,0,true,false,respawn);

        
        
        //MUSIC
        //music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        //PLAYER
        
        this.map.setCollisionBetween(1, 100000, true, 'walls');  //!!! // tot pt coliziune
        
        //this.physics.arcade.collide(player,this.walls); 
        // this.walls.setCollisionBetween(1, 100000, true, 'walls',true);
        //
        
        this.spawn();
        
        //PLAYER SPAWNS AT THE SET POSITION
        
        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
        }
        
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.ESC);
        pauseKey.onDown.add(togglePause, this);
        
       portal = this.add.sprite(2000,3500,'portal');
      portal.anchor.setTo(0.5,0.5);
      this.physics.arcade.enable(portal);
        portal.body.collideWorldBounds = true;
        portal.animations.add('',[0,1,2],5,true);
                           
        portal.body.collideWorldBounds = true;
        portal.body.immovable = true;
        portal.alpha = 0;
        //
        
        webServer = this.add.sprite(230,3520,'webServer');
        this.physics.arcade.enable(webServer);
        webServer.body.collideWorldBounds = true;
        webServer.body.immovable = true;
        getDialog(webId1,webDialog1);
        getDialog(webId2,webDialog2);
        getDialog(webId3,webDialog3);
        getDialog(webId4,webDialog4);
        
        restServer = this.add.sprite(2950,3520,'restServer');
        this.physics.arcade.enable(restServer);
        restServer.body.collideWorldBounds = true;
        restServer.body.immovable = true;
        getDialog(restId1,restDialog1);
        getDialog(restId2,restDialog2);
        
        BDsprite = this.add.sprite(400,2300,'Books_Flying1');
        BDsprite.anchor.setTo(0.5,0.5);
        BDsprite.scale.setTo(3);
        this.physics.arcade.enable(BDsprite);
        BDsprite.body.collideWorldBounds = true;
        BDsprite.animations.add('openBook',[3,4,5,6,7,8,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,12,13,14,18,19,20],5,true);
        BDsprite.animations.add('closeBook',[4]);
        BDsprite.body.immovable = true;
        getDialog(bdId,bdDialog);
        
        FormSprite = this.add.sprite(477,850,'button');
        FormSprite.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(FormSprite);
        FormSprite.animations.add('pressButton',[1],5,true);
        FormSprite.animations.add('reliseButton',[0],5,true);
        FormSprite.body.collideWorldBounds = true;
        FormSprite.body.immovable = true;
        getDialog(formId,formDialog);
        
        UriSprite = this.add.sprite(3000,466,'URI');
        UriSprite.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(UriSprite);
        UriSprite.animations.add('openURI',[1],5,true);
        UriSprite.animations.add('closeURI',[2],5,true);
        UriSprite.body.collideWorldBounds = true;
        UriSprite.body.immovable = true;
        getDialog(uriId,uriDialog);
        
        APIsprite = this.add.sprite(1170,2250,'topMapNpc');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(APIsprite);
        //topMapNpc.animations.add('topMapNpc_anim');
        //topMapNpc.animations.play('topMapNpc_anim', 12, true);
        APIsprite.body.collideWorldBounds = true;
        APIsprite.body.immovable = true;
        getDialog(apiId,apiDialog);
        
        WallSprite = this.add.sprite(1756.7,170,'Wallul');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(WallSprite);
        //topMapNpc.animations.add('topMapNpc_anim');
        //topMapNpc.animations.play('topMapNpc_anim', 12, true);
        WallSprite.body.collideWorldBounds = true;
        WallSprite.body.immovable = true;
       /* 
        BUGsprite2 = this.add.sprite(600,500,'robots');
        BUGsprite2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(BUGsprite2);
        BUGsprite2.body.collideWorldBounds = true;
        BUGsprite2.animations.add('walkLeft',[12,13,14],12,true);
        BUGsprite2.animations.add('walkRight',[24,25,26],12,true);
        BUGsprite2.animations.add('walkUp',[36,37,38],9,true);
        BUGsprite2.animations.add('walkDown',[0,1,2],10,true);
        BUGsprite2.animations.add('stay',[1],13,true);
        
        BUGsprite2.body.collideWorldBounds = true;
        BUGsprite2.body.immovable = true;
        */
        
        for(i=0; i<nrenemies/2; i++)
            {
                randomx=(Math.floor(Math.random() * 800)%900);
                randomy=(Math.floor(Math.random() * 800)%900);
                enemy=new EnemyBugType(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                enemys.push(enemy); 
            }
        
        for(i=nrenemies/2; i<nrenemies; i++)
            {
                randomx=(Math.floor(Math.random() * 800)%900 + 2200);
                randomy=(Math.floor(Math.random() * 800)%900 );
                enemy=new EnemyBugType(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                enemys.push(enemy);
            }
        for(i=0; i<nrSuperEnemies/2; i++)
            {
                randomx=(Math.floor(Math.random() * 800)%900);
                randomy=(Math.floor(Math.random() * 800)%900);
                enemy=new EnemySuperBugType(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                superEnemys.push(enemy);
                
            }
        for(i=nrSuperEnemies/2; i<nrSuperEnemies; i++)
            {
                randomx=(Math.floor(Math.random() * 800)%900 + 2200);
                randomy=(Math.floor(Math.random() * 800)%900 );
                enemy=new EnemySuperBugType(i,this.game,randomx,randomy);
                console.log(randomx+"  "+randomy);
                superEnemys.push(enemy);
            }
        for(i = 0; i < nrRootEnemys; i++){
            if(i==0){
                randomx=1708;
                randomy=2370;
             }
            else if(i == 1){
                randomx=2900;
                randomy=2300;
            }
            else if(i == 2){
                randomx=2900;
                randomy=1800;
            }
            else if(i == 3){
                randomx=850;
                randomy=1800;
            }
            else if(i == 4){
                randomx=850;
                randomy=1300;
            }
            else if(i == 5 ){
                randomx=1500;
                randomy=1370;
            }
            
            enemy=new EnemyRootBugType(i,this.game,randomx,randomy);
            console.log(randomx+"  "+randomy);
            rootEnemys.push(enemy);
        }
        
         bullets = this.game.add.group();
         bullets.enableBody = true;
         bullets.physicsBodyType = Phaser.Physics.ARCADE;
         bullets.createMultiple(30,'bullet',0,false);                //check nr of bullets
         //bullets.setAll('anchor.x',0.5);
         //bullets.setAll('anchor.y',0.5);

         bullets.callAll('anchor.setTo', 'anchor', 0.5, 1.0);
         bullets.callAll('events.onOutOfBounds.add','events.onOutOfBounds', resetBullet);
         bullets.setAll('checkWorldBounds', true);
         nrCoins = nrenemies+nrSuperEnemies+nrRootEnemys;
         for(i=0; i<nrCoins; i++)
            {
                cookieAnimation = this.add.sprite(1000,650,'cookieAnimation');
                cookieAnimation.anchor.setTo(0.5,0.5);
                this.physics.arcade.enable(cookieAnimation);
                cookieAnimation.animations.add('cookieAnimation4_anim',[0,1,4,5,8,9,12,13]);
                cookieAnimation.animations.play('cookieAnimation4_anim', 12, true);
                cookieAnimation.body.collideWorldBounds = true;
                cookieAnimation.body.immovable = true;
                cookieAnimation.visible = false;
                coins.push(cookieAnimation);
            }
        for(i=0;i< nrRootEnemys;++i){
            checkDeadBugsRoot[i]= false;
        }
        for(i=0;i< nrSuperEnemies;++i){
            checkDeadBugsSuper[i]= false;
        }
        for(i=0;i< nrenemies;++i){
            checkDeadBugsRobots[i]= false;
        }
    },
    
    update:function(){
        
        //this.physics.arcade.collide(player,this.moreMoreThenTopLayer);   //CU CE LAYER VREICOLIZIUNE 
        this.physics.arcade.collide(player,this.walls); 
        this.physics.arcade.collide(player,webServer);
        this.physics.arcade.collide(player,restServer);
        this.physics.arcade.collide(player,BDsprite);
        this.physics.arcade.collide(player,APIsprite);
        this.physics.arcade.collide(player,FormSprite);
        this.physics.arcade.collide(player,UriSprite);
        if(changeBody == 0){
            this.physics.arcade.collide(player,WallSprite);
        }
        if (changeBody){
            this.physics.arcade.collide(player1,this.walls); 
            this.physics.arcade.collide(player1,webServer);
            this.physics.arcade.collide(player1,restServer);
            this.physics.arcade.collide(player1,BDsprite);
            this.physics.arcade.collide(player1,APIsprite);
            player1.animations.play('idle');
            //changeBody=0;
        }
        //this.physics.arcade.collide(BUGsprite1,this.walls);
        //this.physics.arcade.collide(player,BUGsprite1);
        if(element.value<100)
            element.value+=0.5;
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        //BUGsprite2.animations.play('walkUp');
        if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;
                        //element.value-=0.5;
        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;  
            //element.value+=0.5;
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        if (this.game.input.activePointer.isDown)
        {
           /* if(this.game.input.activePointer.x + 530 > player.x){
                player.animations.play('walkright');
            }
            else if(this.game.input.activePointer.x - 480 < player.x){
                player.animations.play('walkleft');
            }*/

            
            if (this.game.time.now > nextFire && bullets.countDead() > 0)
            {
            nextFire = this.game.time.now + fireRate;
            bullet = bullets.getFirstDead();
            bullet.reset(player.x - 5 , player.y + 45);
            bullet.rotation = this.game.physics.arcade.angleToPointer(player);
           
            this.game.physics.arcade.moveToPointer(bullet, 2500);
                bullet.lifespan = 100;
                //bullet.body.velocity.x = 0;
                //bullet.body.velocity.y = 0;
            }
        }
        
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
        
    
        
        for(i=0; i<nrenemies; i++){
            this.physics.arcade.collide(enemys[i].bug,this.walls);
            this.physics.arcade.collide(enemys[i].bug,FormSprite);
            this.physics.arcade.collide(enemys[i].bug,WallSprite);
        }    
        for(i=0; i<nrenemies; i++)
            {
                 if(checkOverlap(player,enemys[i].bug) && enemys[i].bug.alive){
                   element.value-=2;
                   if(element.value == 0){
                        //this.resetPlayer();
                        element.value=100;
                       player.reset(1800,3670);
                    }
                }
                else if(!checkOverlap(WallSprite,enemys[i].bug)){
                     absolut = Math.abs( (player.x - enemys[i].bug.x));
                     if( absolut <= 50 ){
                         if(player.y >= enemys[i].bug.y)
                            enemys[i].bug.animations.play('walkDown');
                         else
                            enemys[i].bug.animations.play('walkUp'); 
                             
                        }
                     else if(player.x >= enemys[i].bug.x)
                            enemys[i].bug.animations.play('walkRight');
                         else
                            enemys[i].bug.animations.play('walkLeft'); 
                             
                        
                    if(Phaser.Math.distance(player.x, player.y, enemys[i].bug.x, enemys[i].bug.y) <= 700 && player.y<=1250 && player.y >=330
                         && Phaser.Math.distance(enemys[i].originx, enemys[i].originy, player.x, player.y) < 700)  
                              this.game.physics.arcade.moveToXY(enemys[i].bug, player.x,player.y, 300);
                       else 
                           {   
                             absolut = Math.abs( (enemys[i].originx - enemys[i].bug.x));
                             if( absolut <= 50 ){
                                 if(enemys[i].originy >= enemys[i].bug.y)
                                    enemys[i].bug.animations.play('walkDown');
                                 else
                                    enemys[i].bug.animations.play('walkUp'); 

                                }
                             else if(enemys[i].originx >= enemys[i].bug.x)
                                    enemys[i].bug.animations.play('walkRight');
                                 else
                                    enemys[i].bug.animations.play('walkLeft'); 
                             this.game.physics.arcade.moveToXY(enemys[i].bug, enemys[i].originx,enemys[i].originy, 150);
                            //enemys[i].cookie3.body.velocity.x = 0;
                            //enemys[i].cookie3.body.velocity.y = 0;
                         }
                      
                }
                
            }    
        
        for(i=0; i<nrSuperEnemies; i++){
            this.physics.arcade.collide(superEnemys[i].bug,this.walls); 
            this.physics.arcade.collide(superEnemys[i].bug,FormSprite);
            this.physics.arcade.collide(superEnemys[i].bug,WallSprite);
            
        }
        for(i=0; i<nrSuperEnemies; i++)
            {
                 if(checkOverlap(player,superEnemys[i].bug) && superEnemys[i].bug.alive){
                   element.value-=2;
                   if(element.value==0)
                    {
                        
                        //this.resetPlayer();
                        element.value=100;
                        player.reset(1800,3670);
                    }
                }
                else if(!checkOverlap(WallSprite,superEnemys[i].bug)){
                     absolut = Math.abs( (player.x - superEnemys[i].bug.x));
                     if( absolut <= 50 ){
                         if(player.y >= superEnemys[i].bug.y)
                            superEnemys[i].bug.animations.play('walkDown');
                         else
                            superEnemys[i].bug.animations.play('walkUp'); 
                             
                        }
                     else if(player.x >= superEnemys[i].bug.x)
                            superEnemys[i].bug.animations.play('walkRight');
                         else
                            superEnemys[i].bug.animations.play('walkLeft'); 
                             
                        
                    if(Phaser.Math.distance(player.x, player.y, superEnemys[i].bug.x, superEnemys[i].bug.y) <= 700 && player.y<=1250 && player.y >=330
                         && Phaser.Math.distance(superEnemys[i].originx, superEnemys[i].originy, player.x, player.y) < 700 )  
                              this.game.physics.arcade.moveToXY(superEnemys[i].bug, player.x,player.y, 300);
                       else 
                           {   
                             absolut = Math.abs( (superEnemys[i].originx - superEnemys[i].bug.x));
                             if( absolut <= 50 ){
                                 if(superEnemys[i].originy >= superEnemys[i].bug.y)
                                    superEnemys[i].bug.animations.play('walkDown');
                                 else
                                    superEnemys[i].bug.animations.play('walkUp'); 

                                }
                             else if(superEnemys[i].originx >= superEnemys[i].bug.x)
                                    superEnemys[i].bug.animations.play('walkRight');
                                 else
                                    superEnemys[i].bug.animations.play('walkLeft'); 
                             this.game.physics.arcade.moveToXY(superEnemys[i].bug, superEnemys[i].originx,superEnemys[i].originy, 150);
                            //enemys[i].cookie3.body.velocity.x = 0;
                            //enemys[i].cookie3.body.velocity.y = 0;
                         }
                      
                }
                
            }   
        
        for(i=0; i<nrRootEnemys; i++){
            this.physics.arcade.collide(rootEnemys[i].bug,this.walls);
            this.physics.arcade.collide(rootEnemys[i].bug,UriSprite);
            this.physics.arcade.collide(rootEnemys[i].bug,WallSprite);
        }
        for(i=0; i<nrRootEnemys; i++)
            {
                 if(checkOverlap(player,rootEnemys[i].bug) && rootEnemys[i].bug.alive){
                   element.value-=2;
                   if(element.value==0)
                    {
                        
                       // this.resetPlayer();
                        element.value=100;
                        player.reset(1800,3670);
                    }
                }
                else{
                     absolut = Math.abs( (player.x - rootEnemys[i].bug.x));
                     if( absolut <= 50 ){
                         if(player.y >= rootEnemys[i].bug.y)
                            rootEnemys[i].bug.animations.play('walkDown');
                         else
                            rootEnemys[i].bug.animations.play('walkUp'); 
                             
                        }
                     else if(player.x >= rootEnemys[i].bug.x)
                            rootEnemys[i].bug.animations.play('walkRight');
                         else
                            rootEnemys[i].bug.animations.play('walkLeft'); 
                             
                        
                    if(Phaser.Math.distance(player.x, player.y, rootEnemys[i].bug.x, rootEnemys[i].bug.y) <= 700 
                         && Phaser.Math.distance(rootEnemys[i].originx, rootEnemys[i].originy, player.x, player.y) < 700)  
                              this.game.physics.arcade.moveToXY(rootEnemys[i].bug, player.x,player.y, 300);
                       else 
                           {   
                             absolut = Math.abs( (rootEnemys[i].originx - rootEnemys[i].bug.x));
                             if( absolut <= 50 ){
                                 if(rootEnemys[i].originy >= rootEnemys[i].bug.y)
                                    rootEnemys[i].bug.animations.play('walkDown');
                                 else
                                    rootEnemys[i].bug.animations.play('walkUp'); 

                                }
                             else if(rootEnemys[i].originx >= rootEnemys[i].bug.x)
                                    rootEnemys[i].bug.animations.play('walkRight');
                                 else
                                    rootEnemys[i].bug.animations.play('walkLeft'); 
                             this.game.physics.arcade.moveToXY(rootEnemys[i].bug, rootEnemys[i].originx,rootEnemys[i].originy, 150);
                            //enemys[i].cookie3.body.velocity.x = 0;
                            //enemys[i].cookie3.body.velocity.y = 0;
                         }
                      
                }
                
            } 
        
        
       /* if(checkOverlap(player,webServer)){
            if(misWebServer == 0 && beenOnWeb == 0){
                beenOnWeb =1;
                misWebServer++;
                vizNpc =0;
               // document.getElementById("MSG").innerHTML = "Te cunosc.sa Ai primit 10 banuti";
                //gold+=10;
                document.getElementById("MSG").innerHTML = " ";
                repaintGold();
            }
            else if(misWebServer == 1 && beenOnWeb == 0){
                        misWebServer++;
                        beenOnWeb = 1;
                        gold+=10;
                        document.getElementById("MSG").innerHTML = " ";
                        repaintGold();        
            }
            else if(misWebServer == 2 && beenOnWeb == 0){
                        misWebServer++;
                        beenOnWeb = 1;
                        gold+=10;
                        document.getElementById("MSG").innerHTML = " ";
                        repaintGold();
            }
            else if(misWebServer == 3 && beenOnWeb == 0){
                        misWebServer++;
                        beenOnWeb = 1;
                        gold+=10;
                        document.getElementById("MSG").innerHTML = " ";
                        repaintGold();
                
            }
        }  */
        
        document.getElementById("ChatContent").innerHTML="";
        document.getElementById("ChatAnswer1").innerHTML="";
        document.getElementById("ChatAnswer2").innerHTML="";
        document.getElementById("ChatAnswer3").innerHTML="";
        document.getElementById("ChatAnswer4").innerHTML="";
         
        if(checkOverlap(player,webServer)){
            if(misWebServer == 0 && beenOnWeb == 0){
            if(aliasDialog!=webDialog1)
                webDialog1.pos=0;
             aliasDialog=webDialog1;
             if(aliasDialog.dialog[2].length > aliasDialog.pos)
             {
             document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
             if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
             document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
             if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
            document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
             if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
            document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
             if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
            document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
            }
             else
             {
                document.getElementById("ChatContent").innerHTML="";
                document.getElementById("ChatAnswer1").innerHTML="";
                document.getElementById("ChatAnswer2").innerHTML="";
                document.getElementById("ChatAnswer3").innerHTML="";
                document.getElementById("ChatAnswer4").innerHTML="";
            }
        }
            else if(misWebServer == 1 && beenOnWeb == 0){
                     if(aliasDialog!=webDialog2)
                        webDialog2.pos=0;
                     aliasDialog=webDialog2;
                     if(aliasDialog.dialog[2].length > aliasDialog.pos)
                     {
                     document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                     if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                     document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                     if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                    document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                     if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                    document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                     if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                    document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                    }
                     else
                     {
                        document.getElementById("ChatContent").innerHTML="";
                        document.getElementById("ChatAnswer1").innerHTML="";
                        document.getElementById("ChatAnswer2").innerHTML="";
                        document.getElementById("ChatAnswer3").innerHTML="";
                        document.getElementById("ChatAnswer4").innerHTML="";
                    }
                }
            else if(misWebServer == 2 && beenOnWeb == 0){
                     if(aliasDialog!=webDialog3)
                        webDialog3.pos=0;
                     aliasDialog=webDialog3;
                     if(aliasDialog.dialog[2].length > aliasDialog.pos)
                     {
                     document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                     if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                     document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                     if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                    document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                     if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                    document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                     if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                    document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                    }
                     else
                     {
                        document.getElementById("ChatContent").innerHTML="";
                        document.getElementById("ChatAnswer1").innerHTML="";
                        document.getElementById("ChatAnswer2").innerHTML="";
                        document.getElementById("ChatAnswer3").innerHTML="";
                        document.getElementById("ChatAnswer4").innerHTML="";
                    }
                }
            else if(misWebServer == 3 && beenOnWeb == 0){
                     if(aliasDialog!=webDialog4)
                        webDialog4.pos=0;
                     aliasDialog=webDialog4;
                     if(aliasDialog.dialog[2].length > aliasDialog.pos)
                     {
                     document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                     if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                     document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                     if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                    document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                     if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                    document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                     if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                    document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                    }
                     else
                     {
                        document.getElementById("ChatContent").innerHTML="";
                        document.getElementById("ChatAnswer1").innerHTML="";
                        document.getElementById("ChatAnswer2").innerHTML="";
                        document.getElementById("ChatAnswer3").innerHTML="";
                        document.getElementById("ChatAnswer4").innerHTML="";
                    }
                }
        }
        else {webDialog1.pos=0;
              webDialog2.pos=0;
              webDialog3.pos=0;
              webDialog4.pos=0;
             }
        
        /*if(checkOverlap(player,restServer)){
            if(misWebServer == 4){
                if(missionREST == 0 && beenOnRest == 0){
                    missionREST++;
                    beenOnRest =1;
                   // document.getElementById("MSG").innerHTML = "Te cunosc.sa Ai primit 10 banuti";
                    gold+=20;
                    repaintGold();
                    changePlayer();
                    //this.physics.arcade.disable(player1);
                    if (changeBody == 0){
                        this.transform();
                        changeBody = 1; 
                    }
                    
                }
                else  if(missionREST == 1 && beenOnRest == 0){
                    missionREST++;
                    beenOnRest = 1;
                   // document.getElementById("MSG").innerHTML = "Te cunosc.sa Ai primit 10 banuti";
                    gold+=20;
                    repaintGold();
                    changePlayer();
                }
            }
        }*/
         if(checkOverlap(player,restServer)){
            if(misWebServer == 4 && beenOnWeb == 1){
             if(missionREST == 0 && beenOnRest == 0){
                if (changeBody == 0){
                        this.transform();
                        changeBody = 1; 
                    }
                if(aliasDialog!=restDialog1)
                    restDialog1.pos=0;
                 aliasDialog=restDialog1;
                 if(aliasDialog.dialog[2].length > aliasDialog.pos)
                 {
                 document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                 if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                 document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                 if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                 if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                 if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                }
                 else
                 {
                    document.getElementById("ChatContent").innerHTML="";
                    document.getElementById("ChatAnswer1").innerHTML="";
                    document.getElementById("ChatAnswer2").innerHTML="";
                    document.getElementById("ChatAnswer3").innerHTML="";
                    document.getElementById("ChatAnswer4").innerHTML="";
                }
             }
            else if(missionREST == 1 && beenOnRest == 0){
                if(aliasDialog!=restDialog2)
                    restDialog2.pos=0;
                 aliasDialog=restDialog2;
                 if(aliasDialog.dialog[2].length > aliasDialog.pos)
                 {
                 document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                 if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                 document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                 if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                 if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                 if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                }
                 else
                 {
                    document.getElementById("ChatContent").innerHTML="";
                    document.getElementById("ChatAnswer1").innerHTML="";
                    document.getElementById("ChatAnswer2").innerHTML="";
                    document.getElementById("ChatAnswer3").innerHTML="";
                    document.getElementById("ChatAnswer4").innerHTML="";
                }
            }
        }
        }
        else {restDialog1.pos=0;
              restDialog2.pos=0;
              //BDsprite.animations.play('closeBook',6,true);
             }
        /*if(checkOverlap(player,BDsprite)){
                if(misWebServer == 1){ 
                    BDsprite.animations.play('openBook',6,true);
                    if( beenOnWeb == 1){
                        beenOnWeb = 0;
                        gold+=20;
                        //BDsprite.animations.play('open');
                        repaintGold(); 
                    }
                }
        }
        else BDsprite.animations.play('closeBook');*/
         
        if(checkOverlap(player,BDsprite)){
            if(misWebServer == 1 && beenOnWeb == 1){
                BDsprite.animations.play('openBook',6,true);
                if(aliasDialog!=bdDialog)
                    bdDialog.pos=0;
                 aliasDialog=bdDialog;
                 if(aliasDialog.dialog[2].length > aliasDialog.pos)
                 {
                 document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                 if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                 document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                 if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                 if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                 if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                }
                 else
                 {
                    document.getElementById("ChatContent").innerHTML="";
                    document.getElementById("ChatAnswer1").innerHTML="";
                    document.getElementById("ChatAnswer2").innerHTML="";
                    document.getElementById("ChatAnswer3").innerHTML="";
                    document.getElementById("ChatAnswer4").innerHTML="";
                }
        }
        }
        else {bdDialog.pos=0;
              BDsprite.animations.play('closeBook',6,true);
             }
        
        /*if(checkOverlap(player,APIsprite)){
                if(misWebServer == 2 ){
                    
                    if(beenOnWeb == 1){
                        beenOnWeb = 0;
                        // document.getElementById("MSG").innerHTML = "Te cunosc.sa Ai primit 10 banuti";
                        gold+=20;
                        //BDsprite.animations.play('open');
                        repaintGold();
                    }
                }
        }*/
        if(checkOverlap(player,APIsprite)){
            if(misWebServer == 2 && beenOnWeb == 1){
                if(aliasDialog!=apiDialog)
                    apiDialog.pos=0;
                 aliasDialog=apiDialog;
                 if(aliasDialog.dialog[2].length > aliasDialog.pos)
                 {
                 document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                 if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                 document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                 if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                 if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                 if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                }
                 else
                 {
                    document.getElementById("ChatContent").innerHTML="";
                    document.getElementById("ChatAnswer1").innerHTML="";
                    document.getElementById("ChatAnswer2").innerHTML="";
                    document.getElementById("ChatAnswer3").innerHTML="";
                    document.getElementById("ChatAnswer4").innerHTML="";
                }
        }
        }
        else {apiDialog.pos=0;
             }
        
        /*if(checkOverlap(player,FormSprite)){
                if(misWebServer == 3){ 
                    FormSprite.animations.play('pressButton');
                    if( beenOnWeb == 1){
                        beenOnWeb = 0;
                        gold+=20;
                        repaintGold(); 
                    }
                }
        }
        else  FormSprite.animations.play('reliseButton');
        */
        if(checkOverlap(player,FormSprite)){
            if(misWebServer == 3 && beenOnWeb == 1){
                FormSprite.animations.play('pressButton');
                if(aliasDialog!=formDialog)
                    formDialog.pos=0;
                 aliasDialog=formDialog;
                 if(aliasDialog.dialog[2].length > aliasDialog.pos)
                 {
                 document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                 if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                 document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                 if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                 if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                 if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                }
                 else
                 {
                    document.getElementById("ChatContent").innerHTML="";
                    document.getElementById("ChatAnswer1").innerHTML="";
                    document.getElementById("ChatAnswer2").innerHTML="";
                    document.getElementById("ChatAnswer3").innerHTML="";
                    document.getElementById("ChatAnswer4").innerHTML="";
                }
        }
        }
        else {formDialog.pos=0;
              FormSprite.animations.play('reliseButton');
             }
        
        /*if(checkOverlap(player,UriSprite)){
                if(missionREST == 1){
                    UriSprite.animations.play('openURI'); 
                    if( beenOnRest == 1){
                        beenOnRest = 0;
                        gold+=20;
                        repaintGold(); 
                    }
                }
        }
        else  UriSprite.animations.play('closeURI');
        */
        if(definedPortal == true){
            if(checkOverlap(player,portal)){
                this.state.start('Level1');
            }
        }
        
        if(checkOverlap(player,UriSprite)){
            if(missionREST == 1 && beenOnRest == 1){
                UriSprite.animations.play('openURI');
                if(aliasDialog!=uriDialog)
                    uriDialog.pos=0;
                 aliasDialog=uriDialog;
                 if(aliasDialog.dialog[2].length > aliasDialog.pos)
                 {
                 document.getElementById("ChatContent").innerHTML="Server> "+aliasDialog.dialog[0][aliasDialog.pos];
                 if(aliasDialog.dialog[1][aliasDialog.pos][0]!=null)
                 document.getElementById("ChatAnswer1").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][0];
                 if(aliasDialog.dialog[1][aliasDialog.pos][1]!=null)
                document.getElementById("ChatAnswer2").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][1];
                 if(aliasDialog.dialog[1][aliasDialog.pos][2]!=null)
                document.getElementById("ChatAnswer3").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][2];
                 if(aliasDialog.dialog[1][aliasDialog.pos][3]!=null)
                document.getElementById("ChatAnswer4").innerHTML="Client>"+aliasDialog.dialog[1][aliasDialog.pos][3];   
                }
                 else
                 {
                    document.getElementById("ChatContent").innerHTML="";
                    document.getElementById("ChatAnswer1").innerHTML="";
                    document.getElementById("ChatAnswer2").innerHTML="";
                    document.getElementById("ChatAnswer3").innerHTML="";
                    document.getElementById("ChatAnswer4").innerHTML="";
                }
        }
        }
        else {uriDialog.pos=0;
              UriSprite.animations.play('closeURI');
             }
        
        
        for(i=0; i < nrRootEnemys; i++)
            {
                if(checkOverlap(bullets,rootEnemys[i].bug) && rootEnemys[i].bug.alive){
                    bullet.kill();
                    bullet.body.velocity.x = 0;
                    bullet.body.velocity.y = 0;
                    coins[i].x=rootEnemys[i].bug.x;
                    coins[i].y=rootEnemys[i].bug.y;
                    coins[i].body.velocity.x = 0;
                    coins[i].body.velocity.y = 0;
                   // this.game.physics.arcade.moveToXY(cookies[i], enemys[i].cookie3.x,enemys[i].cookie3.y, 200);
                    checkDeadBugsRoot[i] = true;
                    //enemys[i].cookie3.body.velocity.x = 0;
                    //enemys[i].cookie3.body.velocity.y = 0;

                    rootEnemys[i].bug.kill();
                    coins[i].visible = true;
                    this.destroyBullet;
                    }
                
            }
        for(i=0; i < nrSuperEnemies; i++)
            {
                if(checkOverlap(bullets,superEnemys[i].bug) && superEnemys[i].bug.alive){
                    bullet.kill();
                    bullet.body.velocity.x = 0;
                    bullet.body.velocity.y = 0;
                    coins[i+nrRootEnemys].x=superEnemys[i].bug.x;
                    coins[i+nrRootEnemys].y=superEnemys[i].bug.y;
                    coins[i+nrRootEnemys].body.velocity.x = 0;
                    coins[i+nrRootEnemys].body.velocity.y = 0;
                   // this.game.physics.arcade.moveToXY(cookies[i], enemys[i].cookie3.x,enemys[i].cookie3.y, 200);
                    checkDeadBugsRoot[i] = true;
                    //enemys[i].cookie3.body.velocity.x = 0;
                    //enemys[i].cookie3.body.velocity.y = 0;

                    superEnemys[i].bug.kill();
                    coins[i+nrSuperEnemies].visible = true;
                    this.destroyBullet;
                    }
                
            }
        for(i=0; i < nrenemies; i++)
            {
                if(checkOverlap(bullets,enemys[i].bug) && enemys[i].bug.alive){
                    bullet.kill();
                    bullet.body.velocity.x = 0;
                    bullet.body.velocity.y = 0;
                    coins[i+nrRootEnemys+nrSuperEnemies].x=enemys[i].bug.x;
                    coins[i+nrRootEnemys+nrSuperEnemies].y=enemys[i].bug.y;
                    coins[i+nrRootEnemys+nrSuperEnemies].body.velocity.x = 0;
                    coins[i+nrSuperEnemies+nrRootEnemys].body.velocity.y = 0;
                   // this.game.physics.arcade.moveToXY(cookies[i], enemys[i].cookie3.x,enemys[i].cookie3.y, 200);
                    checkDeadBugsRoot[i] = true;
                    //enemys[i].cookie3.body.velocity.x = 0;
                    //enemys[i].cookie3.body.velocity.y = 0;

                    enemys[i].bug.kill();
                    coins[i+nrSuperEnemies+nrRootEnemys].visible = true;
                    this.destroyBullet;
                    }
                
            }
        for(i=0; i<nrCoins; i++)
            {
                if(coins[i]!=null)
                if(checkOverlap(player,coins[i])){
                    gold++;
                    repaintGold();
                    coins[i].x=gravex;
                    coins[i].y=gravey;
                    //coins[i].visible=false;
                }
            }
    },
    
    
    
    
    //RESET PLAYER TO GIVEN POSITION
    resetPlayer : function(){
        this.spawn();
    },
    
    render:function() {
	this.game.debug.text(this.game.time.fps, 2, 14, "#33DBFF");
    this.game.debug.spriteInfo(player, 32, 130,"#33DBFF");
    },
    
    //SPAWN FUNCTION
    spawn : function(){
          
        element = document.getElementById("HPBar");
        //alert(element.value);
        /*respawn.forEach(function(spawnPoint){
            player.reset(spawnPoint.x,spawnPoint.y);
        },this);*/
        if(isPlayer == 0){
            player = this.add.sprite(1800,3670,'player');//(1800,3670,'player');
            isPlayer=1;
        }
            
        

        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        this.physics.arcade.enable(player);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
    },
    
    
    //CHANGE FUNCTION
    transform:function(){
        player.animations.play('idle');
        var aux = player;
        player1 = this.add.sprite(1800,3670,'character');//(1800,3670,'player');
        player1.scale.setTo(2);
        player1.anchor.setTo(0.5,0.5);
        player1.animations.add('walkdown',[0,1,2,1],13,true);
        player1.animations.add('walkright',[6,7,8,7],13,true);
        player1.animations.add('walkup',[9,10,11,10],13,true);
        player1.animations.add('walkleft',[4,3,5,3],13,true);
        player1.animations.add('idle',[1],4,true);
        
        
        this.physics.arcade.enable(player1);
        player = player1;
        player1 = aux;
        this.physics.arcade.enable(player);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
    },
    
    //WHEN HIT FIRE JUMP TO LEVEL2 MAP
    goToLevel2 :function(){
        this.state.start('Level2');
    },
    
};



function togglePause() {

    this.game.physics.arcade.isPaused = (this.game.physics.arcade.isPaused) ? false : true;

};
function repaintGold()
{
    var canvas = document.getElementById("Gold");
    var ctx = canvas.getContext("2d");
    ctx.font = "20px Arial";
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillText(gold,50,15);
}
function changePlayer(){
    
}
function actionInput(action){
    //console.log("aici>"+CurrentMap.id+" "+action);
    if(CurrentMap.id == 11){
        //console.log("aici>"+beenOnWeb+" "+misWebServer+ " "+ action + " "+moveOn );
        if(misWebServer == 0 && beenOnWeb == 0){
            if(action == 0){
                if(moveOn == 1){
                    beenOnWeb = 1;
                    misWebServer++;
                    gold+=10;
                    repaintGold();
                    moveOn = 0;    
                }
                else moveOn =1;
            }
        }
        else if(misWebServer == 1 && beenOnWeb == 0){
                if(action == 0){
                    if(moveOn == 1){
                        misWebServer++;
                        beenOnWeb = 1;
                        gold+=10;
                        repaintGold();
                        moveOn= 0;
                    }
                        else moveOn =1;
                    }
        }
        else if(misWebServer == 1 && beenOnWeb == 1){ //DataBase
            //console.log("aici>"+beenOnWeb+" "+misWebServer+ " "+ action + " "+moveOn );
                    if(action == 0){
                        if(moveOn == 1){
                            //misWebServer++;
                            beenOnWeb = 0;
                            gold+=10;
                            moveOn = 0;
                            repaintGold(); 
                        }
                        else moveOn =1;
                    }
        }
        else if(misWebServer == 2 && beenOnWeb == 0){
                if(action == 0){
                    if(moveOn == 1){
                        misWebServer++;
                        beenOnWeb = 1;
                        gold+=10;
                        //document.getElementById("MSG").innerHTML = " ";
                        repaintGold();
                        moveOn = 0;
                    }
                    else moveOn =1;
            }
        }
        else if(misWebServer == 2 && beenOnWeb == 1){//API
                if(action == 0){
                     if(moveOn == 1){
                        //misWebServer++;
                        beenOnWeb = 0;
                        gold+=10;
                        //document.getElementById("MSG").innerHTML = " ";
                        repaintGold();
                        moveOn=0;
                     }
                     else moveOn =1;
            }
        }
        else if(misWebServer == 3 && beenOnWeb == 0){
                if(action == 0){
                    if(moveOn == 1){
                        misWebServer++;
                        beenOnWeb = 1;
                        gold+=10;
                        //document.getElementById("MSG").innerHTML = " ";
                        repaintGold();
                        moveOn=0;
                     }
                     else moveOn =1;
                
            }
        }
        else if(misWebServer == 3 && beenOnWeb == 1){
                if(action == 0){
                    if(moveOn == 1){
                        //misWebServer++;
                        beenOnWeb = 0;
                        gold+=10;
                        //document.getElementById("MSG").innerHTML = " ";
                        repaintGold();
                        moveOn=0;
                     }
                     else moveOn =1;
                
            }
        }
        else if(misWebServer == 4 && beenOnWeb == 1){
                if(missionREST == 0 && beenOnRest == 0){
                    if(action == 0){
                        if(moveOn == 1){
                            missionREST++;
                            beenOnRest = 1;
                            gold+=10;
                            //document.getElementById("MSG").innerHTML = " ";
                            repaintGold();
                            moveOn=0;
                        }
                     else moveOn =1;
                    }
                }
                else if(missionREST == 1 && beenOnRest == 1){
                    console.log("aici>"+beenOnWeb+" "+misWebServer+ " "+ action + " "+moveOn );
                    if(action == 0){
                        if(moveOn == 1){
                            //missionREST++;
                            beenOnRest = 0;
                            gold+=10;
                            //document.getElementById("MSG").innerHTML = " ";
                            repaintGold();
                            moveOn=0;
                        }
                     else moveOn =1;
                    }
                }
                else if(missionREST == 1 && beenOnRest == 0){
                    if(action == 0){
                        if(moveOn == 1){
                            definedPortal= true;
                            portal.alpha = 1; 
                            missionREST++;
                            beenOnRest = 1;
                            gold+=10;
                            //document.getElementById("MSG").innerHTML = " ";
                            repaintGold();
                            moveOn=0;
                            player = player1;
                            this.camera.follow(player);
                            
                        }
                     else moveOn =1;
                    }
                }
            }
        else {//final de joc!
        }
         
    }
}