create table dialog 
(
  id char(5) not null,
  monster_id char(3),
  message clob,
  answer1 clob,
  answer2 clob,
  answer3 clob,
  answer4 clob,
  order_index number(5),
  CONSTRAINT dialog_unique UNIQUE (id,order_index,monster_id)

);
insert into map values('10',1,200);
insert into monster_type values('1',1,0,null,1);
insert into monster_onmap values('1',1,1,1710,1670,'10');
insert into monster_onmap values('2',1,1,1800,1670,'10');
insert into dialog values ('1','2','Happy to see you client,I''m server GET HTTP request ! <br> Your quest is very simple. <br> Go to left for web session experience or to the right for sessionless web experience. <br> .Choose your path and ''POST'' to server 25 gold ','GET[Session?]','GET[Without session?]',null,null,1);
insert into dialog values ('11','2','200 ok! <br> Be aware you may encounter cookies','GET[Great]',null,null,null,2);
insert into dialog values ('12','2','200 ok! <br> It may give you a scence of deja-vu','GET[GREAT]',null,null,null,2);
insert into dialog values ('111','2','200 ok! <br> Good luck!',null,null,null,null,3);
insert into dialog values ('121','2','200 ok! <br> Good luck!',null,null,null,null,3);

insert into dialog values ('1','1','Happy to see you client,I''m server POST HTTP request! <br> Can you POST another $$$ gold <br> For more info make a GET request to my friend here!  ','POST[Sure, take it what i have]','POST[Not yet]',null,null,1);
insert into dialog values ('15','1','404 not found!You don''t have any gold ','POST[I''ll go get more]',null,null,null,2);
insert into dialog values ('11','1','200 ok!Good job, you need $$$ more ','POST[In a moment]',null,null,null,2);
insert into dialog values ('12','1','200 ok!Good luck','POST[No need]',null,null,null,2);
insert into dialog values ('111','1','200 ok! Good luck!',null,null,null,null,3);
ALTER TABLE map
  MODIFY valoare number(5);
ALTER TABLE monster_onmap
  ADD dialog clob;
insert into monster_type
drop table camera;
ALTER TABLE monster_onmap SET UNUSED (position);
alter table monster_onmap set unused(camera_id);
alter table monster_onmap set unused(dialog);
alter table monster_onmap
add map_id char(3);
alter table monster_onmap
add position_x number(6);

alter table monster_onmap
add position_y number(6);

ALTER TABLE item
ADD CONSTRAINT Item_UnikId UNIQUE (id);
ALTER TABLE map
ADD CONSTRAINT map_UnikId UNIQUE (id);
ALTER TABLE map
ADD CONSTRAINT monster_UnikId UNIQUE (id);
ALTER TABLE monster_type
ADD CONSTRAINT monster_UnikId UNIQUE (id);
ALTER TABLE player
ADD CONSTRAINT player_UnikId UNIQUE (id);
ALTER TABLE savegame
ADD CONSTRAINT savegame_UnikId UNIQUE (id);
ALTER TABLE skill
ADD CONSTRAINT skill_UnikId UNIQUE (id);
ALTER TABLE dialog
ADD CONSTRAINT dialog_UnikId UNIQUE (id);