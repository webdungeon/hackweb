Game.Level1 = function(game){
   
};

var map;
var layer;
var music;
var respawn;

var player;
var controls = {};
var playerSpeed = 150;
var jumpTimer = 0;
 

Game.Level1.prototype = {
    create: function(){
        this.stage.backgroundColor = '#3A5963';
        
        // OBJECT !!!  THATS HOW IT WORKS , NEEDS A GROUP
        respawn = this.game.add.group();
        //
        
        this.map = this.game.add.tilemap('map');       
        
        this.map.addTilesetImage('tiles1');
        this.map.addTilesetImage('nature_tiles');
        this.map.addTilesetImage('nature_tiles2');
        this.map.addTilesetImage('nature_tiles3');
        this.map.addTilesetImage('nature_tiles4');
        this.map.addTilesetImage('ghost');
        
       
        
        this.bottomLayer = this.map.createLayer('bottomLayer');
        this.bottomLayer = this.map.createLayer('secondBottomCollideLayer');
        this.secondBottomLayer = this.map.createLayer('secondBottomLayer');
        this.topLayer = this.map.createLayer('topLayer');
        this.moreThenTopLayer = this.map.createLayer('moreThenTopLayer');
        this.moreMoreThenTopLayer = this.map.createLayer('moreMoreThenTopLayer');
        this.objectLayer = this.map.createLayer('objectLayer');
        
        //this.map.setCollisionByExclusion([0]);
        this.bottomLayer.resizeWorld();
        
        // OBJECT LAYER
        
              //HERE IS THE START POSITION
        this.map.createFromObjects('objectLayer',820 , '' ,0,true,false,respawn);
        
        
        //MUSIC
        music = this.game.add.audio('soundKey');
        //music.play(); // REMOVE THIS IF YOU DONT WANT MUSIC
        
        //SPRITES
        
        //FIRE
        /*fire = this.add.sprite(400,300,'fire');
        //fire.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(fire);
        var fire_anim = fire.animations.add('fire_anim');
        fire.animations.play('fire_anim', 12, true);
        fire.body.collideWorldBounds = true;
        fire.body.immovable = true;*/
        
        
        //DOOR TO NEXT LEVEL
        doors2 = this.add.sprite(65,96,'doors2');
        doors2.anchor.setTo(0.5,0.5);
        this.physics.arcade.enable(doors2);
        var doors2_anim = doors2.animations.add('doors2_anim',[0,4,7,10]);
        doors2.animations.play('doors2_anim', 1, true);
        doors2.body.collideWorldBounds = true;
        doors2.body.immovable = true;
        
        
        //PLAYER
        

        this.map.setCollisionBetween(1, 100000, true, 'topLayer');  //!!!
        
      
        
        //
        
        player = this.add.sprite(0,0,'player');
        player.anchor.setTo(0.5,0.5);
        
        //PLAYER SPAWNS AT THE SET POSITION
        this.spawn();
        
        player.animations.add('walkdown',[2,3,4,5],13,true);
        player.animations.add('walkup',[6,7,8,9,10,11],13,true);
        player.animations.add('walkleft',[14,15,16,17],13,true);
        player.animations.add('walkright',[20,21,22,23],13,true);
        player.animations.add('idle',[0,1],4,true);
        
        
        this.physics.arcade.enable(player);
        this.camera.follow(player);
        player.body.collideWorldBounds = true;
        
        controls = {
            right: this.input.keyboard.addKey(Phaser.Keyboard.D),
            left: this.input.keyboard.addKey(Phaser.Keyboard.A),
            up: this.input.keyboard.addKey(Phaser.Keyboard.W),
            down : this.input.keyboard.addKey(Phaser.Keyboard.S),
        }
        
        
        
      
        
     
        
    },
    
    update:function(){
        
        this.physics.arcade.collide(player,this.moreMoreThenTopLayer);
        this.physics.arcade.collide(player,this.moreThenTopLayer);
        this.physics.arcade.collide(player,this.bottomLayer); 
        //this.physics.arcade.collide(player,this.secondBottomCollideLayer);
        //this.physics.arcade.collide(player,this.secondBottomLayer);
        
        this.physics.arcade.collide(player,this.topLayer);
        //this.physics.arcade.collide(player,fire);
        this.physics.arcade.collide(player,doors2,this.goToLevel2,null,this);
        
        
        
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        
        if(controls.right.isDown){
            player.animations.play('walkright');
            //player.scale.setTo(1,1);
            player.body.velocity.x += playerSpeed;        }
         
      
        if(controls.left.isDown){
            player.animations.play('walkleft');
            //player.scale.setTo(-1,1);
            player.body.velocity.x -= playerSpeed;       
        }
         
        
        if(controls.up.isDown){
             player.body.velocity.y -= playerSpeed;
            
             player.animations.play('walkup');
         }
        
        if(controls.down.isDown){
             player.body.velocity.y += playerSpeed;
            
             player.animations.play('walkdown');
         }
        
        if(player.body.velocity.x ==0 && player.body.velocity.y ==0 ){
            player.animations.play('idle');
        }
    },
    
    
    //RESET PLAYER TO GIVEN POSITION
    resetPlayer :function(){
        player.reset(100,560);
    },
    
    //SPAWN FUNCTION
    spawn : function(){
        respawn.forEach(function(spawnPoint){
            player.reset(spawnPoint.x,spawnPoint.y);
        },this);
    },
    
    
    //WHEN HIT FIRE JUMP TO LEVEL2 MAP
    goToLevel2 :function(){
        this.state.start('Level2');
    },
    
    
};