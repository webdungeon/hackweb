
DROP TABLE player
/
DROP TABLE camera
/
DROP TABLE map
/
DROP TABLE monster_type
/
DROP TABLE monster_onmap
/
DROP TABLE item
/
DROP TABLE skill
/
DROP TABLE savegame
/

create table t_names
( name Varchar2(36)
);
create table t_prenames
( prenames Varchar2(36)
);

create table t_skill_types ( name VARCHAR2(10));


  CREATE or REPLACE TYPE mycoord AS OBJECT (
   x REAL, 
   y REAL,
   locked NUMBER(1),
    MEMBER FUNCTION get_x RETURN REAL,
  MEMBER FUNCTION get_y RETURN REAL
  )
  /

CREATE TABLE player(
  id CHAR(5) NOT NULL,
  username VARCHAR2(25),
  password VARCHAR2(25),
  email VARCHAR2(35),
  creation_date DATE
  )
/



CREATE TABLE camera(
  id CHAR(5) NOT NULL,
  length NUMBER(4),
  width NUMBER(4),
  map_id CHAR(3),
  door_n mycoord ,
  door_s mycoord  ,
  door_e mycoord  ,
  door_v mycoord  ,
  neighboor_n_id CHAR(5),
  neighboor_s_id CHAR(5),
  neighboor_e_id CHAR(5),
  neighboor_v_id CHAR(5),
  monster_number NUMBER(4),
  monster_min_id char(5),
  monster_max_id char(5)
  )
/

CREATE TABLE map(
  id CHAR(3) not null,
  nivel NUMBER(2),
  valoare NUMBER(2)
  )
/

CREATE TABLE monster_type(
  id CHAR(3) not null,
  max_hp NUMBER(6),
  base_power NUMBER(4),
  item_id char(3)
)
/
CREATE TABLE monster_onmap(
  monster_id char(3) not null,
  camera_id char(3),
  position mycoord,
  health NUMBER(3),
  alive NUMBER(1)
)
/
CREATE TABLE Item (
  id char(3) not null,
  power NUMBER(5),
  accuracy NUMBER(3),
  nr_shots NUMBER(1),
  price NUMBER(4),
  name VARCHAR2(25),
  description VARCHAR2(500),
  require_skill_id char(3)
)
/

CREATE TABLE skill (
  id char(3) not null,
  savegame_id char(6),
  skill_type VARCHAR2(10),
  skill_point NUMBER(1),
  skill_max_points NUMBER(1),
  require_skill_id CHAR(3)
)
/


CREATE TABLE savegame(
  id char(6) not null, 
  player_id char(5) not null,
  map_id char(3),
  camera_id char(3),
  player_position mycoord,
  player_hp NUMBER(3),
  player_coins NUMBER(4),
  player_experience NUMBER(5),
  player_level NUMBER(2),
  player_skill_id char(3),
  player_base_power NUMBER(2),
  item1_id char(3),
  item2_id char(3),
  item3_id char(3),
  item4_id char(3),
  creation_date DATE
)
/

COMMIT;




